import React from "react";
import { Link, Redirect } from "react-router-dom";
import Constants from "../variable/Constants";
import Axios from "axios";
import init from "../helper/WindowToken";
import Footer from "../Partials/Footer";
export default class SignIn extends React.Component {
  state = {
    email: "",
    password: "",
    redirect: false,
    login: false,
    userDetails: {
      name: "",
      email: "",
      phone_number: "",
    },
    loading: false,
    notification: false,
    error: false,
    showSuccess: false,
  };
  submit = (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const payload = {
      email: this.state.email,
      password: this.state.password,
    };

    Axios.post(Constants.postUrl.login, payload).then((res) => {
      console.log(res);
      // const id = localStorage.setItem("id", res.data.data.id);
      // const name = localStorage.setItem("name", res.data.data.name);
      // const email = localStorage.setItem("email", res.data.data.email);
      // const usertoken = localStorage.setItem("token", res.data.token);
      // console.log(usertoken);

      if (res.data.status === "failed") {
        // alert("Failed");
        this.setState({
          loading: false,
          error: true,
          successMsg: res.data.message,
        });
        setTimeout(() => {
          this.setState({
            error: false,
            
          });
        document.getElementById("myform").reset();

        }, 2000);
      } else {
        this.setState({
          notification: true,
          showSuccess: true,
          successMsg: res.data.message,
          loading: false,
        });
      
        const { data } = res;
        const ttl = data.expires_in;
        localStorage.setItem("login", "true");
        localStorage.setItem("accessToken", data.token);
        localStorage.setItem("refreshToken", data.token);
        localStorage.setItem("name", data.name);
        console.log(localStorage.getItem("accessToken"));
        localStorage.setItem("expiresIn", ttl);
        init();
        if (init() === "success") {
          const cartData = localStorage.getItem("cart");
          console.log(cartData);
          if (cartData) {
            let parsedCart = JSON.parse(cartData);
            parsedCart.forEach((item) => {
              console.log(item);
              let payload = {
                product_id: item.product_id,
                product_variant_id: item.product_variant_id,
                quantity: item.quantity,
              };
              Axios.post(Constants.postUrl.cart, payload).then((res) => {
                console.log(res.data);
              });
            });
            localStorage.removeItem("cart");
            // setTimeout(() => {
              window.location.href = "/";
            // }, 2000);
          } else {
            window.location.href = "/";
          }
          // window.location.href = "/";
          // this.setState({
          //   redirect: true,
          //   userDetails: res.data.data,
          //   login: true
          // });
        }
        setTimeout(() => {
          this.setState({
            showSuccess: false,
            redirect:true
          });
        }, 2000);
      }
    });
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    if (this.state.redirect) {
      return (
        <Redirect
          to={{ pathname: "/", state: { userDetails: this.state.userDetails } }}
        />
      );
    } else {
      return (
        <div id="sign-in">
          {/* <Header />
          <Menu /> */}
          <div className="signinpage">
            <div className="signin">
              <h3 style={{ textAlign: "center", marginBottom: "30px" }}>
                Login
              </h3>
              <form onSubmit={this.submit} id="myform">
                <input
                  type="text"
                  name="email"
                  required
                  placeholder="Email/Mobile No."
                  onChange={this.handleChange}
                />
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                  onChange={this.handleChange}
                />
                <div className="center">
                  <Link to="/forgot-password">
                    <h4> Forgot your Password? </h4>
                  </Link>
                  <button type="submit">
                    {this.state.loading
                      ? "Logging In..."
                      : this.state.notification
                      ? "Success!"
                      : this.state.error
                      ? "Failed!"
                      : "Sign In"}
                  </button>
               <p style={{color:"green"}}>  {this.state.showSuccess ? this.state.successMsg : ""}</p> 
                  <p style={{color:"red"}}> {this.state.error ? this.state.successMsg : ""}</p>

                  <Link to="register">Create Account </Link>
                  <Link to="/" className="return">
                    Return to Store
                  </Link>
                </div>
              </form>
            </div>
          </div>
          <Footer />
        </div>
      );
    }
  }
}
