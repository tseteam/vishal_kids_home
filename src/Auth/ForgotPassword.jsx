import Axios from "axios";
import React, { Component } from "react";
import Constants from "../variable/Constants";
import Footer from "../Partials/Footer";

export default class ForgotPassword extends Component {
  state = { email: "" };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  sendEmail = (e) => {
    e.preventDefault();
    const load = {
      email: this.state.email,
    };
    console.log(load);
    this.setState({ Loading: true });
    Axios.post(Constants.postUrl.forgotPass, load).then((res) => {
      console.log(res);
    //   if (res.data.status === "success") {
        this.setState({
          showMsg: true,
          Loading: false,
          msg: res.data.message,
        });
        setTimeout(() => {
          this.setState({
            showMsg: false,
          });
        }, 4000);
    //   }
    });
  };
  render() {
    return (
      <div>
        <div className="signinpage">
             <div className="signin">
             <h3 style={{ textAlign: "center", marginBottom: "30px" }}>
                Forgot Password
              </h3>
          <form action="" onSubmit={this.sendEmail}>
            <input
              type="email"
              placeholder="Type your mail"
              name="email"
              onChange={this.handleChange}
            />
            <button
              type="submit"
              
              className="forgot_send"
            >
              {this.state.Loading ? "Sending..." : "Send"}
            </button>
            <p style={{ textTransform: "capitalize", marginTop: "8px" }}>
              {this.state.showMsg ? this.state.msg : null}
            </p>
          </form>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}
