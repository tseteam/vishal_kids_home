import React, { Component } from "react";
import Axios from "axios";
import Constants from "../variable/Constants";
import Footer from "../Partials/Footer";
export default class ResetPassword extends Component {
  state = {
    email: "",
    password: "",
    password_confirmation: "",

    success: false,
  };
  resetPass = (e) => {
    e.preventDefault();

    console.log(document.location.href);
    let urltoken = document.location.href;
    var url = new URL(urltoken);
    var token = url.searchParams.get("token");
    console.log(token);
    this.setState({
      loading: true,
    });
    const load = {
      email: this.state.email,
      password: this.state.password,
      password_confirmation: this.state.password_confirmation,
      token: token,
    };
    console.log(load);
    Axios.post(Constants.postUrl.resetPassword, load).then((res) => {
      console.log(res);
      if (res.data.status === "verified") {
        this.setState({
          loading: false,
          // message: res.data.message,
          success: true,
        });
        document.getElementById("myform").reset();
        setTimeout(() => {
          this.setState({ success: false });
        }, 4000);
        // window.location.href = "/sign-in";
      } else {
        this.setState({
          loading: false,
          // message: res.data.message,
          failed: true,
        });
      }
    });
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    return (
      <div>
           <div className="signinpage">
             <div className="signin">
        <form action="" onSubmit={this.resetPass}>
          <input
            type="text"
            placeholder="Email"
            className="inp_border"
            name="email"
            onChange={this.handleChange}
          />
          <input
            type="password"
            placeholder="New Password"
            className="inp_border"
            name="password"
            onChange={this.handleChange}
          />
          <input
            type="password"
            placeholder="Confirm Password"
            className="inp_border"
            name="password_confirmation"
            onChange={this.handleChange}
          />
          <button type="submit" className="forgot_send">
            {this.state.loading ? "Sending..." : "Send"}
          </button>
        </form>
        {this.state.success ? <p>Password reset successfully. </p> : null}
        </div></div>
        <Footer/>
      </div>
    );
  }
}
