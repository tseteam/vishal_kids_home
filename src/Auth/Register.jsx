import React from "react";
import Axios from "axios";
import Constants from "../variable/Constants";
import Notification from "../Components/Notification";
import { Redirect, Link } from "react-router-dom";
import Footer from "../Partials/Footer";

export default class Register extends React.Component {
  state = {
    first_name: "",
    email: "",
    password: "",
    confirmed_password: "",
    phone: "",
    code: false,
    inputcode: "",
    redirect: false,
    notification: false,
    Verifymsg: "",
    loading: false,
    shownotif: false,
    showSuccess: false,
    Message: "",
    enableBtn:false
  };
  submit = (e) => {
    this.setState({ loading: true });
    e.preventDefault();
    const userinfo = {
      name: this.state.first_name,
      email: this.state.email,
      password: this.state.password,
      password_confirmation: this.state.confirmed_password,
      phone_number: "+91" + this.state.phone,
    };
    console.log(userinfo);
    if (
      this.state.first_name === "" ||
      this.state.email === "" ||
      this.state.password === "" ||
      this.state.confirmed_password === "" ||
      this.state.phone === ""
    ) {
    } else {
      Axios.post(Constants.postUrl.register, userinfo).then((res) => {
        console.log(res);

        if (res.data.status === "success") {
          this.setState({
            shownotif: true,
            showMsg: res.data.message,
            loading: false,
          });
          setTimeout(() => {
            this.setState({
              redirect: true,
              shownotif: false,
            });
          }, 1500);
        } else {
          this.setState({
            error: true,
            showMsg: res.data.message,
            loading: false,
          });
          setTimeout(() => {
            document.getElementById("myform").reset();
            this.setState({
              error: false,
          
            });
          }, 1500);
        }
      });
    }
  };
  verify = (e) => {
    let Mnumber = {
      phone_number: "+91" + this.state.phone,
    };
    Axios.post(Constants.postUrl.SendOTP, Mnumber).then((res) => {
      console.log(res);
      if (res.data.status === "success") {
        this.setState({
          showSuccess: true,
          Message: res.data.message,
          code: true,
        });
        setTimeout(() => {
          this.setState({
            showSuccess: false,
          });
        }, 1500);
      } else {
        this.setState({
          error: true,
          Message: res.data.message,
        });
        setTimeout(() => {
          this.setState({
            error: false,
          });
        }, 1500);
      }
    });
  };
  verifyCode = (e) => {
    let Vcode = {
      code: this.state.inputcode,
      phone_number: "+91" + this.state.phone,
    };
    Axios.post(Constants.postUrl.UserVerify, Vcode).then((res) => {
      console.log(res);
      if ((res.data.status = "success")) {
        this.setState({
          enableBtn:true,
          Verifymsg: res.data.status,
          notification: true,
        });
      } else {
        this.setState({
          Verifymsg: res.data.status,
          notification: true,
        });
      }
    });
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/sign-in" />;
    } else {
      return (
        <div id="register">
          {/* <Header />
          <Menu /> */}
          <form onSubmit={this.submit} id="myform">
            <div className="register">
              <div className="registerpage">
                <h3 style={{ textAlign: "center", marginBottom: "30px" }}>
                  Register
                </h3>

                <input
                  type="text"
                  name="first_name"
                  id="fname"
                  placeholder="Username"
                  required
                  onChange={this.handleChange}
                />
                <input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  required
                  onChange={this.handleChange}
                />
                <input
                  type="text"
                  name="phone"
                  id="phone"
                  maxLength="10"
                  required
                  placeholder="Phone (10 digit Number)"
                  // value="+91"
                  onChange={this.handleChange}
                />
                <button onClick={this.verify} className="verify">
                  Verify
                </button>
                <p style={{ color: "green" }}>
                  {" "}
                  {this.state.showSuccess ? this.state.Message : ""}
                </p>
                <p style={{ color: "red" }}>
                  {this.state.error ? this.state.Message : ""}
                </p>

                {this.state.code ? (
                  <input
                    type="text"
                    name="inputcode"
                    placeholder="code"
                    required
                    onChange={this.handleChange}
                  />
                ) : null}
                {this.state.code ? (
                  <button
                    className="submit"
                    type="submit"
                    onClick={this.verifyCode}
                  >
                    Submit
                  </button>
                ) : null}
                <div>
                  {this.state.notification ? (
                    <Notification
                      title={this.state.Verifymsg}
                      duration={4000}
                    />
                  ) : null}
                </div>
                <input
                  type="password"
                  name="password"
                  id="pass"
                  required
                  placeholder="Password"
                  onChange={this.handleChange}
                />
                <input
                  type="password"
                  name="confirmed_password"
                  id="passconfirm"
                  required
                  placeholder="Confirm Password"
                  onChange={this.handleChange}
                />
                
                <button
                  type="submit"
                  className="button"
                 style={{cursor:this.state.enableBtn?"pointer":"not-allowed"}}
                >
                  {this.state.loading ? "Creating..." : "Create"}
                </button>
                <p
                  style={{
                    color: "green",
                    marginBottom: "5px",
                    marginTop: "5px",
                  }}
                  className="center"
                >
                  {this.state.shownotif ? this.state.showMsg : ""}
                </p>
                <p
                  style={{
                    color: "red",
                    marginBottom: "5px",
                    marginTop: "5px",
                  }}
                  className="center"
                >
                  {this.state.error ? this.state.showMsg : ""}
                </p>
                <div className="center">
                  <Link to="/">Return to Store</Link>
                </div>
              </div>
            </div>
          </form>
          <Footer />
        </div>
      );
    }
  }
}
