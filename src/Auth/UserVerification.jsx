import React, { Component } from "react";
import Header from "../Partials/Header";
import Menu from "../Components/Menu";
import Footer from "../Partials/Footer";

export default class UserVerification extends Component {
  state = {
    phone: "",
    code: ""
  };
  submit = e => {
    e.preventDefault();
    const userVerify = {
      phone: this.state.phone,
      code: this.state.code
    };
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(name);
  };
  render() {
    return (
      <div>
        {/* <Header />
        <Menu /> */}
        <form onClick={this.submit}>
          <div className="verification">
            <div className="inputs">
              <input
                type="text"
                name="phone"
                placeholder="Phone"
                onChange={this.handleChange}
              />
              <input
                type="text"
                name="code"
                placeholder="Code"
                onChange={this.handleChange}
              />
              <div className="center">
                <button type="submit">Verify</button>
              </div>
            </div>
          </div>
        </form>
        <Footer />
      </div>
    );
  }
}
