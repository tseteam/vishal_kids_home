import React from "react";
import Axios from "axios";
import BigStore from "./Components/BigStore";
import Footer from "./Partials/Footer";
import bootstrap from "bootstrap";
import { IoIosArrowDown } from "react-icons/io";
import Carousel from "react-bootstrap/Carousel";

export default class About extends React.Component {
  state = {
    about_welcome: [],
    about_clothinformation: [],
    back_img: [],
    clients: [],
    team_section: [],
    team_members: [],
  };
  componentDidMount() {
    let url = "About.json";
    Axios.get(url).then((res) => {
      this.setState({
        about_welcome: res.data.about_welcome,
        about_clothinformation: res.data.about_clothinformation,
        back_img: res.data.back_img,
        clients: res.data.clients,
        team_section: res.data.team_section,
        team_members: res.data.team_members,
      });
    });
  }

  render() {
    return (
      <div className="about">
        {/* <Header />
        <Menu /> */}
        {this.state.about_welcome.map((aboutelement, index) => (
          <div className="container" key={index}>
            <div className="row welcomesection">
              <div className="col-sm-6">
                <img alt="some" src={aboutelement.about_img} width="100%" />
              </div>
              <div className="col-sm-6">
                <h1> {aboutelement.kids_outfit} </h1>
                <p> {aboutelement.about_para} </p>
                <p>{aboutelement.about_para2} </p>

                <div className="col-sm-4 col-xs-4">
                  <img alt="some" src={aboutelement.about_pic} width="100%" />
                </div>
                <div className="col-sm-4 col-xs-4">
                  <img alt="some" src={aboutelement.about_url} width="100%" />
                </div>
                <div className="col-sm-4  col-xs-4  ">
                  <img alt="some" src={aboutelement.about_image} width="100%" />
                </div>
              </div>
            </div>
          </div>
        ))}

        <div className="container trends">
          <h3>Location and Overview:</h3>
          <h5>
            Established in the year 2006, Vishal Kids World in Sitabuldi, Nagpur
            is a top player in the category Children Readymade Garment Retailers
            in the Nagpur. This well-known establishment acts as a one-stop
            destination servicing customers both local and from other parts of
            Nagpur. Over the course of its journey, this business has
            established a firm foothold in it’s industry. The belief that
            customer satisfaction is as important as their products and
            services, have helped this establishment garner a vast base of
            customers, which continues to grow by the day. This business employs
            individuals that are dedicated towards their respective roles and
            put in a lot of effort to achieve the common vision and larger goals
            of the company. In the near future, this business aims to expand its
            line of products and services and cater to a larger client base. In
            Nagpur, this establishment occupies a prominent location in
            Sitabuldi. It is an effortless task in commuting to this
            establishment as there are various modes of transport readily
            available. It is at Modi No. 1, Vishal Kids World, which makes it
            easy for first-time visitors in locating this establishment. It is
            known to provide top service in the following categories: Toy Shops,
            Children Readymade Garment Retailers, Game Dealers, Toy Dealers,
            Baby Care Product Dealers, Battery Operated Toy Car Dealers,
            Children Readymade Garment Wholesalers, Toy Car Dealers.
          </h5>
          <h3>Products and Services offered:</h3>
          <h5>
            Vishal Kids World in Sitabuldi has a wide range of products and / or
            services to cater to the varied requirements of their customers. The
            staff at this establishment are courteous and prompt at providing
            any assistance. They readily answer any queries or questions that
            you may have. Pay for the product or service with ease by using any
            of the available modes of payment, such as Cash, Master Card, Visa
            Card, Debit Cards, American Express Card, Credit Card. This
            establishment is functional from 11:00 - 21:30.
          </h5>
        </div>
        {this.state.back_img.map((single, index) => (
          <div
            key={index}
            className="bannerimgae"
            style={{
              backgroundImage: "url(" + single.bg_img + ")",
            }}
          >
            <div className="container">
              <h4>{single.outfit} </h4>
              <h5>{single.latest_offer} </h5>
              <h6>{single.offer_para} </h6>
              <button> {single.button} </button>
            </div>
          </div>
        ))}

        <div className="clientsection">
          <div className="container">
            {this.state.clients.map((singleclient, index) => (
              <div key={index}>
                <h3 className="ClientSays">{singleclient.client_say} </h3>
                <h4> {singleclient.test} </h4>
                <Carousel
                interval={null}
                className="carousel-control-prev-icon"

                     
                    >
                          <Carousel.Item>
                <div className="quote">
                  <span>
                    <i class="fa fa-quote-left"></i>
                  </span>
                </div>
                <p>
                  Very satisfied with experience... best shop for kids.. i live
                  3 hours away from nagpur and always come here to shop for my
                  kids..very good collection 👍
                </p>
                <div className="center">
                  <span>
                    <img
                      alt="Client Designer"
                      src={singleclient.designer_img}
                    />
                  </span>
                  <span> Sneha Sharma </span>
                </div>
                </Carousel.Item>
                {/* <Carousel.Item>
                <div className="quote">
                  <span>
                    <i class="fa fa-quote-left"></i>
                  </span>
                </div>
                <p>
                Best shop for new born and kids
                </p>
                <div className="center">
                  <span>
                    <img
                      alt="Client Designer"
                      src={singleclient.designer_img}
                    />
                  </span>
                  <span> Yogesh Pathak </span>
                </div>
                </Carousel.Item>
                <Carousel.Item>
                <div className="quote">
                  <span>
                    <i class="fa fa-quote-left"></i>
                  </span>
                </div>
                <p>
                Lovely experience
Staff are very cooperative
                </p>
                <div className="center">
                  <span>
                    <img
                      alt="Client Designer"
                      src={singleclient.designer_img}
                    />
                  </span>
                  <span> DHIRAJ NIMGADE </span>
                </div>
                </Carousel.Item>
               */}
                </Carousel>
              </div>
             ))} 
            <div className="center">
              <div className="spandotactive">
                <span></span>
              </div>
              <div className="spandot">
                <span></span>
              </div>
              <div className="spandot">
                <span></span>
              </div>
            </div>
          </div>
        </div>

        <BigStore />
        <div
          className="container"
          style={{ marginTop: "50px", marginBottom: "50px" }}
        >
          <h3 style={{ marginBottom: "20px" }}>Frequently Asked Question</h3>
          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <h4 class="accordion-header" id="headingOne">
                <button
                  class="accordion-button"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                  1. What are the various mode of payment accepted here ?
                  <IoIosArrowDown style={{ marginLeft: "64px" }} />
                </button>
              </h4>
              <div
                id="collapseOne"
                class="accordion-collapse collapse show"
                aria-labelledby="headingOne"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <h5>
                    {" "}
                    You can make payment Via Cash, Credit Card, Visa Card,
                    Master Card, Cash on Delivery, Card on Delivery, Debit Card,
                    Amex Card, UPI, BHIM, Paytm, RuPay Card, Diners Club Card, G
                    Pay, PhonePe, NEFT, IMPS.
                  </h5>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h4 class="accordion-header" id="headingTwo">
                <button
                  class="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseTwo"
                  aria-expanded="false"
                  aria-controls="collapseTwo"
                >
                  2. Which is the nearest landmark ?
                  <IoIosArrowDown className="accordianArrow" style={{ marginLeft: "45%" }} />
                </button>
              </h4>
              <div
                id="collapseTwo"
                class="accordion-collapse collapse"
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <h5>
                    You can easily locate the establishment as it is in close
                    proximity to Vishal Kids World
                  </h5>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h4 class="accordion-header" id="headingThree">
                <button
                  class="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseThree"
                  aria-expanded="false"
                  aria-controls="collapseThree"
                >
                  3. What are its hours of operation ?
                  <IoIosArrowDown className="accordianArrow" style={{ marginLeft: "44%" }} />
                </button>
              </h4>
              <div
                id="collapseThree"
                class="accordion-collapse collapse"
                aria-labelledby="headingThree"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <h5> The establishment is functional on </h5>
                  <h5> Monday:- 11:00 Am - 9:30 Pm </h5>
                  <h5> Tuesday:- 11:00 Am - 9:30 Pm</h5>
                  <h5> Wednesday:- 11:00 Am - 9:30 Pm</h5>
                  <h5> Thursday:- 11:00 Am - 9:30 Pm</h5>
                  <h5> Friday:- 11:00 Am - 9:30 Pm</h5>
                  <h5> Saturday:- 11:00 Am - 9:30 Pm</h5>
                  <h5> Sunday:- 11:30 Am - 9:00 Pm</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="container">
          <div className="row teamsection">
            <div className="text">
              <h1>Our Team</h1>
              <h2>Meet Our Team Members and Supporters</h2>
              <div className="shortdisc">
                <span></span>
              </div>
            </div>
            {this.state.team_members.map((teamelement, index) => (
              <div className="col-sm-3 col-xs-6">
                <div className="wrapper" key={index}>
                  <div className="layer">
                    <div className="textlayer">
                   

                      <i className="icofont-envelope fb fbicon"></i>
                      <i className="icofont-bag-alt instaicon "></i>
                      <i className="icofont-chat youtubeicon"></i>
                      <i className="icofont-at pintresticon"></i>
                    </div>
                  </div>
                  <img alt="Team" src={teamelement.member_pic} width="100%" />
                </div>
                <h3> {teamelement.name_member} </h3>
                <h4> {teamelement.profession_member} </h4>
              </div>
            ))}
          </div>
        </div>
       */}
        <Footer />
      </div>
    );
  }
}
