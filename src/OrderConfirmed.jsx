import React, { Component } from "react";
import Lottie from "react-lottie";
import animationData from "../src/assets/lottie.json";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

class OrderConfirmed extends Component {
  state = {
    redirect: false
  };

  constructor() {
    super();
    this.state = { isStopped: false, isPaused: false };
  }
  componentDidMount() {
    setInterval(() => {
      this.setState({
        redirect: true
      });
    }, 6000);
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    } else {
      const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
      };
      return (
        <div className="order">
          <Lottie
            options={defaultOptions}
            height={200}
            width={200}
            isStopped={this.state.isStopped}
            isPaused={this.state.isPaused}
          />
          <div className="card">
            <h1>Your order has been confirmed !!!</h1>
            <h5>Thank you for shopping with us.</h5>
            <h5>
              Thanks For Placing your Order! We will send you a notification
              within few days when it ships.
            </h5>
            <span>If You Have Any questions feel free to</span>
            <span>
              <Link to="/contact" style={{ fontWeight: "bold" }}>
                <span> Contact us</span>
              </Link>
            </span>
          </div>
        </div>
      );
    }
  }
}

export default OrderConfirmed;
