import React, { Component } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";
import Constants from "./variable/Constants";
// import login from "../Components/LoginCheck";
// import init from "../helper/WindowToken";
// import Constants from "../variable/Constants";
export default class AllCategories extends Component {
  state = {
    Pdata: []
  };
  componentDidMount() {
    Axios.get(Constants.getUrl.Primary_Categories).then(res => {
      this.setState(
        {
          Pdata: res.data
        },
        () => {
          console.log(this.state.Pdata);
        }
      );
    });
  }

  render() {
    return (
      <div>
        <div className="row">
          {this.state.Pdata.map((element, index) => (
            <li
              key={index}
              onMouseEnter={() => {
                this.show(element.id);
                this.setState({
                  newState: index
                });
              }}
              onMouseLeave={this.hide}
              id={element.id}
              style={{
                display: "flex",
                textAlign: "left",
                backgroundColor: "white"
              }}
            >
              <div className="col-sm-3 col-xs-6">
                <Link
                  className="linkHoverinmenu"
                  style={{
                    textDecoration: "none",
                    color: "black",
                    padding: "5px"
                  }}
                  to={{
                    pathname: "/productlist/" + element.id
                  }}
                >
                  <span
                    onClick={() => {
                      window.location.href = "/productlist/" + element.id;
                    }}
                  >
                    {element.category_name}
                  </span>
                </Link>
              </div>
              {this.state.new && this.state.newState === index ? (
                <div
                  className="row"
                  style={{
                    margin: "0px auto 0px",
                    position: "absolute",
                    top: "10px",
                    left: "30%"
                  }}
                >
                  <div className="col-md-3 col-sm-3">
                    <h5>Shop By Category</h5>

                    {this.state.ternary_Categories.map((tCategory, index) => (
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "white",
                          padding: "0px",
                          display: "block"
                        }}
                        to={{
                          pathname: "/productlist/" + element.id,
                          state: {
                            ternaryId: tCategory.id
                          }
                        }}
                      >
                        <h4
                          onClick={() => {
                            window.location.href = "/productlist/" + element.id;
                          }}
                          key={index}
                        >
                          {tCategory.category_name}
                        </h4>
                      </Link>
                    ))}
                  </div>
                  <div className="col-md-3 col-sm-3">
                    <h5>Shop By Age</h5>
                    {this.state.ageGroup.map((age, index) => (
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "white",
                          padding: "0px",
                          display: "block"
                        }}
                        to={{
                          pathname: "/productlist/" + element.id,
                          state: {
                            ageId: age.id
                          }
                        }}
                      >
                        <h4
                          onClick={() => {
                            window.location.href = "/productlist/" + element.id;
                          }}
                          key={index}
                        >
                          {age.name}
                        </h4>
                      </Link>
                    ))}
                  </div>

                  <div className="col-md-3 col-sm-3">
                    <h5>Shop By Brands</h5>
                    {this.state.brands.map((brand, index) => (
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "white",
                          padding: "0px",
                          display: "block"
                        }}
                        to={{
                          pathname: "/productlist/" + element.id,
                          state: {
                            brandId: brand.id
                          }
                        }}
                      >
                        <h4
                          onClick={() => {
                            window.location.href = "/productlist/" + element.id;
                          }}
                          key={index}
                        >
                          {brand.name}
                        </h4>
                      </Link>
                    ))}
                  </div>
                  <div className="col-md-3 col-sm-3">
                    <img
                      src={this.state.img}
                      alt=""
                      style={{ width: "200px" }}
                    />
                  </div>
                </div>
              ) : null}
            </li>
          ))}
        </div>
      </div>
    );
  }
}
