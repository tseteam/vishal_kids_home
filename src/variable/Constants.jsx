const mainDomain = "https://admin.vishalkids.in/";
// const mainDomain = "http://192.168.29.12:8000/";

const Constants = {
  mainDomain: "https://admin.vishalkids.in/",
  // mainDomain: "http://192.168.29.12:8000/",
  getUrl: {
    product: mainDomain + "api/products?status=enabled",
    ProductDetail: mainDomain + "api/product/",
    cartDetails: mainDomain + "api/carts",
    Primary_Categories: mainDomain + "api/primary-categories?status=enabled",
    Secondary_Categories:
      mainDomain + "api/secondary-categories?status=enabled",
    ternary_Categories: mainDomain + "api/ternary-categories?status=enabled",
    types: mainDomain + "api/type/give",
    addresses: mainDomain + "api/addresses?status=enabled",
    brands: mainDomain + "api/get_all_brand",
    userDetails: mainDomain + "api/user",
    orders: mainDomain + "api/myorders",
    search: mainDomain + "api/search",
    offers: mainDomain + "api/offers",
    proByBrand: mainDomain + "api/brand-name/",
    ageGroup: mainDomain + "api/age-group/give",
    sections: mainDomain + "api/sections?status=enable",
    sectionSecond: mainDomain + "api/section/second?status=enable",
    payUresponse: mainDomain + "api/order/payment/status/",
    homeSliders: mainDomain + "api/homepages",
    Related: mainDomain + "api/get_prod_by_age_group?primary_category_id="
  },
  postUrl: {
    cart: mainDomain + "api/cart/create",
    pinCheck: mainDomain + "api/chk_pincode",
    applyCoupon: mainDomain + "api/apply_coupon",
    cartDelete: mainDomain + "api/cart/delete/",
    register: mainDomain + "api/register",
    login: mainDomain + "api/login",
    SendOTP: mainDomain + "api/send-otp",
    UserVerify: mainDomain + "api/verify-user",
    address: mainDomain + "api/address/create",
    paymentMode: mainDomain + "api/order-place",
    updateUserInfo: mainDomain + "api/user/update/",
    updateAddress: mainDomain + "api/address/update/",
    updateQuantityInCart: mainDomain + "api/quantity-update/",
    forgotPass:mainDomain+"api/password/create"
  }
};
export default Constants;
