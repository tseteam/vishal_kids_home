import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./assets/css/bootstrap.min.css";
import "./assets/css/icofont.min.css";
import "./assets/css/App.css";
import Home from "./Home";
import ProductDetail from "./ProductDetail";
import Checkout from "./Checkout";
import Register from "./Auth/Register";
import SignIn from "./Auth/SignIn";
import ProductList from "./ProductList";
import Contact from "./Contact";
import About from "./About";
import User from "./Components/User";
import PaymentGateway from "./PaymentGateway";
import OrderConfirmed from "./OrderConfirmed";
import PaymentConfirmed from "./PaymentConfirmed";
import PaymentFail from "./PaymentFail";
import PaymentStatus from "./PaymentStatus";
import init from "./helper/WindowToken";
import Header from "./Partials/Header";
import Menu from "./Components/Menu";
import Axios from "axios";
import Constants from "./variable/Constants";
import login from "./Components/LoginCheck";
// import Mobile from "./Mobile";
import PrivacyPolicy from "./PrivacyPolicy";
import ShippingDetails from "./ShippingDetails";
import Cart from "./Cart";
import AllCategories from "./AllCategories";
import ReturnPolicy from "./Components/ReturnPolicy";
import TermsAndConditions from "./Components/Terms&Conditions";
import ForgotPassword from "./Auth/ForgotPassword";
import ResetPassword from "./Auth/ResetPassword";
// import Footer from "./Partials/Footer";
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: [],
      screenWidth: window.innerWidth
    };
  }
  componentDidMount() {
    // this.setState({screenWidth: window.innerWidth})
    if (init() === "success") {
      if (login()) {
        Axios.get(Constants.getUrl.cartDetails).then(res => {
          console.log(res);
          this.setState({
            cartData: res.data,
            emptyCart: res.data.length === 0 ? true : false
          });
        });
      }
    } else {
      // get cart data from localStorage

      const cartData = localStorage.getItem("cart");
      if (cartData) {
        const parseData = JSON.parse(cartData);
        this.setState({
          cartData: parseData,
          emptyCart: parseData.length === 0 ? true : false
        });
      }
    }
  }
  addToCart = (cartData, id) => {
    console.log(cartData);
    this.setState({ cartData });
  };
  deleteCartItem = (cartId, product_variant_id) => {
    if (login()) {
      Axios.post(Constants.postUrl.cartDelete + cartId).then(res => {
        this.setState(
          {
            cartData: res.data.data
          },
          () => {
            console.log("cartData", this.state.cartData);
            let sub_total = 0;
            this.state.cartData.forEach(cart => {
              sub_total += cart.sub_total;
            });
            this.setState({
              sub_total
            });
          }
        );
      });
    } else {
      const cartData = localStorage.getItem("cart");
      if (cartData) {
        const parseData = JSON.parse(cartData);
        let i = 0;
        parseData.forEach(item => {
          console.log(item, product_variant_id);
          if (item.product_variant_id === product_variant_id) {
            parseData.splice(i, 1);
          }
          i++;
        });
        localStorage.setItem("cart", JSON.stringify(parseData));
        console.log(parseData);
        this.setState({ cartData: parseData },() => {
          console.log("cartData", this.state.cartData);
          let sub_total = 0;
          this.state.cartData.forEach(cart => {
            sub_total += cart.sub_total;
          });
          this.setState({
            sub_total
          });
        });
      }
    }
  };
  render() {
    return (
      <div className="App">
        {/* {this.state.screenWidth <= 900 ? (
          <BrowserRouter>
            <Route exact path="/" component={Mobile} />
          </BrowserRouter>
        ) : ( */}
        <BrowserRouter>
          <Header
            cartData={this.state.cartData}
            onDeleteCartItem={this.deleteCartItem}
          />
          <div className="hidefrommain">
            <Menu />
          </div>
          <Route
            exact
            path="/"
            render={props => <Home {...props} onAddToCart={this.addToCart} />}
          />
          <Route
            path="/cart"
            render={props => (
              <Cart {...props} onDeleteCartItem={this.deleteCartItem} />
            )}
          />
          <Route
            path="/productlist/:id"
            render={props => (
              <ProductList {...props} onAddToCart={this.addToCart} />
            )}
          />
          <Route
            path="/product-detail/:id"
            render={props => (
              <ProductDetail {...props} onAddToCart={this.addToCart} />
            )}
          />
          <Route path="/contact" component={Contact} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/register" component={Register} />
          <Route path="/sign-in" component={SignIn} />
          <Route path="/about" component={About} />
          <Route path="/user" component={User} />
          <Route path="/paymentgateway" component={PaymentGateway} />
          <Route path="/orderconfirmed" component={OrderConfirmed} />
          <Route path="/paymentconfirmed" component={PaymentConfirmed} />
          <Route path="/paymentfailed" component={PaymentFail} />
          <Route path="/paymentstatus" component={PaymentStatus} />
          <Route path="/privacypolicy" component={PrivacyPolicy} />
          <Route path="/shippingDetails" component={ShippingDetails} />
          <Route path="/all-categories" component={AllCategories} />
          <Route path="/return-policy" component={ReturnPolicy} />
          <Route path="/terms&conditions" component={TermsAndConditions} />
          <Route path="/forgot-password" component={ForgotPassword} />
          <Route path="/reset-password" component={ResetPassword} />



          {/* <Footer /> */}
        </BrowserRouter>
        {/* )} */}
      </div>
    );
  }
}

// export default App;
