import React, { Component } from "react";
import init from "./helper/WindowToken";
import login from "./Components/LoginCheck";
import Axios from "axios";
import Constants from "./variable/Constants";
import Footer from "./Partials/Footer";
import Lottie from "react-lottie";
import cart from "../src/assets/cartEmpty.json";

export default class extends Component {
  state = {
    cartData: [],
    empty: false,
    sub_total: 0,
    emptyCart: false
  };
  componentDidMount() {
    if (init() === "success") {
      if (login()) {
        Axios.get(Constants.getUrl.cartDetails).then(res => {
          console.log(res);
          this.setState(
            {
              cartData: res.data,
              emptyCart: res.data.length === 0 ? true : false
            },
            () => {
              let sub_total = 0;
              this.state.cartData.forEach(item => {
                sub_total += init() === "success" ? item.sub_total : null;
              });
              this.setState({
                sub_total: Math.round((sub_total * 10) / 10)
              });
            }
          );
          if (this.state.cartData.length > 0) {
            this.setState({ empty: false });
          } else {
            this.setState({ empty: true });
          }
        });
      }
    } else {
      // get cart data from localStorage
      const cartData = localStorage.getItem("cart");
      if (cartData) {
        const parseData = JSON.parse(cartData);
        this.setState(
          {
            cartData: parseData,
            emptyCart: parseData.length === 0 ? true : false
          },
          () => {
            let sub_total = 0;
            this.state.cartData.forEach(item => {
              sub_total += item.price;
            });
            this.setState({
              sub_total: Math.round((sub_total * 10) / 10)
            });
          }
        );
      }
      if (this.state.cartData.length > 0) {
        this.setState({ empty: false });
      } else {
        this.setState({ empty: true });
      }
    }
  }
  changeQuantity = (e, pid, product_id, product_variant_id) => {
    // let userToken = localStorage.getItem("token");
    if (init() === "success") {
      this.setState(
        {
          quantity: e.target.value
        },
        () => {
          const payload = {
            quantity: parseInt(this.state.quantity),
            product_id: product_id,
            product_variant_id: product_variant_id
          };
          console.log(payload);
          Axios.post(
            Constants.postUrl.updateQuantityInCart + pid,
            payload
          ).then(res => {
            console.log(res);
            this.setState({
              basePrice: res.data.sub_total
            });
            window.location.reload();
          });
        }
      );
    } else {
      this.setState(
        {
          quantity: e.target.value
        },
        () => {
          console.log(this.state.quantity);
          const cartData = localStorage.getItem("cart");
          if (cartData) {
            console.log("data present");
            let parsedData = JSON.parse(cartData);
            console.log(parsedData);
            const productObj = parsedData.find(obj => obj.product_id === pid);
            console.log(productObj);
            if (productObj) {
              parsedData.forEach(item => {
                if (item.product_id === pid) {
                  item.quantity = parseInt(this.state.quantity);
                  this.setState(
                    {
                      price: parseInt(this.state.quantity * item.price)
                    },
                    () => {
                      console.log(this.state.price);
                    }
                  );
                }
              });
              // window.location.reload();
            }
          }
        }
      );
    }
  };
  // deleteCart = (e, pVariantId) => {
  //   let cartId = e.target.id;
  //   this.props.onDeleteCartItem(cartId, pVariantId);
  // };
  deleteCartItem = (cartId, variantId) => {
    if (init() === "success") {
      Axios.post(Constants.postUrl.cartDelete + cartId).then(res => {
        console.log(res);
        Axios.get(Constants.getUrl.cartDetails).then(res => {
          console.log(res);
          this.setState({ cartData: res.data, login: true }, () => {
            this.props.onAddToCart(this.state.cartData);
            let subTotal = 0;
            this.state.cartData.forEach(item => {
            subTotal += parseFloat(item.unit_price) * item.quantity;
            });
            this.setState({ subTotal });
            if (this.state.cartData.length > 0) {
              this.setState({ empty: false });
            } else {
              this.setState({ empty: true, discount: 0 });
            }
          });
        });
      });
    } else {
      this.setState({ login: false });
      const cartData = localStorage.getItem("cart");
      if (cartData) {
        const parseData = JSON.parse(cartData);
        let i = 0;
        parseData.forEach(item => {
          if (item.product_variant_id === variantId) {
            parseData.splice(i, 1);
          }
          i++;
        });
        localStorage.setItem("cart", JSON.stringify(parseData));
        console.log(parseData);
        this.setState({ cartData: parseData }, () => {
          console.log(this.state.cartData);
          this.props.onAddToCart(this.state.cartData);
          let subTotal = 0;
          this.state.cartData.forEach(item => {
            subTotal += parseFloat(item.unit_price) * item.quantity;
          });
          this.setState({ subTotal });
          if (this.state.cartData.length > 0) {
            this.setState({ empty: false });
          } else {
            this.setState({ empty: true, discount: 0 });
          }
        });
      }
    }
  };
  handleQuantity = (quantity, cartId, variantId,product_variant_id,product_id) => {
    if (init() === "success") {
      Axios
        .post(Constants.postUrl.updateQuantityInCart + cartId, {
          quantity,product_variant_id,product_id
        })
        .then(res => {
          // console.log(res);
          Axios.get(Constants.getUrl.cartDetails).then(res => {
            console.log(res.data);
            this.setState({ cartData: res.data, login: true }, () => {
              this.props.onAddToCart(this.state.cartData);
              let subTotal = 0;
              this.state.cartData.forEach(item => {
            subTotal += parseFloat(item.unit_price) * item.quantity;
              });
              this.setState({ subTotal });
            });
          });
        });
    } else {
      this.setState({ login: false });
      const cartData = localStorage.getItem("cart");
      if (cartData) {
        const parseData = JSON.parse(cartData);
        let i = 0;
        parseData.forEach(item => {
          if (item.product_variant_id === variantId) {
            item.quantity = quantity;
          }
          i++;
        });
        localStorage.setItem("cart", JSON.stringify(parseData));
        console.log(parseData);
        this.setState({ cartData: parseData }, () => {
          this.props.onAddToCart(this.state.cartData);
          let subTotal = 0;
          this.state.cartData.forEach(item => {
            subTotal += parseFloat(item.unit_price) * item.quantity;
          });
          this.setState({ subTotal });
        });
      }
    }
  };
  // componentDidUpdate() {
  //   if (login()) {
  //     if (this.state.cartData !== this.props.cartData) {
  //       console.log("state", this.state.cartData);
  //       console.log("props", this.props.cartData);
  //       this.setState({ cartData: this.props.cartData }, () => {
  //         let sub_total = 0;
  //         if (this.state.cartData ? this.state.cartData.length > 0 : null) {
  //           this.state.cartData.forEach(item => {
  //             if (login()) {
  //               sub_total += item.sub_total * item.quantity;
  //             }
  //             //   else {
  //             //     sub_total += item.price * item.quantity;
  //             //   }
  //           });
  //         }
  //         this.setState({ sub_total });
  //       });
  //     } else {
  //       if (this.state.cartData !== this.props.cartData) {
  //         console.log("state", this.state.cartData);
  //         console.log("props", this.props.cartData);
  //         this.setState({ cartData: this.props.cartData }, () => {
  //           let sub_total = 0;
  //           if (this.state.cartData ? this.state.cartData.length > 0 : null) {
  //             this.state.cartData.forEach(item => {
  //               // if (login()) {
  //               //   sub_total += item.sub_total * item.quantity;
  //               // } else {
  //               sub_total += item.price * item.quantity;
  //               // }
  //             });
  //           }
  //           this.setState({ sub_total });
  //         });
  //       }
  //     }
  //   }
  // }
  update() {
    window.location.reload();
  }
  render() {
    return (
      <div>
        <h3 style={{ textAlign: "center" }}>My Cart</h3>
        <div className="container CartWrapper">
          <div className="row cartRow">
            <div className="col-sm-2">Product</div>
            <div className="col-sm-2">Name</div>
            <div className="col-sm-2">Price</div>
            <div className="col-sm-2">Quantity</div>
            <div className="col-sm-2">Total</div>
            <div className="col-sm-2">Remove</div>
          </div>
          {!this.state.emptyCart ? (
            <div>
              {this.state.cartData.map((singledetail, index) => {
                let parseImageArray = [];
                if (login()) {
                  parseImageArray = JSON.parse(
                    singledetail.product_variant.image
                  );
                }
                return login() ? (
                  <div>
                    <div className="row borderBottom hideCart" key={index}>
                      <div className="col-sm-2 col-xs-4">
                        <img alt="some" src={parseImageArray[0]} width="100%" />
                      </div>
                      <div className="col-sm-2 col-xs-8">
                        <h2 className="cartProductName">
                          {" "}
                          {singledetail.product.product_name}
                        </h2>
                      </div>
                      <div className="col-sm-2 col-xs-2">
                        <h2 className="cartProductPrice">
                          ₹{singledetail.product_variant.price}
                          {/* x{" "}
                    {singledetail.quantity} */}
                        </h2>
                      </div>
                      <div className="col-sm-2">
                        <h2 className="cartSelect ">
                          <select
                            className="selectTag"
                            name="quantity"
                            onChange={e => {
                              this.changeQuantity(
                                e,
                                singledetail.id,
                                singledetail.product_id,
                                singledetail.product_variant.id
                              );
                            }}
                            value={singledetail.quantity}
                            key={index}
                          >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                          </select>
                        </h2>
                      </div>
                      <div className="col-sm-2">
                        <h2
                          className="cartProductPrice"
                          style={{ fontWeight: "bold" }}
                        >
                          ₹
                          {singledetail.product_variant.price *
                            singledetail.quantity}
                          {/* {singledetail.product_variant.price
                      ? singledetail.product_variant.price
                      : this.state.basePrice} */}
                          {/* {this.state.basePrice} */}
                        </h2>
                      </div>
                      <div className="col-sm-2">
                        <h2 className="delete">
                          <i
                            className="far fa-trash-alt cartDelete"
                            type="submit"
                            id={singledetail.id}
                            onClick={e => {
                              this.deleteCart(
                                e,
                                singledetail.product_variant_id
                              );
                            }}
                          ></i>
                        </h2>
                      </div>
                    </div>
                    <div className="row borderBottom showcart">
                      <div className="col-sm-2 col-xs-4">
                        <img alt="some" src={parseImageArray[0]} width="100%" />
                        <h2 className="cartSelect ">
                          <span style={{ fontSize: "13px" }}>Qty :</span>
                          <select
                            className="selectTag"
                            name="quantity"
                            onChange={e => {
                              this.changeQuantity(
                                e,
                                singledetail.id,
                                singledetail.product_id,
                                singledetail.product_variant.id
                              );
                            }}
                            value={singledetail.quantity}
                            key={index}
                          >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                          </select>
                        </h2>
                      </div>
                      <div className="col-sm-2 col-xs-8">
                        <h2 className="cartProductName">
                          {" "}
                          {singledetail.product.product_name}
                        </h2>
                        <div className="row">
                          <div className="col-xs-6">
                            <h2 className="cartProductPrice">
                              ₹{singledetail.product_variant.price}
                              {/* x{" "}
                    {singledetail.quantity} */}
                            </h2>
                          </div>
                          {/* <div className="col-xs-6">
                            <h2 className="cartSelect ">
                              <select
                                className="selectTag"
                                name="quantity"
                                onChange={e => {
                                  this.changeQuantity(
                                    e,
                                    singledetail.id,
                                    singledetail.product_id,
                                    singledetail.product_variant.id
                                  );
                                }}
                                value={singledetail.quantity}
                                key={index}
                              >
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                              </select>
                            </h2>
                         
                          </div>
                      */}
                        </div>
                        <h2
                          className="cartProductPrice"
                          style={{ fontWeight: "bold" }}
                        >
                          Total : ₹
                          {singledetail.product_variant.price *
                            singledetail.quantity}
                        </h2>
                      </div>

                      <div className="col-sm-2">
                        <h2 className="delete">
                          <i
                            style={{
                              border: "1px solid #ddd",
                              padding: "10px",
                              borderRadius: "5px"
                            }}
                            className="far fa-trash-alt cartDelete"
                            type="submit"
                            id={singledetail.id}
                            onClick={e => {
                              this.deleteCart(
                                e,
                                singledetail.product_variant.id
                              );
                            }}
                          ></i>
                        </h2>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="row borderBottom" key={index}>
                    <div className="col-sm-2 col-xs-2">
                      <img alt="some" src={singledetail.img_url} width="100%" />
                    </div>
                    <div className="col-sm-2 col-xs-3">
                      <h2 className="cartProductName">
                        {" "}
                        {singledetail.product_name}{" "}
                      </h2>
                    </div>
                    <div className="col-md-2 col-xs-3">
                      <h2 className="cartProductPrice">
                        ₹{singledetail.price}
                      </h2>
                    </div>
                    <div className="col-md-2 col-xs-2">
                      <h2 className="cartSelect ">
                        <select
                          className="selectTag"
                          name="quantity"
                          onChange={e => {
                            this.changeQuantity(e, singledetail.product_id);
                          }}
                          value={singledetail.quantity}
                          key={index}
                        >
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                        </select>
                      </h2>
                    </div>
                    <div className="col-sm-2">
                      <h2 className="cartProductPrice">
                        ₹{singledetail.price * singledetail.quantity}
                        {/* {this.state.price} */}
                      </h2>
                    </div>
                    <div className="col-md-2 col-xs-2">
                      <h2 className="delete">
                        <i
                          className="far fa-trash-alt cartDelete"
                          type="submit"
                          id={singledetail.id}
                          onClick={e => {
                            this.deleteCart(e, singledetail.product_variant_id);
                          }}
                        ></i>
                      </h2>
                    </div>
                  </div>
                );
              })}
              <div className="textRight">
                <h2 className="cartspanCart">
                  Total : ₹
                  <span style={{ marginLeft: "0px" }}>
                    {this.state.sub_total}
                  </span>
                </h2>
                {/* <button
                  className="cartCheckOut"
                  type="submit"
                  // onClick={() => {
                  //   this.update();
                  // }}
                >
                  Update Cart
                </button> */}
                <button
                  className="cartCheckOut"
                  type="submit"
                  onClick={() => {
                    window.location.href = "/checkout";
                  }}
                >
                  Check Out
                </button>
              </div>
            </div>
          ) : (
            <div>
              {this.state.empty ? (
                <div>
                  <Lottie
                    options={{
                      loop: true,
                      animationData: cart,
                      rendererSettings: {
                        preserveAspectRatio: "xMidYMid slice"
                      }
                    }}
                    height={200}
                    width={200}
                  />
                  <h6 style={{ textAlign: "center" }}>Your Cart is Empty!</h6>
                </div>
              ) : null}
            </div>
          )}
        </div>
        <Footer />
      </div>
    );
  }
}
