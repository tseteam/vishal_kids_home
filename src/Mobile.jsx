import React, { Component } from 'react'

export default class Mobile extends Component {
    render() {
        return (
            <div style={{height: '100vh', width:'100%', backgroundColor:'#f00181'}}>
            <div className='mobileContentWrapper'>
            <img
                    alt="some"
                    src="https://vishalkids.in/vishal-logo.png"
                    width="50%"
                  />
                  <h3>Sorry for the Inconvenience!</h3>
                  <h4>Our technical team are working on mobile version. Please visit desktop version.</h4>
            </div>
            </div>
        )
    }
}
