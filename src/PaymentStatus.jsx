import React, { Component } from "react";
import Lottie from "react-lottie";
import animationData from "../src/assets/loader.json";
import Axios from "axios";
import Constants from "./variable/Constants.jsx";
import { Redirect } from "react-router-dom";

class PaymentStatus extends Component {
  state = {
    redirect: false,
    redirectTofail: false
  };
  constructor() {
    super();
    this.state = { isStopped: false, isPaused: false };
  }
  componentDidMount() {
    // console.log(this.props);
    const statusInterval = setInterval(() => {
      Axios.get(
        Constants.getUrl.payUresponse + this.props.location.orderId
      ).then(res => {
        console.log(res.data);
        if (res.data.payment_status === "Completed") {
          clearInterval(statusInterval);
          this.setState({
            redirect: true
          });
        } else if (res.data.payment_status === "Failed") {
          // console.log("fail");
          clearInterval(statusInterval);

          this.setState({
            redirectTofail: true
          });
        }
      });
    }, 3000);
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/paymentconfirmed" />;
    } else if (this.state.redirectTofail) {
      return <Redirect to="/paymentfailed" />;
    } else {
      const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
      };
      return (
        <div className="paymentStatus">
          <h2
            style={{
              textAlign: "center",
              marginTop: "50px",
              fontFamily: "cursive"
            }}
          >
            Payment status pending.....
          </h2>
          <Lottie
            options={defaultOptions}
            height={200}
            width={200}
            isStopped={this.state.isStopped}
            isPaused={this.state.isPaused}
          />
        </div>
      );
    }
  }
}

export default PaymentStatus;
