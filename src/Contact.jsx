import React, { Component } from "react";
import Axios from "axios";
import Footer from "./Partials/Footer";
import { GrDirections } from "react-icons/gr";
import { SiMinutemailer } from "react-icons/si";
import { HiPhone } from "react-icons/hi";
import { AiOutlineFieldTime } from "react-icons/ai";
export default class Contact extends Component {
  state = {
    fullname: "",
    email: "",
    phone: "",
    message: "",
    icons: [],
  };
  componentDidMount() {
    let url = "Contact.json";
    Axios.get(url).then((res) => {
      console.log(res);

      this.setState({
        icons: res.data.icons,
      });
    });
  }
  submit = () => {
    const contact = {
      fullname: this.state.fullname,
      email: this.state.email,
      phone: this.state.phone,
      message: this.state.message,
    };
    console.log(contact);
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    return (
      <div>
        {/* <Header />
        <Menu /> */}
        <div className="ContactForm">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-xs-3">
                <div className="firstsection">
                  <p className="round">
                    <GrDirections
                      style={{
                        zIndex: 9999,
                        position: "absolute",
                        fontSize: "24px",
                        left: "14px",
                        top: "12px",
                      }}
                    />
                  </p>
                  <p className="info">
                    Vishal Kids world vishal kids world, modi mo.1, Sitabuldi,
                    Nagpur, Maharashtra 440012
                  </p>
                </div>
              </div>
              <div className="col-md-3 col-xs-3">
                <div className="firstsection">
                  <p className="round">
                    <SiMinutemailer
                      style={{
                        zIndex: 9999,
                        position: "absolute",
                        fontSize: "24px",
                        left: "14px",
                        top: "12px",
                      }}
                    />
                  </p>
                  <p className="info">
                   vishalkidsworld1008@gmail.com
                  </p>
                </div>
              </div>
              <div className="col-md-3 col-xs-3">
                <div className="firstsection">
                  <p className="round">
                    <HiPhone
                      style={{
                        zIndex: 9999,
                        position: "absolute",
                        fontSize: "24px",
                        left: "14px",
                        top: "12px",
                      }}
                    />
                  </p>
                  <p className="info">
                  +91-9552729086
                  </p>
                </div>
              </div>
              <div className="col-md-3 col-xs-3">
                <div className="firstsection">
                  <p className="round">
                    <AiOutlineFieldTime
                      style={{
                        zIndex: 9999,
                        position: "absolute",
                        fontSize: "24px",
                        left: "14px",
                        top: "12px",
                      }}
                    />
                  </p>
                  <p className="info">
                  11:00 Am - 9:30 Pm
                  </p>
                </div>
              </div>
            
            </div>
          </div>
          <div className="row padd">
            <div className="col-md-6">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29768.8338057148!2d79.07873882881454!3d21.148250099746384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4c17e2f0e2c27%3A0x13dda0b118501f80!2sVishal%20Kids%20world!5e0!3m2!1sen!2sin!4v1617969111559!5m2!1sen!2sin"
                width="600"
                height="450"
                allowfullscreen=""
                className="map"
                loading="lazy"
              ></iframe>
            </div>
            <div className="col-md-6">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29768.834167591536!2d79.07873881953145!3d21.14824829923829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4c0d1cf43f947%3A0xec02e447ae4abeb7!2sVishal%20Kids%20Wear!5e0!3m2!1sen!2sin!4v1617969374768!5m2!1sen!2sin"
                width="300"
                height="300"
                allowfullscreen=""
                className="map"
                loading="lazy"
              ></iframe>
            </div>
          </div>
          <div className="container">
            <div className="row rowpadd">
              <div className="border">
                <div className="col-md-6">
                  <h3 className="heading">SEND US A MESSAGE</h3>

                  <input
                    type="text"
                    name="fullname"
                    placeholder="Name"
                    className="inputboxes"
                    onChange={this.handleChange}
                  />
                  <input
                    type="text"
                    name="email"
                    placeholder="Email"
                    className="inputboxes"
                    onChange={this.handleChange}
                  />
                  <input
                    type="text"
                    name="phone"
                    placeholder="Phone"
                    className="inputboxes"
                    onChange={this.handleChange}
                  />
                </div>
                <div className="col-md-6">
                  <textarea
                    name="message"
                    id="message"
                    placeholder="Message"
                    onChange={this.handleChange}
                  ></textarea>
                  <button onClick={this.submit} className="contact-button">
                    Send
                  </button>
                </div>
              </div>
            </div>
            {/* <div className="iconsection">
              <ul>
                <li>
                  <i className="icofont-facebook icon">F</i>
                </li>
                <li>
                  <i className="icofont-twitter">T</i>
                </li>
                <li>
                  <i className="icofont-google-plus">G+</i>
                </li>
                <li>
                  <i className="icofont-brand-youtube">Y</i>
                </li>
                <li>
                  <i className="icofont-pinterest">P</i>
                </li>
              </ul>
            </div> */}
          </div>
        </div>
        <Footer />
        {/* <Brand_slider /> */}
      </div>
    );
  }
}
