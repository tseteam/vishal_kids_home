import React from "react";
import Axios from "axios";

export default class Footer extends React.Component {
  state = {
    footer: [],
    copyright: [],
    footerbgimg: []
  };
  componentDidMount() {
    let url = "/Footer.json";
    Axios.get(url).then(res => {
      // console.log(res);
      this.setState({
        footer: res.data.footer,
        copyright: res.data.copyright,
        footerbgimg: res.data.footerbgimg
      });
    });
  }

  render() {
    return (
      <div>
        <div>
          <img
            src="https://cdn.shopify.com/s/files/1/0033/7064/7667/files/kidslife-footer-bg.png?v=1528185765"
            width="100%"
          />
        </div>
        {this.state.footerbgimg.map((footerbg, index) => (
          <div
            key={index}
            className="footer"
            // style={{ backgroundImage: "url(" + footerbg.url + ")" }}
            style={{
              backgroundImage:
                "url(https://cdn.shopify.com/s/files/1/0033/7064/7667/files/footer_49911034-0258-47c4-8556-f2ff6d55d388.png?v=1531811005)"
            }}
          >
            <div>
              <div className="container">
                <div className="row rowmargin">
                  <div className="col-sm-3">
                    <h2> Conatct Us </h2>
                    <img
                      alt="some"
                      src="https://cdn.shopify.com/s/files/1/0033/7064/7667/files/blur.png?v=1528187819"
                    />
                    <h5>
                      {" "}
                      Modi Number 1, Sitabuldi, Nagpur, Maharashtra 440012{" "}
                    </h5>

                    <h5> 094223 68510 </h5>

                    <h5> @gmail.com </h5>
                  </div>
                  <div className="col-sm-3">
                    <h2>Help </h2>
                    <img
                      alt="some"
                      src="https://cdn.shopify.com/s/files/1/0033/7064/7667/files/green.png?v=1528187828"
                    />
                    <h5> Search</h5>

                    <h5> Information</h5>

                    <a href="/privacypolicy">
                      {" "}
                      <h5> Privacy Policy </h5>
                    </a>
                    <a href="/shippingDetails">
                      <h5>Shipping Details</h5>
                    </a>
                  </div>
                  <div className="col-sm-3">
                    <h2>Support </h2>
                    <img
                      alt="some"
                      src="https://cdn.shopify.com/s/files/1/0033/7064/7667/files/pink.png?v=1528187838"
                    />
                    <a href="/contact">
                      {" "}
                      <h5>Contact us</h5>
                    </a>

                    <a href="/about">
                      {" "}
                      <h5> About Us</h5>
                    </a>
                    <a href="/terms&conditions">
                      {" "}
                      <h5> Terms And Conditions </h5>
                    </a>
                    <a href="/privacypolicy">
                      {" "}
                      <h5> Privacy Policy </h5>
                    </a>
                    <a href="/shippingDetails">
                      <h5>Shipping Details</h5>
                    </a>
                    <h5>Carrers</h5>
                   <a href="/return-policy"><h5>Return Policy</h5></a> 
                   
                  </div>
                  <div className="col-sm-3">
                    <h2>Information </h2>
                    <img
                      alt="some"
                      src="https://cdn.shopify.com/s/files/1/0033/7064/7667/files/yellow.png?v=1528187847"
                    />

                    <h5>Help & FAQ's </h5>

                    <a href="/contact">
                      {" "}
                      <h5>Store Location</h5>
                    </a>

                    <h5>Orders and Returns </h5>
                  </div>
                </div>
              </div>
            </div>
            {this.state.copyright.map((element, index) => (
              <div key={index}>
                <h4> {element.copyright_title}</h4> 
                <i className="icofont-visa-alt"></i>
                <i className="icofont-paypal-alt"></i>
                <i className="icofont-mastercard-alt"></i>
              </div>
            ))}
            {/* <div>
              <div className="container">
                <div className="row rowmargin">
                  {this.state.footer.map((singlefooter, index) => (
                    <div className="col-sm-3" key={index}>
                      <h2> {singlefooter.help_section} </h2>
                      <img alt="some" src={singlefooter.help_img} />
                      <h5> {singlefooter.help_search} </h5>
                      <a href="/about">
                        {" "}
                        <h5> {singlefooter.help} </h5>
                      </a>
                      <h5> {singlefooter.help_info} </h5>
                      <a href="/contact">
                        <h5> {singlefooter.help_privacy} </h5>
                      </a>
                      <h5> {singlefooter.help_details} </h5>
                    </div>
                  ))}
                </div>
              </div>
            </div> */}
            {/* {this.state.copyright.map((element, index) => (
              <div key={index}>
                <h4> {element.copyright_title}</h4>
                <i className="icofont-visa-alt"></i>
                <i className="icofont-paypal-alt"></i>
                <i className="icofont-mastercard-alt"></i>
              </div>
            ))} */}
          </div>
        ))}
       
      </div>
    );
  }
}
