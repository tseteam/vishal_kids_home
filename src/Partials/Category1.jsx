import React from "react";
import { Link } from "react-router-dom";

export default class Category1 extends React.Component {
  state = {
    primary_category_id: 0,
    pcatName: ""
  };
  componentDidMount() {
    // console.log(this.props.id);
    this.setState(
      {
        primary_category_id: this.props.id,
        pcatName: this.props.pcatName
      },
      () => {
        // console.log(this.state.pcatName);
      }
    );
  }
  render() {
    return (
      <div className="category">
        {/* <Link
          to={
            ("/productlist/" + this.state.primary_category_id,
            (this.state = { pcatName: this.state.pcatName }))
          }
        > */}
        <Link
          to={{
            pathname: "/productlist/" + this.state.primary_category_id,
            state: { pcatName: this.state.pcatName }
          }}
        >
          <div className="categorycontents">
            <div className="layeronimage"> </div>
            <div className="hidenn">
              <img alt="some" src={this.props.url} width="100%" />
            </div>
            <div className="categorytext">
              <h2>{this.props.title} </h2>
              <h3>{this.props.offer}% OFF </h3>
              {/* <button> {this.props.button} </button> */}
              {/* <button type="submit"></button> */}
            </div>
          </div>
        </Link>
      </div>
    );
  }
}
