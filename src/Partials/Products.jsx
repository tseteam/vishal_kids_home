import React from "react";
import { Link } from "react-router-dom";
import Constants from "../variable/Constants";
import Axios from "axios";
import login from "../Components/LoginCheck";
import addToCartWithoutLogin from "../Components/WithoutLoginCartHandler";

export default class Products extends React.Component {
  state = {
    product_id: 0,
    quantity: 1
  };
  componentDidMount() {
    // console.log(this.props.singleproduct);
    // console.log(this.props.product_variant_id);
    // const product_id = this.props.singleproduct.id;
    // console.log(product_id);
    //
  }
  addToCart = e => {
    let payload = {
      product_id: this.props.singleproduct.id,
      product_variant_id: this.props.product_variant_id,
      quantity: this.state.quantity,
      product_name: this.props.name,
      img_url: this.props.url,
      price: this.props.price
    };
    console.log(login());
    if (login()) {
      Axios.post(Constants.postUrl.cart, payload).then(resp => {
        console.log(resp);
        Axios.get(Constants.getUrl.cartDetails).then(res => {
          this.setState(
            {
              cartDetails: res.data
            },
            () => {
              this.props.onAdd(res.data);
            }
          );
        });
      });
    } else {
      if (addToCartWithoutLogin(payload) === "success") {
        const localData = localStorage.getItem("cart");
        const parsedData = JSON.parse(localData);
        // this.setState({ cartDetails: parsedData });
        this.props.onAdd(parsedData);
      }
    }
  };
  render() {
    return (
      <div className="col-sm-3 product col-xs-6">
        <div className="mainCard">
          <div className="wrapper">
            <Link
              to={{ pathname: "product-detail/" + this.props.product_id }}
              style={{ textDecoration: "none" }}
            >
              <img alt="some" src={this.props.url} />
              <h4> {this.props.name} </h4>
              {/* <h5>Brand : {this.props.brand} </h5> */}
              <h3> ₹ {this.props.price} </h3>
            </Link>
          </div>
          <div className="addToCartBtn">
            <button type="submit" onClick={this.addToCart}>
              Add To Cart
              {/* <i class="fa fa-shopping-cart"></i> */}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
