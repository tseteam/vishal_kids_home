import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Axios from "axios";
import Constants from "../variable/Constants";
import login from "../Components/LoginCheck";
import init from "../helper/WindowToken";
import Menu from "../Components/Menu";
// import { white } from "color-name";
import cat from "../assets/images/menu.png";

// import { url } from "inspector";
export default class Header extends Component {
  state = {
    userinfo: {
      name: ""
    },
    search: "",
    login: false,
    sub_total: 0,
    redirect: false,
    cartData: [],
    Primary_Categories: [],
    products: [],
    userDetails: [],
    showSuggestion: false,
    visible: false,
    showSearchs: false,
    empty: false,
    scrolled: false
  };
  componentDidMount() {
    if (init() === "success") {
      if (login()) {
        Axios.get(Constants.getUrl.userDetails).then(res => {
          this.setState({
            userDetails: res.data
          });
          console.log("user", res.data);
        });
      }
    }
  }

  deleteCart = (e, pVariantId) => {
    let cartId = e.target.id;
    this.props.onDeleteCartItem(cartId, pVariantId);
  };
  formSubmit = e => {
    e.preventDefault();
    this.setState({
      redirect: true
    });
  };
  renderRedirect = (e, pcategory) => {
    if (window.location.href.indexOf("productlist") > -1) {
      this.props.onSelect(pcategory);
    } else {
      this.setState({
        redirect: true
      });
    }
  };
  handleInput = e => {
    this.setState(
      {
        search: e.target.value
      },
      () => {
        console.log(this.state.search.length);
      }
    );
    if (this.state.search.length >= 3) {
      this.setState({ showSuggestion: true });
      Axios.get(
        Constants.getUrl.search +
          "?primary_category=true&q=" +
          this.state.search
      ).then(res => {
        console.log(res.data);
        this.setState(
          {
            Primary_Categories: res.data.primary_categories,
            // show: true,
            products: res.data.products
          },
          () => {
            // console.log(this.state.Primary_Categories);
          }
        );
      });
    } else {
      this.setState({
        showSuggestion: false
      });
    }
  };
  showMenu() {
    this.setState({
      visible: true
    });
  }

  hideMenu() {
    this.setState({
      visible: false
    });
  }
  logout = e => {
    localStorage.clear();
    window.location.href = "/";
  };
  showSearch() {
    this.setState({
      showSearchs: !this.state.showSearchs
    });
  }
  componentDidUpdate() {
    // if(login()){
    if (this.state.cartData !== this.props.cartData) {
      console.log("state", this.state.cartData);
      console.log("props", this.props.cartData);
      this.setState(
        {
          cartData: this.props.cartData,
          empty: this.state.cartData.length === 0 ? true : false
        },
        () => {
          let sub_total = 0;
          if (this.state.cartData ? this.state.cartData.length > 0 : null) {
            this.state.cartData.forEach(item => {
              if (login()) {
                sub_total += item.sub_total * item.quantity;
              } else {
                sub_total += item.price * item.quantity;
              }
            });
          }
          this.setState({ sub_total });
        }
      );
    }
    // }
    // else{
    //     if (this.state.cartData !== this.props.cartData) {
    //   console.log("state", this.state.cartData);
    //   console.log("props", this.props.cartData);
    //   this.setState({ cartData: this.props.cartData?this.props.cartData:this.state.empty,empty:this.state.cartData.length===0?true:false }, () => {
    //     let sub_total = 0;
    //     if(this.state.cartData){
    //     this.state.cartData.forEach(item => {
    //       if (login()) {
    //         sub_total += item.sub_total * item.quantity;
    //       } else {
    //         sub_total += item.price * item.quantity;
    //       }
    //     });
    //     }
    //     this.setState({ sub_total });
    //   });
    // }
    // }
  }
  handleSuggestion = () => {
    this.setState({ showSuggestion: false });
  };
  // componentWillMount() {
  //   if (window.innerHeight === 750) {
  //     this.setState({ visible: false });
  //   }
  //   // if (window.innerWidth < 1199) {
  //   //   this.setState({ navClose: { right: "-370px" } });
  //   // }
  // }
  render() {
    if (this.state.redirect) {
      return (
        <Redirect
          to={{
            pathname: "/productlist/" + this.state.pcategoryId,
            state: {
              search: this.state.search,
              pcategoryId: this.state.Primary_Categories[0].id,
              pcategoryName: this.state.Primary_Categories[0].category_name
            }
          }}
        />
      );
    }
    return (
      <div>
        <div className="header">
          <div className="container-fluid">
            <div className="row rowmargin">
              <div className="inputmarg">
                <div className="showsidebar">
                  <div className="menuclass">
                    <h5
                      onClick={() => {
                        this.showMenu();
                      }}
                      style={{ marginLeft: "-24px" }}
                    >
                      <i class="fas fa-bars"></i>
                    </h5>
                    {this.state.visible ? (
                      <div
                        className="sidebarWrapper"
                        style={{ left: 0 }}
                        // style={{
                        //   position: "absolute",
                        //   overflow: "hidden",
                        //   top: 0,
                        //   bottom: 0,
                        //   right: 0,
                        //   left: 0,
                        //   background: "rgba(0, 0, 0, 0.27)"
                        // }}
                      >
                        <div
                          onClick={() => {
                            this.hideMenu();
                          }}
                          style={{
                            // color: "black",
                            marginLeft: "195px",
                            // fontSize: "17px",
                            height: "100vh",
                            width: "131px"
                          }}
                          // onClick={this.closeNav.bind(this)}
                        ></div>

                        <div className="somecontents" style={{ left: 0 }}>
                          <div className="filter-back">
                            <span
                              onClick={() => {
                                this.hideMenu();
                              }}
                              style={{
                                color: "black",
                                marginLeft: "13px",
                                fontSize: "17px"
                              }}
                              // onClick={this.closeNav.bind(this)}
                            >
                              {/* <i
                              className="fa fa-angle-left"
                              aria-hidden="true"
                            ></i>{" "}
                            Back */}
                              <i
                                class="icofont-close-line"
                                style={{ marginLeft: "167px" }}
                              ></i>
                            </span>
                          </div>
                          {/* <p
                          className="hoverclass"
                          onClick={() => {
                            this.hideMenu();
                          }}
                          style={{
                            backgroundColor: "white",
                            marginBottom: "0px"
                          }}
                        >
                          <i class="fas fa-times"></i>
                        </p> */}
                          <Menu />
                        </div>
                      </div>
                    ) : null}
                    {/* <p
                    className="hoverClass"
                    style={{
                      color: "black",
                      marginLeft: "500%",
                      marginTop: "-20px"
                    }}
                    onClick={() => {
                      this.hideMenu();
                    }}
                  >
                    <i class="fas fa-times"></i>
                  </p> */}
                    {/* {this.state.visible ? (
                    <div className="somecontents" style={{ left: "-300px" }}>
                      <Menu />
                    </div>
                  ) : null} */}
                  </div>
                </div>
                <div className="col-sm-4 col-xs-4 col-md-4">
                  <div style={{ textAlign: "center" }} className="newclass">
                    <img
                      alt="some"
                      src="https://vishalkids.in/vishal-logo.png"
                      width="40%"
                      // margin-bottom= "-30px"
                      // margin-top= "-40px"

                      className="logoimage"
                    />
                  </div>
                </div>

                <div className="col-sm-4 col-xs-4 col-md-4">
                  {/* <form> */}
                  <span className="searchIcon">
                    <i className="fas fa-search"></i>
                  </span>

                  <input
                    className="inputserachbox"
                    value={this.state.search}
                    onChange={e => {
                      this.handleInput(e);
                    }}
                    type="text"
                    placeholder="Search for Products or Categories..."
                  />

                  {/* <div className="row"> */}
                  {this.state.showSuggestion ? (
                    <div className="searchBoxWrapper">
                      <div
                        onClick={() => {
                          this.setState({ showSuggestion: false });
                        }}
                        style={{
                          height: "100vh",
                          width: "100% "
                          // background: "white"
                        }}
                      ></div>
                      <div className="searchModel">
                        {this.state.Primary_Categories.length === 0
                          ? this.state.products.map((product, index) => {
                              const parseImageArray = JSON.parse(
                                product.product_variant[0].image
                              );

                              return (
                                <Link
                                  to={{
                                    pathname: "/product-detail/" + product.id
                                  }}
                                  // onMouseLeave={this.setState({ show: false })}
                                  key={index}
                                  onClick={this.handleSuggestion}
                                >
                                  {/* {this.state.show ? ( */}
                                  <div className={"row"}>
                                    <div className="col-md-3 col-3  ">
                                      <img
                                        src={parseImageArray[0]}
                                        alt=""
                                        width="50px"
                                      />
                                    </div>
                                    <div className="col-md-6 col-6">
                                      <h5
                                        style={{
                                          marginTop: " 8px",
                                          display: "block"
                                        }}
                                      >
                                        {" "}
                                        {product.product_name}
                                      </h5>
                                    </div>
                                    <div className="col-md-3 col-3">
                                      <h5
                                        style={{
                                          marginTop: " 8px",
                                          display: "block"
                                        }}
                                      >
                                        ₹ {product.product_variant[0].price}
                                      </h5>
                                    </div>
                                  </div>
                                  {/* ) : null} */}
                                </Link>
                              );
                            })
                          : this.state.Primary_Categories.length !== 0
                          ? this.state.Primary_Categories.map(
                              (pcategory, index) => (
                                <div
                                  key={index}
                                  className="row"
                                  style={{ marginBottom: "8px" }}
                                >
                                  <Link
                                    to={{
                                      pathname: "/productlist/" + pcategory.id,
                                      state: {
                                        pcatName: pcategory.category_name
                                      }
                                    }}
                                    onClick={this.handleSuggestion}
                                  >
                                    <div className="col-md-2">
                                      <img
                                        src={pcategory.image}
                                        alt=""
                                        width="60px"
                                      />
                                    </div>
                                    <div className="col-md-10">
                                      <h5
                                        className=""
                                        // onClick={() => {
                                        //   window.location.href =
                                        //     "/productlist/" + pcategory.id;
                                        // }}
                                      >
                                        {pcategory.category_name}
                                      </h5>
                                    </div>
                                  </Link>
                                </div>
                              )
                            )
                          : "Search result not found"}
                      </div>
                    </div>
                  ) : null}
                  <i className="icofont-search"></i>
                  {/* </form> */}
                </div>
                <i
                  class="fa fa-search searchIconNavbar"
                  onClick={() => {
                    this.showSearch();
                  }}
                ></i>
                <div
                  className="col-sm-4 col-xs-4 col-md-4"
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="dropdown" style={{ textAlign: "center" }}>
                    <h5>
                      {this.state.cartData.length}
                      <i
                        style={{ marginLeft: "4px", color: "orange" }}
                        className="fas fa-shopping-bag"
                      ></i>
                    </h5>
                    {/* {!this.state.empty ? ( */}
                    <div className="drop dropdown-contents">
                      <div className="mini-product-list">
                        {this.state.cartData.map((singledetail, index) => {
                          let parseImageArray = [];
                          if (login()) {
                            parseImageArray = JSON.parse(
                              singledetail.product_variant.image
                            );
                          }
                          return login() ? (
                            <div className="row border" key={index}>
                              <div className="col-sm-3 col-xs-3">
                                <img
                                  alt="some"
                                  src={parseImageArray[0]}
                                  width="100%"
                                />
                              </div>
                              <div className="col-sm-6 col-xs-6">
                                <h2 style={{ textAlign: "left" }}>
                                  {" "}
                                  {singledetail.product.product_name}{" "}
                                </h2>
                                <h2
                                  style={{
                                    textAlign: "left",
                                    marginTop: "8px",
                                    color: "#ef0081"
                                  }}
                                >
                                  ₹{singledetail.product_variant.price} x{" "}
                                  {singledetail.quantity}
                                </h2>
                              </div>
                              {/* <div className="col-md-3 col-xs-3">
                            <h2>
                              ₹{singledetail.product_variant.price} x{" "}
                              {singledetail.quantity}
                            </h2>
                          </div>
                          <div className="col-md-2 col-xs-2">
                            <h2>
                              ₹
                              {singledetail.product_variant.price *
                                singledetail.quantity}
                            </h2>
                          </div> */}

                              <div className="col-sm-3 col-xs-3">
                                <h2 className="delete">
                                  {/* <i
                                className="far fa-trash-alt"
                                type="submit"
                                id={singledetail.id}
                                onClick={e => {
                                  this.deleteCart(
                                    e,
                                    singledetail.product_variant_id
                                  );
                                }}
                              ></i> */}

                                  <i
                                    class="icofont-close-line"
                                    // className="far fa-trash-alt"
                                    type="submit"
                                    id={singledetail.id}
                                    onClick={e => {
                                      this.deleteCart(
                                        e,
                                        singledetail.product_variant_id
                                      );
                                    }}
                                    // style={{ marginLeft: "167px" }}
                                  ></i>
                                </h2>
                              </div>
                            </div>
                          ) : (
                            <div className="row border" key={index}>
                              <div className="col-sm-3 col-xs-3">
                                <img
                                  alt="some"
                                  src={singledetail.img_url}
                                  width="100%"
                                />
                              </div>
                              <div className="col-sm-6 col-xs-6">
                                <h2 style={{ textAlign: "left" }}>
                                  {" "}
                                  {/* {singledetail.product_name.length > 20
                                ? singledetail.product_name.substring(0, 20) +
                                  "..."
                                : singledetail.product_name}{" "} */}
                                  {singledetail.product_name}
                                </h2>
                                <h2
                                  style={{
                                    textAlign: "left",
                                    marginTop: "8px",
                                    color: "#ef0081"
                                  }}
                                >
                                  ₹{singledetail.price} x{" "}
                                  {singledetail.quantity}
                                </h2>
                              </div>
                              {/* <div className="col-md-3 col-xs-3">
                            <h2>
                              ₹{singledetail.price} x {singledetail.quantity}
                            </h2>
                          </div>
                          <div className="col-md-2 col-xs-2">
                            <h2>
                              ₹{singledetail.price * singledetail.quantity}
                            </h2>
                          </div>
                        */}
                              <div className="col-md-3 col-xs-2">
                                <h2 className="delete">
                                  <i
                                    className="icofont-close-line"
                                    type="submit"
                                    id={singledetail.id}
                                    onClick={e => {
                                      this.deleteCart(
                                        e,
                                        singledetail.product_variant_id
                                      );
                                    }}
                                  ></i>
                                </h2>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                      <div className="center">
                        {this.state.cartData.length !== 0 ? (
                          <div style={{ marginTop: "16px" }}>
                            <h2
                              className="cartspan"
                              style={{ textAlign: "center" }}
                            >
                              CART TOTAL :
                              <span
                                style={{
                                  marginLeft: "8px",
                                  color: "#ef0081"
                                }}
                              >
                                ₹ {this.state.sub_total}
                              </span>
                            </h2>
                            {/* <Link to="/checkout"> */}
                            <button
                              className="checkout"
                              type="submit"
                              onClick={() => {
                                window.location.href = "/checkout";
                              }}
                            >
                              <i
                                class="far fa-check-circle"
                                style={{ fontSize: "13px" }}
                              ></i>
                              Check Out
                            </button>
                            <button
                              className="checkout"
                              type="submit"
                              onClick={() => {
                                window.location.href = "/cart";
                              }}
                            >
                              <i
                                class="fas fa-shopping-cart"
                                style={{ fontSize: "13px" }}
                              ></i>
                              View Cart
                            </button>
                            {/* </Link> */}
                          </div>
                        ) : (
                          <h2>Oops! Cart is empty</h2>
                        )}
                      </div>
                    </div>
                    {/* ) : null} */}
                  </div>
                  <div className="hideprofile">
                    <div className="dropdown" style={{}}>
                      <h5>
                        {login()
                          ? "Hi, " + this.state.userDetails.name
                          : "Login | Sign up"}
                        <i
                          style={{ marginLeft: "4px", color: "orange" }}
                          className="fas fa-user-alt"
                        ></i>
                      </h5>
                      <div className="dropdown-contents">
                        {login() ? (
                          <div>
                            <Link to="/user/info">My Profile</Link>
                            <Link to="/user/order">Orders</Link>
                            <Link to="/sign-in" onClick={this.logout}>
                              Logout
                            </Link>
                          </div>
                        ) : (
                          <div>
                            <Link to="/sign-in"> Login</Link>
                            <Link to="/register"> Create account</Link>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                  <i className="icofont-search"></i>
                  <i className="icofont-user"></i>
                </div>
              </div>
            </div>
          </div>
          {this.state.showSearchs ? (
            <div>
              <input
                className="searchbar"
                value={this.state.search}
                onChange={e => {
                  this.handleInput(e);
                }}
                type="text"
                placeholder="Search for Products or Categories..."
              />
              <span className="">
                <i
                  className="fas fa-search searchIconInsidebar"
                  style={{ padding: "11px" }}
                ></i>
              </span>
              {this.state.showSuggestion ? (
                <div className="searchBoxWrapper">
                  <div
                    onClick={() => {
                      this.setState({ showSuggestion: false });
                    }}
                    style={{
                      height: "100vh",
                      width: "100% "
                      // background: "white"
                    }}
                  ></div>

                  <div className="searchModels">
                    {this.state.Primary_Categories.length === 0
                      ? this.state.products.map((product, index) => {
                          const parseImageArray = JSON.parse(
                            product.product_variant[0].image
                          );

                          return (
                            <Link
                              to={{
                                pathname: "/product-detail/" + product.id
                              }}
                              key={index}
                              onClick={this.handleSuggestion}
                            >
                              <div className={"row"}>
                                <div className="col-md-3 col-3">
                                  <img
                                    src={parseImageArray[0]}
                                    alt=""
                                    width="50px"
                                  />
                                </div>
                                <div className="col-md-6 col-6">
                                  <h5
                                    style={{
                                      marginTop: " 8px",
                                      display: "block"
                                    }}
                                  >
                                    {" "}
                                    {product.product_name}
                                  </h5>
                                </div>
                                <div className="col-md-3 col-3">
                                  <h5
                                    style={{
                                      marginTop: " 8px",
                                      display: "block"
                                    }}
                                  >
                                    ₹ {product.product_variant[0].price}
                                  </h5>
                                </div>
                              </div>
                            </Link>
                          );
                        })
                      : this.state.Primary_Categories.length !== 0
                      ? this.state.Primary_Categories.map(
                          (pcategory, index) => (
                            <div
                              key={index}
                              className="row"
                              style={{ marginBottom: "8px" }}
                            >
                              <Link
                                to={{
                                  pathname: "/productlist/" + pcategory.id,
                                  state: { pcatName: pcategory.category_name }
                                }}
                                onClick={this.handleSuggestion}
                              >
                                <div className="col-md-2 col-xs-2">
                                  <img
                                    src={pcategory.image}
                                    alt=""
                                    width="60px"
                                  />
                                </div>
                                <div className="col-md-10 col-xs-10">
                                  <h5 className="">
                                    {pcategory.category_name}
                                  </h5>
                                </div>
                              </Link>
                            </div>
                          )
                        )
                      : "Search result not found"}
                  </div>
                </div>
              ) : null}
            </div>
          ) : // <input placeholder="Search products" className="searchbar" />
          null}
        </div>
        {/* <div className="hide-from-web">
          <ul>
            <li>
              <img src={cat} width="7%" className="headerCatall" />
              <p className="allcatFont" style={{ marginTop: "11px" }}>
                {" "}
                All Categories
              </p>
            </li>
            <li>
              <img
                src="https://admin.vishalkids.in/storage/PrimaryCategories/PCategoryName1582344635.jpg"
                width="20%"
                className="imgcat"
              />
              <p className="allcatFont" style={{ marginLeft: "-55px" }}>
                Baby Fashion
              </p>
            </li>
            <li>
              <img
                src="https://admin.vishalkids.in/storage/PrimaryCategories/PCategoryName1582344635.jpg"
                width="20%"
                // className="imgcat"
              />
              <p style={{ marginLeft: "-55px" }}>Toys</p>
            </li>
          </ul>
        </div>
      */}
      </div>
    );
  }
}
