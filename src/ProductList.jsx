/* eslint-disable no-loop-func */
import React from "react";
import Axios from "axios";
import Products from "./Products";
import Constants from "./variable/Constants";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import Footer from "./Partials/Footer";
import Lottie from "react-lottie";
import animationData from "../src/assets/not.json";
import Loader from "../src/assets/26126-loader.json";

// const productarray = [];
//
export default class ProductList extends React.Component {
  state = {
    filter_category: [],
    filter_color: [],
    filter_size: [],
    filter_brand: [],
    filter_price: [],
    filter_sale: [],
    data: [],
    Tshow: true,
    show: true,
    location: {},
    search: "",
    Pdata: [],
    ternaryData: [],
    PdataId: "",
    Sdata: [],
    display: true,
    primaryFilterArray: [],
    secondaryFilterArray: [],
    ternaryFilterArray: [],
    filterData: [],
    pcat_id_array: [],
    SdataId: "",
    Scat_id_array: [],
    Tcat_id_array: [],
    products: [],
    secondaryViewArray: [],
    ternaryViewArray: [],
    brands: [],
    showBrand: false,
    brandsFilter: [],
    priceFilter: [],
    showPrice: false,
    value: { min: 1, max: 50000 },
    pcategoryId: 0,
    pcategoryName: "",
    byPrimaryCat: "",
    key: "",
    allProducts: [],
    dataAvailable: false,
    isStopped: false,
    isPaused: false,
    ternId: 0,
    branId: 0,
    brandIds: [],
    ternIds: [],
    secIds: [],
    prices: [],
    selectedSec: [],
    selectedTer: [],
    selectedBr: [],
    filterArray: [],
    loading: true,
    filterObj: {
      secondary_category_id: [],
      ternary_category_id: [],
      brand_id: []
    },
    product_overview: "",
    fromInc: 1,
    toInc: 5,
    btnIdArray: [],
    filteredBtnIdArray: []
  };
  componentDidMount() {
    console.log(this.props);
    const pcategoryId = this.props.match.params.id;

    this.setState(
      {
        pcategoryId
        // pcategoryName: this.props.location.state.pcatName
      },
      () => {
        let searchUrl = Constants.getUrl.product;
        Axios.get(searchUrl + "&pcatid=" + this.state.pcategoryId).then(res => {
          console.log(res.data);
          let btnIdArray = [];
          for (let i = 1; i <= res.data.last_page; i++) {
            btnIdArray.push(i);
            console.log(btnIdArray);
          }
          this.setState(
            {
              filterData: res.data.data,
              btnIdArray,
              filterData: res.data.data.filter(
                v => v.product_variant.length > 0
              ),
              data: res.data.data,
              pcategoryName: res.data.data
                ? res.data.data[0]
                  ? res.data.data[0].primary_category
                    ? res.data.data[0].primary_category.category_name
                    : "No data"
                  : "No data"
                : "No data",
              loading:
                this.props.location.state !== undefined &&
                this.props.location.state.ageId
                  ? true
                  : false
            },
            () => {
              let filteredBtnIdArray = this.state.btnIdArray.filter(
                v => v >= this.state.fromInc && v <= this.state.toInc
              );
              console.log(filteredBtnIdArray);
              console.log(this.state.filterData);
              this.setState({
                dataAvailable: true,
                filteredBtnIdArray
              });
              if (this.props.location.state !== undefined) {
                if (this.props.location.state.ternaryId) {
                  console.log(this.props.location.state.ternaryId);
                  this.setState(
                    {
                      ternId: this.props.location.state.ternaryId,
                      byPrimaryCat: "no",
                      key: "ternary_category_id"
                    },
                    () => {
                      let arr = [];
                      this.state.data.forEach(v => {
                        if (v.ternary_category_id === this.state.ternId) {
                          arr.push(v);
                        }
                        this.setState({
                          filterData: arr.filter(
                            v => v.product_variant.length > 0
                          )
                        });
                      });
                      // this.dummyfilter(this.state.key, this.state.ternId);
                    }
                  );
                } else if (this.props.location.state.brandId) {
                  console.log(this.props.location.state.brandId);
                  this.setState(
                    {
                      branId: this.props.location.state.brandId,
                      byPrimaryCat: "no",
                      key: "brand_id"
                    },
                    () => {
                      let arr = [];
                      this.state.data.forEach(v => {
                        if (v.brand_id === this.state.branId) {
                          arr.push(v);
                        }
                        this.setState({
                          filterData: arr.filter(
                            v => v.product_variant.length > 0
                          )
                        });
                      });
                      // this.dummyfilter(this.state.key, this.state.branId);
                    }
                  );
                } else {
                  let arr = [];
                  this.state.data.forEach(v => {
                    if (v.age_group_id === this.props.location.state.ageId) {
                      arr.push(v);
                    }
                    this.setState({
                      filterData: arr.filter(v => v.product_variant.length > 0),
                      loading: false
                    });
                  });
                  // Axios.get(
                  //   "https://admin.vishalkids.in/api/get_prod_by_age_group?age_group_id=" +
                  //     this.props.location.state.ageId +
                  //     "&primary_category_id=" +
                  //     this.props.match.params.id
                  // ).then(res => {
                  //   console.log(res.data);
                  //   this.setState({ filterData: res.data, loading: false });
                  // });
                }
              }
            }
          );
        });
      }
    );

    this.renderFilters(pcategoryId);
  }
  componentDidUpdate(prevProps) {
    console.log(prevProps.match.params.id, this.props.match.params.id);
    if (prevProps.match.params.id !== this.props.match.params.id) {
      let id = this.props.match.params.id;
      Axios.get(Constants.getUrl.product + "&pcatid=" + id).then(res => {
        this.setState(
          {
            filterData: res.data.data.filter(v => v.product_variant.length > 0),
            pcategoryName: res.data.data
              ? res.data.data[0]
                ? res.data.data[0].primary_category
                  ? res.data.data[0].primary_category.category_name
                  : "No data"
                : "No data"
              : "No data",
            loading: false
          },
          () => {
            console.log(this.state.filterData);
            this.setState({
              dataAvailable: true
            });
          }
        );
      });
      this.renderFilters(id);
    }
  }
  renderFilters = pcategoryId => {
    Axios.get(
      Constants.getUrl.Secondary_Categories + "&pcateid=" + pcategoryId
    ).then(res => {
      // console.log(res);
      this.setState({
        secondaryViewArray: res.data,
        secondaryData: res.data
      });
    });
    Axios.get(
      Constants.getUrl.ternary_Categories + "&pcateid=" + pcategoryId
    ).then(res => {
      // console.log(res);
      this.setState(
        {
          ternaryViewArray: res.data,
          ternaryData: res.data
        },
        () => {}
      );
    });
    Axios.get(
      "https://admin.vishalkids.in/api/primary-category/" + pcategoryId
    ).then(res => {
      // console.log(res.data);
      this.setState({
        brands: res.data.brand_name
      });
    });
    // Axios.get(
    //   "https://admin.vishalkids.in/api/primary-category/" + pcategoryId
    // ).then(res => {
    //   console.log(res);
    //   this.setState(
    //     {
    //       brands: res.data.brand_name
    //     },
    //     () => {
    //       console.log(this.state.brands);
    //     }
    //   );
    // });
  };
  categoryChange = pcatData => {
    // console.log(pcatData);
    let searchUrl = Constants.getUrl.product;
    Axios.get(searchUrl + "&pcatid=" + pcatData.id).then(res => {
      // console.log(res.data.data);
      let tfilterData = this.state.ternaryData.filter(
        ternary => pcatData.id === ternary.primary_category_id
      );
      // // console.log(tfilterData);
      let sfilterData = this.state.secondaryData.filter(
        secondary => pcatData.id === secondary.primary_category_id
      );
      this.setState(
        {
          filterData: res.data.data.filter(v => v.product_variant.length > 0),
          pcategoryName: pcatData.category_name,
          ternaryViewArray: tfilterData,
          secondaryViewArray: sfilterData
          // products: res.data.data
        },
        () => {
          // console.log(this.state.filterData);
        }
      );
    });
  };
  selectBrand = e => {
    this.setState({
      showBrand: true
    });
  };
  selectPrice = e => {
    this.setState({
      showPrice: true
    });
  };
  filterWithSecondary = e => {
    const id = e.target.id;
    if (this.state.secondaryFilterArray.includes(id) === true) {
      const index = this.state.secondaryFilterArray.indexOf(id);
      this.state.secondaryFilterArray.splice(index, 1);
    } else {
      this.state.secondaryFilterArray.push(id);
    }

    let sFilter = [];
    // let tFilter = [];
    if (this.state.secondaryFilterArray.length !== 0) {
      this.state.secondaryFilterArray.forEach(sId => {
        let sData = this.state.products.filter(
          product => sId === product.secondary_category_id
        );
        sFilter.push(...sData);
      });
      this.setState(
        {
          filterData: sFilter.filter(v => v.product_variant.length > 0)
        },
        () => {}
      );
    }
  };
  filterwithTernaryCat = e => {
    const id = e.target.id;
    if (this.state.ternaryFilterArray.includes(id) === true) {
      const index = this.state.ternaryFilterArray.indexOf(id);
      this.state.ternaryFilterArray.splice(index, 1);
    } else {
      this.state.ternaryFilterArray.push(id);
    }

    let tFilter = [];
    if (this.state.ternaryFilterArray.length !== 0) {
      this.state.ternaryFilterArray.forEach(sId => {
        let tData = this.state.products.filter(
          product => sId === product.ternary_category_id
        );
        tFilter.push(...tData);
      });
      this.setState(
        {
          filterData: [...tFilter].filter(v => v.product_variant.length > 0)
        },
        () => {}
      );
    }
  };
  filterwithBrand = e => {
    const id = e.target.id;
    if (this.state.brandsFilter.includes(id) === true) {
      const index = this.state.brandsFilter.indexOf(id);
      this.state.brandsFilter.splice(index, 1);
    } else {
      this.state.brandsFilter.push(id);
    }
    // console.log(this.state.brandsFilter);

    let brandData = [];
    // if (this.state.brandsFilter.length !== 0) {
    this.state.brandsFilter.forEach(bId => {
      let brand = this.state.products.filter(data => bId === data.brand.id);
      // console.log(brand);
      brandData.push(...brand);
      // console.log(brandData);

      this.setState({
        filterData: brandData.filter(v => v.product_variant.length > 0)
      });
    });
    // }
  };
  filterwithPrice = value => {
    console.log(this.state.value);

    // this.setState(
    //   {
    //     value
    //   },
    //   () => {
    //     // console.log(this.state.value);

    //     const minValue = this.state.value.min;
    //     const maxValue = this.state.value.max;
    //     // console.log(minValue);

    //     let priceData = [];

    //     // priceArray.forEach(singlePrice => {
    //     // console.log(this.state.filterData.product_variant.price[0]);

    //     let newPrice = this.state.data.filter(
    //       price =>
    //         price.product_variant[0].price >= minValue &&
    //         price.product_variant[0].price <= maxValue
    //     );
    //     priceData.push(...newPrice);
    //     // console.log(priceData);
    //     this.setState(
    //       {
    //         filterData: [...priceData]
    //       },
    //       () => {
    //         // console.log(this.state.filterData);
    //       }
    //     );
    //     // });
    //   }
    // );
  };
  addItems = payload => {
    // console.log(payload);

    this.setState(
      {
        cartDetails: payload
      },
      () => {
        this.props.onAddToCart(this.state.cartDetails);
        console.log(this.state.cartDetails);
      }
    );
  };
  // onSelectCategory = pcategoryData => {
  //   // console.log(pcategoryData);
  //   const category_name = pcategoryData.category_name;
  //   const pcategoryId = pcategoryData.id;
  //   let searchUrl = Constants.getUrl.product;

  //   this.setState(
  //     {
  //       pcategoryId,
  //       pcategoryName: category_name
  //     },
  //     () => {
  //       // console.log(this.state.allProducts);
  //       Axios.get(searchUrl + "?pcatid=" + this.state.pcategoryId).then(res => {
  //         // console.log(res.data.data);
  //         let tfilterData = this.state.ternaryData.filter(
  //           ternary => this.state.pcategoryId === ternary.primary_category_id
  //         );
  //         // // console.log(tfilterData);

  //         let sfilterData = this.state.secondaryData.filter(
  //           secondary =>
  //             this.state.pcategoryId === secondary.primary_category_id
  //         );
  //         // // console.log(sfilterData);
  //         this.setState({
  //           filterData: res.data.data,
  //           ternaryViewArray: tfilterData,
  //           secondaryViewArray: sfilterData
  //         });
  //       });
  //     }
  //   );
  // };

  // filter triggers

  // handleSecondCat = id => {
  //   console.log(id);
  //   if (this.state.filterObj.sec.includes(id) == true) {
  //     let i = 0;
  //     this.state.selectedSec.forEach(secId => {
  //       if (secId === id) {
  //         this.state.selectedSec.splice(i, 1);
  //       }
  //       i++;
  //     });
  //   } else {
  //     this.state.selectedSec.push(id);
  //   }

  // };

  filter = (key, id) => {
    // this.filterwithPrice(this.state.value);
    const filterArray = [];
    if (this.state.filterObj[key].includes(id) === true) {
      const indexOf = this.state.filterObj[key].indexOf(id);
      this.state.filterObj[key].splice(indexOf, 1);
    } else {
      this.state.filterObj[key].push(id);
    }
    // console.log(this.state.filterObj);
    let filterObjLength = 0;
    for (const item in this.state.filterObj) {
      // console.log(item);
      this.state.filterObj[item].forEach(filterId => {
        this.state.data.forEach(obj => {
          // console.log(filterId, obj);
          if (filterId === obj[item]) {
            const checkFilter = filterArray.find(
              filterObj => filterObj[item] === filterId
            );
            if (!checkFilter) {
              filterArray.push(obj);
            }
            filterObjLength += 1;
          }
        });
      });
    }
    console.log(filterObjLength);
    this.setState({
      filterData:
        filterObjLength === 0
          ? this.state.data.filter(v => v.product_variant.length > 0)
          : filterArray.filter(v => v.product_variant.length > 0)
    });
    console.log(filterArray);
  };
  submitFilter = () => {
    if (
      this.state.secIds.length === 0 &&
      this.state.ternIds.length === 0 &&
      this.state.brandIds.length === 0 &&
      this.state.value.min === 1 &&
      this.state.value.max === 50000
    ) {
      this.setState({
        filterData: this.state.data.filter(v => v.product_variant.length > 0)
      });
    } else {
      Axios.post("https://admin.vishalkids.in/api/get_prod_by_sec_ter_brand", {
        scatid: this.state.secIds,
        tcatid: this.state.ternIds,
        brand: this.state.brandIds,
        min_price: this.state.value.min,
        max_price: this.state.value.max
      }).then(resp => {
        console.log(resp);
        if (resp.data.length > 0) {
          let filterData = [];
          resp.data.forEach(v => {
            if (v.product_variant.length > 0) {
              filterData.push(v);
            }
            this.setState({
              filterData: filterData.filter(v => v.product_variant.length > 0)
            });
          });
        } else {
          this.setState({ filterData: [] });
        }
      });
    }
  };
  currentPage = v => {
    const pcategoryId = this.props.match.params.id;
    let searchUrl = Constants.getUrl.product;
    Axios.get(searchUrl + "&pcatid=" + pcategoryId + "&page=" + v).then(
      resp => {
        console.log(resp.data.data);
        this.setState({
          filterData: resp.data.data,
          active: v
        });
      }
    );
  };
  prevPages = () => {
    this.setState(
      {
        fromInc: this.state.fromInc - 5,
        toInc: this.state.toInc - 5
      },
      () => {
        let filteredBtnIdArray = this.state.btnIdArray.filter(
          v => v >= this.state.fromInc && v <= this.state.toInc
        );
        if (filteredBtnIdArray.length >= 1) {
          if (filteredBtnIdArray.length === 1) {
            this.setState(
              {
                fromInc: this.state.fromInc + 4,
                toInc: this.state.toInc + 4
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 2) {
            this.setState(
              {
                fromInc: this.state.fromInc + 3,
                toInc: this.state.toInc + 3
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 3) {
            this.setState(
              {
                fromInc: this.state.fromInc + 2,
                toInc: this.state.toInc + 2
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 4) {
            this.setState(
              {
                fromInc: this.state.fromInc + 1,
                toInc: this.state.toInc + 1
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else {
            this.setState({ filteredBtnIdArray });
          }
        } else {
          this.setState(
            {
              fromInc: this.state.fromInc + 5,
              toInc: this.state.toInc + 5
            },
            () => {
              let filteredBtnIdArray = this.state.btnIdArray.filter(
                v => v >= this.state.fromInc && v <= this.state.toInc
              );
              this.setState({ filteredBtnIdArray });
            }
          );
        }
      }
    );
  };
  nextPages = v => {
    this.setState(
      {
        fromInc: this.state.fromInc + 5,
        toInc: this.state.toInc + 5
      },
      () => {
        let filteredBtnIdArray = this.state.btnIdArray.filter(
          v => v >= this.state.fromInc && v <= this.state.toInc
        );
        if (filteredBtnIdArray.length >= 1) {
          if (filteredBtnIdArray.length === 1) {
            this.setState(
              {
                fromInc: this.state.fromInc - 4,
                toInc: this.state.toInc - 4
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 2) {
            this.setState(
              {
                fromInc: this.state.fromInc - 3,
                toInc: this.state.toInc - 3
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 3) {
            this.setState(
              {
                fromInc: this.state.fromInc - 2,
                toInc: this.state.toInc - 2
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else if (filteredBtnIdArray.length === 4) {
            this.setState(
              {
                fromInc: this.state.fromInc - 1,
                toInc: this.state.toInc - 1
              },
              () => {
                let filteredBtnIdArray = this.state.btnIdArray.filter(
                  v => v >= this.state.fromInc && v <= this.state.toInc
                );
                this.setState({ filteredBtnIdArray });
              }
            );
          } else {
            this.setState({ filteredBtnIdArray });
          }
        } else {
          this.setState(
            {
              fromInc: this.state.fromInc - 5,
              toInc: this.state.toInc - 5
            },
            () => {
              let filteredBtnIdArray = this.state.btnIdArray.filter(
                v => v >= this.state.fromInc && v <= this.state.toInc
              );
              this.setState({ filteredBtnIdArray });
            }
          );
        }
      }
    );
  };

  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
      }
    };
    return (
      <div>
        <div className="productlist">
          <div className="row">
            <div className="somenew">
              <h2 className="new">
                <i class="fas fa-bars"></i>
              </h2>
              <div className="col-md-3 some">
                {/* {this.state.secondaryFilterArray.length !== 0 ? ( */}
                <div>
                  <p>Products For</p>
                  <ul>
                    {this.state.secondaryViewArray.map((single, index) => (
                      <li key={index}>
                        <input
                          type="checkbox"
                          className="check"
                          id={single.id}
                          checked={
                            this.state.secIds.includes(single.id) ? true : false
                          }
                          onClick={() => {
                            if (this.state.secIds.includes(single.id)) {
                              let index = this.state.secIds.indexOf(single.id);
                              this.state.secIds.splice(index, 1);
                              this.setState(
                                { secIds: this.state.secIds },
                                () => {
                                  this.submitFilter();
                                }
                              );
                            } else {
                              this.state.secIds.push(single.id);
                              this.setState(
                                { secIds: this.state.secIds },
                                () => {
                                  this.submitFilter();
                                }
                              );
                            }
                          }}
                          // onClick={() =>
                          //   // this.filter("secondary_category_id", single.id)
                          // }
                          // onClick={this.handleSecondCat}
                        />
                        {single.category_name}
                      </li>
                    ))}
                  </ul>
                </div>
                {/* ) : null} */}

                <p>Sub Category</p>

                <ul>
                  {this.state.ternaryViewArray.map((singleT, index) => (
                    <li key={index}>
                      <input
                        type="checkbox"
                        className="check"
                        checked={
                          this.state.ternIds.includes(singleT.id) ? true : false
                        }
                        onClick={() => {
                          if (this.state.ternIds.includes(singleT.id)) {
                            let index = this.state.ternIds.indexOf(singleT.id);
                            this.state.ternIds.splice(index, 1);
                            this.setState(
                              { ternIds: this.state.ternIds },
                              () => {
                                this.submitFilter();
                              }
                            );
                          } else {
                            this.state.ternIds.push(singleT.id);
                            this.setState(
                              { ternIds: this.state.ternIds },
                              () => {
                                this.submitFilter();
                              }
                            );
                          }
                        }}
                        // onClick={this.filterwithTernaryCat}
                        // onClick={() =>
                        //   // this.filter("ternary_category_id", singleT.id)
                        // }
                        id={singleT.id}
                      />
                      {singleT.category_name}
                    </li>
                  ))}
                </ul>
                {/* ) : null} */}
                <p onClick={this.selectPrice}>Shop By Price</p>
                {/* {this.state.showPrice ? ( */}
                <div style={{ padding: "4px 16px", fontSize: "16px" }}>
                  <InputRange
                    // id={price.product_variant.price}
                    maxValue={50000}
                    minValue={0}
                    value={this.state.value}
                    step={500}
                    // onChange={value => this.setState({ value })}
                    onChange={value => this.setState({ value })}
                    onChangeComplete={this.submitFilter}
                  />
                </div>
                {/* ) : null} */}
                <p onClick={this.selectBrand}>Shop By Brand</p>
                {/* {this.state.showBrand ? ( */}
                <ul>
                  {this.state.brands.map((clothbrand, index) => (
                    <li key={index}>
                      <input
                        type="checkbox"
                        className="check"
                        checked={
                          this.state.brandIds.includes(clothbrand.id)
                            ? true
                            : false
                        }
                        id={clothbrand.id}
                        // onClick={this.filterwithBrand}
                        onClick={
                          () => {
                            if (this.state.brandIds.includes(clothbrand.id)) {
                              let index = this.state.brandIds.indexOf(
                                clothbrand.id
                              );
                              this.state.brandIds.splice(index, 1);
                              this.setState(
                                { brandIds: this.state.brandIds },
                                () => {
                                  this.submitFilter();
                                }
                              );
                            } else {
                              this.state.brandIds.push(clothbrand.id);
                              this.setState(
                                { brandIds: this.state.brandIds },
                                () => {
                                  this.submitFilter();
                                }
                              );
                            }
                          }
                          // this.filter("brand_id", clothbrand.id)
                        }
                      />
                      {clothbrand.name}
                    </li>
                  ))}
                </ul>
                {/* ) : null} */}
                {/* <p>Best Sellers</p>
              {this.state.filter_sale.map((clothsale, index) => (
                <div key={index}>
                  <img alt="some" src={clothsale.url} width="55%" />
                  <div className="saletext">
                    <h5> {clothsale.brandname} </h5>
                    <h6> {clothsale.price} </h6>
                  </div>
                </div>
              ))} */}
              </div>
            </div>
            <div className="col-md-9 colMrg">
              {this.state.filterData.length !== 0 ? (
                <h2 style={{ color: "#555" }}>
                  Showing products from {this.state.pcategoryName} category
                </h2>
              ) : null}
              <div className="row">
                {this.state.loading ? (
                  <div>
                    <Lottie
                      options={{
                        animationData: Loader
                      }}
                      style={{
                        width: "80px",
                        height: "80px"
                        // marginRight: "16px",
                      }}
                    />
                    {/* <h4>Loading...</h4> */}
                  </div>
                ) : this.state.filterData.length === 0 ? (
                  <div>
                    <Lottie
                      options={defaultOptions}
                      height={200}
                      width={360}
                      marginRight={30}
                      isStopped={this.state.isStopped}
                      isPaused={this.state.isPaused}
                      style={{ marginTop: "100px" }}
                    />
                    <h5 style={{ textAlign: "center" }}>
                      Data Not Available For This Category
                    </h5>
                  </div>
                ) : (
                  this.state.filterData.map((singledata, index) => {
                    // let parseImageArray = JSON.parse(
                    //   singledata.product_variant[0]
                    //     ? singledata.product_variant[0].image
                    //     : ""
                    // );
                    // console.log(
                    //   JSON.parse(singledata.product_variant[0].image)
                    // );

                    return (
                      <div>
                        <Products
                          onAdd={this.addItems}
                          id={singledata.id}
                          // cartId={this.state.cartDetails.id}
                          key={index}
                          name={singledata.product_name}
                          brand={singledata.brand.name}
                          // price={
                          //   singledata.product_variant[0]
                          //     ? singledata.product_variant[0].price
                          //     : ""
                          // }
                          price={
                            singledata.offer_price
                              ? singledata.offer_price
                              : singledata.product_variant[0]
                              ? singledata.product_variant[0].price
                              : ""
                          }
                          offer_price={
                            singledata.offer_price ? singledata.offer_price : ""
                          }
                          overView={
                            singledata.product_overview
                              ? singledata.product_overview.length > 15
                                ? singledata.product_overview.substring(0, 50) +
                                  "..."
                                : "some description sdfsgfdgdsfsfdg"
                              : "some description sdfsgfdgdsfsfdg"
                          }
                          url={
                            singledata.product_variant[0]
                              ? JSON.parse(
                                  singledata.product_variant[0].image
                                )[0]
                              : "https://images.unsplash.com/photo-1501686637-b7aa9c48a882?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80"
                          }
                          variantId={
                            singledata.product_variant[0]
                              ? singledata.product_variant[0].id
                              : ""
                          }
                          note={singledata.note}
                        />
                      </div>
                    );
                  })
                )}
                {this.state.filteredBtnIdArray.length > 1 ? (
                  <div className="paginationWrapper">
                    {this.state.filteredBtnIdArray
                      ? this.state.filteredBtnIdArray.map((v, i) => (
                          <span
                            style={{
                              cursor: "pointer",
                              background:
                                this.state.active === v ? "#ef0081" : "",
                              // color: "#fff",
                              border:
                                this.state.active === v
                                  ? "1px solid #ec72b3ad"
                                  : ""
                            }}
                            key={i}
                            onClick={() => {
                              this.currentPage(v);
                            }}
                            className={this.state.active === v ? "active" : ""}
                          >
                            {v}
                          </span>
                        ))
                      : null}
                  </div>
                ) : null}

                {/* <button onClick={this.prevPages}>Prev</button>

                <button onClick={this.nextPages}>Next</button> */}
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
