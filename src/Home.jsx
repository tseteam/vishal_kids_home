import React from "react";
import Axios from "axios";
import Category1 from "./Partials/Category1";
import Products from "./Partials/Products";
import Offer from "./Components/Offer";
import CategoryHorizontal from "./Components/CategoryHorizontal";
import Footer from "./Partials/Footer";
import Constants from "./variable/Constants";
import { Fade } from "react-slideshow-image";
import login from "./Components/LoginCheck.jsx";
import init from "./helper/WindowToken";
import { Link } from "react-router-dom";
import BrandSlider from "./Components/BrandSlider";

export default class Home extends React.Component {
  state = {
    category: [],
    product_header: [],
    product: [],
    brands: [],
    horizontal_cat: [],
    location: {},
    userDetails: { name: "" },
    offers: [],
    products: [],
    pCategoryArray: [],
    sectionFirst: [],
    sectionSecond: [],
    primary_category_id: 0,
    sliders: []
  };
  componentDidMount() {
    init();
    console.log("login", login());
    if (login()) {
      console.log("Logged in");
    } else {
      console.log("Not logged in");
    }
    let url = "Home.json";
    Axios.get(url).then(res => {
      this.setState(
        {
          category: res.data.category,
          product_header: res.data.product_header,
          product: res.data.product,
          horizontal_cat: res.data.horizontal_cat
        },
        () => {}
      );
    });
    Axios.get(Constants.getUrl.offers + "?status=enabled").then(res => {
      this.setState({
        offers: res.data
      });
    });
    Axios.get(Constants.getUrl.brands).then(res => {
      console.log(res.data);
      this.setState({
        brands: res.data
      });
    });
    Axios.get(Constants.getUrl.homeSliders + "?status=enabled").then(res => {
      console.log(res.data);
      this.setState({
        sliders: res.data.data
      });
    });
    Axios.get(Constants.getUrl.sections + "&section=1&count=4").then(res => {
      console.log(res.data);
      this.setState({
        sectionFirst: res.data
      });
    });
    Axios.get(Constants.getUrl.sections + "&section=2&count=6").then(res => {
      console.log(res.data);
      this.setState({
        sectionSecond: res.data
      });
    });
  }
  addItems = payload => {
    // console.log(payload);

    this.setState(
      {
        cartDetails: payload
      },
      () => {
        this.props.onAddToCart(this.state.cartDetails);
      }
    );
  };

  render() {
    let properties = {
      duration: 1000,
      transitionDuration: 500,
      infinite: true,
      arrows: false,
      pauseOnHover: false,
      indicators: true,
      onChange: (oldIndex, newIndex) => {
        // console.log(`slide transition from ${oldIndex} to ${newIndex}`);
      }
    };
    return (
      <div>
        {/* <Fade {...properties}> */}
          {this.state.sliders?this.state.sliders.map((slider, index) => (
            <div
              key={index}
              style={{
                height: "95vh",
                overflow: "hidden",
                position: "relative"
              }}
              className="mainSlider"
            >
              <img
                alt={slider.title}
                src={slider.image}
                className={"sliderImages"}
              />
              <Link to={slider.link}>
                <div
                  // style={{
                  //   position: "absolute",
                  //   overflow: "hidden",
                  //   top: 0,
                  //   bottom: 0,
                  //   right: 0,
                  //   left: 0,
                  //   background: "rgba(0, 0, 0, 0.27)"
                  // }}
                  className="sliderposition"
                />

                <div className="sliderWrapper">
                  <div className="sliderContent">
                    <h1>{slider.title}</h1>
                    <p>{slider.description}</p>
                  </div>
                </div>
              </Link>
              {/* https://images.unsplash.com/photo-1528948442628-282fb90a6487?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 */}
            </div>
          )):null}
        {/* </Fade> */}

        <div className="container">
          <div className="categoryWrapper">
            {this.state.offers.slice(0, 6).map((singleOffer, index) => (
              <Category1
                id={singleOffer.primary_category_id}
                key={index}
                url={singleOffer.image}
                title={singleOffer.title}
                offer={singleOffer.offer_percentage}
                pcatName={singleOffer.primary_category.category_name}
                // button={singleOffer.category_button}
              />
            ))}
          </div>
        </div>
        <Offer />
        <div className="container" style={{ marginBottom: "50px" }}>
          {/* {this.state.product_header.map((producthead, index) => (
            <div className="homelogo" key={index}>
              <h3>
                <i> {producthead.product_logo} </i>
              </h3>
              <h1> {producthead.product_title} </h1>
            </div>
          ))} */}

          {/* {this.state.sectionFirst.slice(0, 8).map((singleproduct, index) => {
            const parseImageArray = JSON.parse(
              singleproduct.product_variant[0].image
            );
            return (
              <Products
                onAdd={this.addItems}
                singleproduct={singleproduct}
                product_id={singleproduct.id}
                product_variant_id={singleproduct.product_variant[0].id}
                key={index}
                url={parseImageArray[0]}
                name={singleproduct.product_name}
                brand={singleproduct.product_brand}
                price={singleproduct.product_variant[0].price}
              />
            );
          })} */}
          <div className="homelogo">
            <h3>
              <i>Our colors matching your moods...</i>
            </h3>
            <h1> {this.state.sectionFirst.title} </h1>
          </div>
          {this.state.sectionFirst.product ? (
            this.state.sectionFirst.product.map((singleProduct, index) => {
              const parseImageArray = JSON.parse(
                singleProduct.product_variant[0].image
              );
              return (
                <Products
                  onAdd={this.addItems}
                  singleproduct={singleProduct}
                  product_id={singleProduct.id}
                  product_variant_id={singleProduct.product_variant[0].id}
                  key={index}
                  // url={parseImageArray[0]}
                  url={parseImageArray[0]}
                  name={singleProduct.product_name}
                  // brand={singleProduct.brand.name}
                  price={singleProduct.product_variant[0].price}
                />
              );
            })
          ) : (
            <h2>Data Not Available</h2>
          )}
        </div>
        <div className="categoryHorizontal">
          <div className="layer"></div>
          <div className="textcenter">
            <p>Fetch your collection</p>
            <h1>{this.state.sectionSecond.title}</h1>
          </div>
          <div className="container catemarg">
            <div className="row rowspace">
              {/* {this.state.sectionSecond
                .slice(0, 6)
                .map((SingleElement, index) => {
                  const parseImageArray = JSON.parse(
                    SingleElement.product_variant[0].image
                  );

                  return (
                    <CategoryHorizontal
                      key={index}
                      imgurl={parseImageArray[0]}
                      title={SingleElement.product_name}
                      // brand={SingleElement.brand.name}
                      price={SingleElement.product_variant[0].price}
                    />
                  );
                })} */}
              {this.state.sectionSecond.product ? (
                this.state.sectionSecond.product.map(
                  (singleCategory, index) => {
                    const parseImageArray = JSON.parse(
                      singleCategory.product_variant[0].image
                    );
                    return (
                      <Link
                        to={"/product-detail/" + singleCategory.id}
                        key={index}
                      >
                        <CategoryHorizontal
                          key={index}
                          // imgurl={parseImageArray[0]}
                          imgurl={parseImageArray[0]}
                          title={singleCategory.product_name}
                          // brand={singleCategory.brand.name}
                          price={singleCategory.product_variant[0].price}
                        />
                      </Link>
                    );
                  }
                )
              ) : (
                <h2>Data Not Available</h2>
              )}
            </div>
          </div>
        </div>
        <BrandSlider />
        <Footer />
      </div>
    );
  }
}
