import React, { Component } from "react";
import Lottie from "react-lottie";
import { Redirect } from "react-router-dom";

import animationData from "../src/assets/payment.json";

class PaymentConfirmed extends Component {
  state = {
    redirect: false
  };
  constructor() {
    super();
    this.state = { isStopped: false, isPaused: false };
  }
  componentDidMount() {
    setInterval(() => {
      this.setState({
        redirect: true
      });
    }, 6000);
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    } else {
      const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
      };
      return (
        <div className="paymentConfirmed">
          <Lottie
            options={defaultOptions}
            height={200}
            width={200}
            isStopped={this.state.isStopped}
            isPaused={this.state.isPaused}
          />
          <h1>Payment successful ! </h1>
          {/* <a href="/">
          <h5>Go To Home Page </h5>
        </a> */}
        </div>
      );
    }
  }
}

export default PaymentConfirmed;
