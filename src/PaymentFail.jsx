import React, { Component } from "react";
import Lottie from "react-lottie";
import animationData from "../src/assets/fail.json";

class PaymentFail extends Component {
  constructor() {
    super();
    this.state = { isStopped: false, isPaused: false };
  }
  render() {
    const defaultOptions = {
      loop: false,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
      }
    };
    return (
      <div className="paymentFail">
        <Lottie
          options={defaultOptions}
          height={200}
          width={200}
          isStopped={this.state.isStopped}
          isPaused={this.state.isPaused}
        />
        <h1>Payment failed ! </h1>
        {/* <h5>Oh Snap!</h5> */}
        <p>Looks like something went wrong</p>
        <p>while processing your request.</p>
      </div>
    );
  }
}

export default PaymentFail;
