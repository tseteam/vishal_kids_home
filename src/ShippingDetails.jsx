import React, { Component } from "react";
import Footer from "./Partials/Footer";

export default class ShippingDetails extends Component {
  render() {
    return (
      <div>
        <h2
          style={{
            textAlign: "center",
            borderBottom: "1px solid lightgray",
            marginBottom: "10px",
            padding: "10px"
          }}
        >
          Shipping Details
        </h2>
        <div
          className="container"
          style={{ fontSize: "18px", marginTop: "30px", marginBottom: "30px" }}
        >
          <p>
            Vishal Kids ships its products to almost all parts of India. Orders
            placed will be shipped within 24* hrs. We ship on all days except
            Sunday and National Holidays.
          </p>
          <p>
            <b>Shipping Charges:</b> A fixed shipping charge of Rs. 50 is
            applicable on all orders below Rs. 699/- (Excluding COD charges,
            Shipping charges, Taxes or value reduced after application of
            coupons or any other offer) at all locations with an additional
            charge applicable at certain locations.
          </p>
          <p>
            For orders equal to or above Rs. 699 (Excluding COD charges,
            Shipping charges, Taxes or value reduced after application of
            coupons or any other offer) free shipping is available at certain
            locations only. For other locations, additional shipping charges
            will be applicable. At these locations, in which additional charges
            are applicable, the Rs. 50 shipping charge will be added to the
            shipping charges for all orders below Rs. 699 (with exclusions as
            above).
          </p>
          <p>
            The shipping charge applicable per quantity of the product can be
            checked by entering your pin code details on the product pages and
            the total shipping charge applicable on the order will be the sum of
            the charges for all chargeable product(s) in your order (+ Rs. 50
            for orders below Rs. 699). The order level charge will be visible to
            you in the cart as well as when you enter the shipping address while
            you are checking out with your order.
          </p>
          <p>
            These shipping charges applicable are not refundable in the case of
            return/cancellation of the product or the order.
          </p>
          <p>
            The shipping charges can be modified by Vishal Kids at any point
            without prior intimation. The new charges would reflect on the
            product page as well as in the checkout flow.
          </p>
          <p>
            Every once in a while, Vishal Kids brings out various offers in an
            attempt to satisfy customers. Various offers could be introduced for
            a limited span of time that enable free shipping of goods, subject
            to the fulfilment of the terms and conditions attached to the offer.
          </p>
          <p>
            In light of the COVID- 19 outbreak, we will not be deducting the
            shipping charges while processing a cancellation request for all
            orders placed during the nation-wide lockdown.This is to avoid any
            inconvenience to our customers.
          </p>
          <p>
            {" "}
            <b> Estimated Delivery Time:</b> During Covid lockdown delivery is
            subject to MHA and state government guidelines. For all areas
            serviced by reputed couriers, the delivery time would be within 3 to
            4 business days of shipping (business days exclude Sundays and other
            holidays). However items weighing over 2 kilos or high volume may
            take a couple of days longer to reach. For other areas the products
            will be shipped through Indian Postal Service and may take 1-2 weeks
            depending on location.
          </p>
          <p>
            The delivery date displayed while placing the order i.e. on the
            Product Details, Cart, Checkout and Order Confirmation page and in
            the order confirmation email are tentative. It may change once the
            order is shipped. Post shipment of the order, an Estimated Delivery
            Date will be displayed in the 'Order Details' under 'My Account '
            section which will help you to keep a track of the shipment status
            of your order.
          </p>
          <p>
            Vishal Kids ensures to provide a delightful customer experience by
            delivering the products as per the Estimated Delivery Date
            communicated as above, however, at times there might be unexpected
            delays in the delivery of your order due to unavoidable and
            undetermined logistics challenges beyond our control for which
            Vishal Kids is not liable and would request its users to cooperate as
            Vishal Kids continuously tries to nought such instances. Also, Vishal Kids
            reserves the right to cancel your order at its sole discretion in
            cases where it takes longer than usual delivery time or the shipment
            is physically untraceable and refund the amount paid for cancelled
            product(s) in your Vishal Kids wallet.
          </p>
          <p>
            {" "}
            <b>Address Change:</b> You can change the delivery address of your
            order only if the order is in Confirmed or Pending status and none
            of the item(s) purchased in the order are shipped. However, if there
            are any variations in the charges/taxes (if any) applied for the
            original delivery address used while placing the order v/s the
            delivery address to be updated, the request for updating the
            delivery address might not be accepted.
          </p>
          <p>
            {" "}
            <b>Hidden Charges (Sales tax, Octroi etc): </b>You will get the
            final price during check out. Our prices are all inclusive and you
            need not pay anything extra.
          </p>
          <p>
            {" "}
            <b> International Locations:</b> Currently we do not deliver to
            locations outside India
          </p>
          <p>
            {" "}
            <b> Tracking Packages:</b> We will mail you the name of the courier
            company and the tracking number of your consignment in your
            registered email address. In case you do not receive a mail from us
            within 24hrs of placing an order please check your spam folder.
            Tracking may not appear online for up to another 24 hours in some
            cases, so please wait until your package is scanned by the courier
            company.
          </p>
          <p>
            {" "}
            <b> Non-availability on Delivery:</b> Our delivery partners will
            attempt to deliver the package thrice before it is returned back to
            our warehouse. Please provide your mobile number in the delivery
            address as it will help in making a faster delivery.
          </p>
          <p>
            * Valid only for owned stock. Shipping time may increase for third
            party shipments.
          </p>
        </div>
        <Footer />
      </div>
    );
  }
}
