import React, { Component } from "react";
import Products from "./Products";
import Axios from "axios";
import ReactImageMagnify from "react-image-magnify";
import Constants from "./variable/Constants";
import Footer from "./Partials/Footer";
import login from "./Components/LoginCheck";
import ReactPlayer from "react-player";
import renderHTML from "react-render-html";
import addToCartWithoutLogin from "./Components/WithoutLoginCartHandler";
import NotificationonAddCart from "./Components/NotificationonAddCart";
import init from "./helper/WindowToken";
let quantities = [];
export default class ProductDetail extends Component {
  state = {
    sArray: [],
    cArray: [],
    related: [],
    packOf: [],
    quantities: [],
    size: false,
    success: false,
    warn: false,
    actImage: "",
    actSize: "",
    actPack: "",
    actColor: "",
    sizeValue: "",
    colorValue: "",
    match: "",
    bName: "",
    bDesc: "",
    bImg: "",
    showMenu: 1,
    smallImage: {
      alt: "",
      isFluidWidth: "",
      src: "",
    },
    largeImage: {
      src: "",
      width: "",
      height: "",
    },
    data: {
      type: {
        name: "",
      },
      brandData: {
        type: {
          name: "",
        },
      },
      order_availability: {
        name: "",
      },
    },
    productVariants: [
      {
        price: "",
        value: "",
        pack: "",
        image: "",
      },
    ],
    variantsImages: [],
    id: [],
    showBrand: false,
    showDesc: true,
    bigImage: "",
    variantId: "",
    spec: "",
    price: 0,
    pincode: 0,
    quantity: 0,
    orderQuantity: 1,
    base_price: 0,
    product_id: 0,
    pack: "",
    cartDetails: [
      {
        sub_total: "",
        quantity: "",

        product: {
          product_name: "",
        },
        product_variant: {
          image: [],
        },
      },
    ],
  };
  componentDidMount() {
    let id = this.props.match.params.id;
    console.log(this.props);
    let apiurl = Constants.getUrl.ProductDetail + id;
    Axios.get(apiurl).then((res) => {
      console.log(res.data.specification.slice(47, 92));
      console.log(res);
      this.setState({
        spec: res.data.specification,
      });
      console.log(JSON.parse(res.data.productVariants[0].image));
      Axios.get(Constants.getUrl.Related + res.data.primary_category_id).then(
        (res) => {
          console.log(res);
          this.setState({
            related: res.data,
          });
        }
      );
      // Axios.get(Constants.getUrl.proByBrand + res.data.brand_id).then(res => {
      //   console.log(res);
      //   this.setState({
      //     related: res.data.product
      //   });
      // });
      this.setState(
        {
          data: res.data,
          product_id: id,
          bName: res.data.brand.name,
          bImg: res.data.brand.image,
          bDesc: res.data.brand.description,
          productVariants: res.data.productVariants,
          variantId: res.data.productVariants[0].id,
          variantsImages: JSON.parse(res.data.productVariants[0].image),
          price: res.data.productVariants[0].price,
          actImage: JSON.parse(res.data.productVariants[0].image)[0],
          base_price: res.data.productVariants[0].price,
          quantity: res.data.productVariants[0].quantity,
          pack: res.data.productVariants[0].pack,
          brandData: res.data.brand.name,
        },
        () => {
          console.log(this.state.data.product_name);
          quantities = [];
          for (let i = 1; i <= this.state.quantity; i++) {
            quantities.push(i);
          }
          this.setState({ quantities });
          let fcArray = [];
          let fsArray = [];
          this.state.productVariants.forEach((v) => {
            if (fcArray.includes(v.color) === false) {
              fcArray.push(v.color);
            }
            if (fsArray.includes(v.size) === false) {
              fsArray.push(v.size);
            }
            console.log(fsArray);
            console.log(fcArray);
            this.setState(
              {
                bigImage: this.state.variantsImages[0],
                cArray: fcArray,
                sArray: fsArray,
              },
              () => {
                Axios.get(
                  "https://admin.vishalkids.in/api/product-variant/get_variant?size=" +
                    this.state.sArray[0] +
                    "&product_id=" +
                    this.state.product_id
                ).then((res) => {
                  console.log(res.data);
                  let pack = [];
                  res.data.forEach((v) => {
                    pack.push(v);
                  });
                  console.log(pack);
                  this.setState(
                    {
                      packOf: pack,
                      size: true,
                      sizeValue: this.state.sArray[0],
                      actSize: this.state.sArray[0],
                      actPack: pack[0].pack,
                    },
                    () => {
                      Axios.get(
                        "https://admin.vishalkids.in/api/product-variant/get_variant?pack_of=" +
                          this.state.packOf[0].pack +
                          "&product_id=" +
                          this.state.product_id +
                          "&size=" +
                          this.state.sArray[0]
                      ).then((res) => {
                        console.log(res.data);
                        this.setState(
                          {
                            quantity: res.data[0].quantity,
                            base_price: res.data[0].price,
                            price: res.data[0].price,
                          },
                          () => {
                            quantities = [];
                            for (let i = 1; i <= this.state.quantity; i++) {
                              quantities.push(i);
                            }
                            this.setState({ quantities });
                          }
                        );
                      });
                    }
                  );
                });
              }
            );
          });
        }
      );
    });
  }
  componentWillUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      window.scrollTo(0, 0);
      let id = prevProps.match.params.id;
      let apiurl = Constants.getUrl.ProductDetail + id;
      Axios.get(apiurl).then((res) => {
        console.log(res.data);
        this.setState({
          spec: res.data.specification,
        });
        console.log(JSON.parse(res.data.productVariants[0].image));
        // Axios.get(Constants.getUrl.Related+)
        Axios.get(Constants.getUrl.proByBrand + res.data.brand_id).then(
          (res) => {
            console.log(res);
            this.setState({ related: res.data.product });
          }
        );
        this.setState(
          {
            data: res.data,
            product_id: id,
            bName: res.data.brand.name,
            bImg: res.data.brand.image,
            bDesc: res.data.brand.description,
            productVariants: res.data.productVariants,
            variantId: res.data.productVariants[0].id,
            variantsImages: JSON.parse(res.data.productVariants[0].image),
            price: res.data.productVariants[0].price,
            actImage: JSON.parse(res.data.productVariants[0].image)[0],
            base_price: res.data.productVariants[0].price,
            quantity: res.data.productVariants[0].quantity,
            pack: res.data.productVariants[0].pack,
            brandData: res.data.brand.name,
          },
          () => {
            quantities = [];
            for (let i = 1; i <= this.state.quantity; i++) {
              quantities.push(i);
            }
            this.setState({ quantities });
            let fcArray = [];
            let fsArray = [];
            this.state.productVariants.forEach((v) => {
              if (fcArray.includes(v.color) === false) {
                fcArray.push(v.color);
              }
              if (fsArray.includes(v.size) === false) {
                fsArray.push(v.size);
              }
              console.log(fsArray);
              console.log(fcArray);
              this.setState(
                {
                  bigImage: this.state.variantsImages[0],
                  cArray: fcArray,
                  sArray: fsArray,
                },
                () => {
                  Axios.get(
                    "https://admin.vishalkids.in/api/product-variant/get_variant?size=" +
                      this.state.sArray[0] +
                      "&product_id=" +
                      this.state.product_id
                  ).then((res) => {
                    console.log(res.data);
                    let pack = [];
                    res.data.forEach((v) => {
                      pack.push(v);
                    });
                    console.log(pack);
                    this.setState(
                      {
                        packOf: pack,
                        size: true,
                        sizeValue: this.state.sArray[0],
                        actSize: this.state.sArray[0],
                        actPack: pack[0].pack,
                      },
                      () => {
                        Axios.get(
                          "https://admin.vishalkids.in/api/product-variant/get_variant?pack_of=" +
                            this.state.packOf[0].pack +
                            "&product_id=" +
                            this.state.product_id +
                            "&size=" +
                            this.state.sArray[0]
                        ).then((res) => {
                          console.log(res.data);
                          this.setState(
                            {
                              quantity: res.data[0].quantity,
                              base_price: res.data[0].price,
                              price: res.data[0].price,
                            },
                            () => {
                              quantities = [];
                              for (let i = 1; i <= this.state.quantity; i++) {
                                quantities.push(i);
                              }
                              this.setState({ quantities });
                            }
                          );
                        });
                      }
                    );
                  });
                }
              );
            });
          }
        );
      });
    }
  }
  selectSize = (size) => {
    console.log(size);
    Axios.get(
      "https://admin.vishalkids.in/api/product-variant/get_variant?size=" +
        size +
        "&product_id=" +
        this.state.product_id
    ).then((res) => {
      console.log(res.data);
      let pack = [];
      res.data.forEach((v) => {
        pack.push(v);
      });
      console.log(pack);
      this.setState(
        {
          packOf: pack,
          size: true,
          actColor: "",
          actSize: size,
          actPack: pack[0].pack,
          sizeValue: size,
        },
        () => {
          Axios.get(
            "https://admin.vishalkids.in/api/product-variant/get_variant?pack_of=" +
              this.state.packOf[0].pack +
              "&product_id=" +
              this.state.product_id +
              "&size=" +
              this.state.sizeValue
          ).then((res) => {
            console.log(res.data);
            this.setState(
              {
                quantity: res.data[0].quantity,
                base_price: res.data[0].price,
                price: res.data[0].price,
                orderQuantity: 1,
              },
              () => {
                quantities = [];
                console.log(quantities);
                for (let i = 1; i <= this.state.quantity; i++) {
                  quantities.push(i);
                }
                this.setState({ quantities });
              }
            );
          });
        }
      );
    });
    // console.log(e.target.id);
    // this.setState({ variantId: e.target.id });

    // let variant = this.state.productVariants.find(
    //   productVariant => productVariant.id == this.state.variantId
    // );
    // console.log(variant);
    // this.setState(
    //   {
    //     variantsImages: JSON.parse(variant ? variant.image : ""),
    //     price: variant ? variant.price : "",
    //     base_price: variant ? variant.price : "",
    //     pack: variant ? variant.pack : "",
    //     quantity: variant ? variant.quantity : ""
    //   },
    //   () => {
    //     // console.log(this.state.variantsImages);
    //     this.setState(
    //       {
    //         bigImage: this.state.variantsImages[0]
    //       },
    //       () => {}
    //     );
    //   }
    // );
  };
  selectColor = (color) => {
    Axios.get(
      "https://admin.vishalkids.in/api/product-variant/get_variant?product_id=" +
        this.state.product_id +
        "&color=" +
        encodeURIComponent(color)
    ).then((res) => {
      console.log(res.data);
      let pack = [];
      res.data.forEach((v) => {
        pack.push(v);
      });
      console.log(pack);
      this.setState(
        {
          packOf: pack,
          size: false,
          actColor: color,
          actSize: "",
          actPack: pack[0].pack,
          colorValue: encodeURIComponent(color),
          bigImage: JSON.parse(res.data[0].image),
        },
        () => {
          console.log(this.state.bigImage);
          Axios.get(
            "https://admin.vishalkids.in/api/product-variant/get_variant?pack_of=" +
              this.state.packOf[0].pack +
              "&product_id=" +
              this.state.product_id +
              "&color=" +
              this.state.colorValue
          ).then((res) => {
            console.log(res.data);
            this.setState(
              {
                quantity: res.data[0].quantity,
                base_price: res.data[0].price,
                price: res.data[0].price,
                orderQuantity: 1,
              },
              () => {
                quantities = [];
                for (let i = 1; i <= this.state.quantity; i++) {
                  quantities.push(i);
                }
                this.setState({ quantities });
              }
            );
          });
        }
      );
    });
  };
  selectPack = (pack) => {
    console.log(pack);
    let value = this.state.size ? this.state.sizeValue : this.state.colorValue;
    let sizeOrColor = this.state.size ? "&size=" : "&color=";
    Axios.get(
      "https://admin.vishalkids.in/api/product-variant/get_variant?pack_of=" +
        pack +
        "&product_id=" +
        this.state.product_id +
        sizeOrColor +
        value
    ).then((res) => {
      console.log(res.data);
      this.setState(
        {
          quantity: res.data[0].quantity,
          actPack: pack,
          base_price: res.data[0].price,
          price: res.data[0].price,
          orderQuantity: 1,
          // bigImage: res.data.image
        },
        () => {
          quantities = [];
          for (let i = 1; i <= this.state.quantity; i++) {
            quantities.push(i);
          }
          this.setState({ quantities });
        }
      );
    });
  };
  setImage = (e) => {
    let ImageSrc = e.target.src;
    this.setState({
      bigImage: ImageSrc,
      actImage: ImageSrc,
    });
  };
  changeQuantity = (e) => {
    this.setState(
      {
        orderQuantity: e.target.value,
      },
      () => {
        this.setState({
          price: this.state.orderQuantity * this.state.base_price,
        });
      }
    );
  };
  pinCheck = () => {
    Axios.post(Constants.postUrl.pinCheck, {
      pincode: this.state.pincode,
    }).then((res) => {
      console.log(res);
      if (res.data.status === "failed") {
        this.setState({ warn: true });
        setTimeout(() => {
          this.setState({ warn: false });
        }, 1000);
      } else {
        this.setState({ success: true });
        setTimeout(() => {
          this.setState({ success: false });
        }, 1000);
      }
    });
  };

  addToCart = () => {
    // if (this.state.ulti === false) {
    //   this.setState({warnee:true});
    // } else {
    if (init() === "success") {
      let payload = {
        product_id: parseInt(this.state.product_id),
        product_variant_id: this.state.variantId,
        quantity: this.state.orderQuantity,
        max_quantity: this.state.quantity,
        unit_price: this.state.unit_price,
      };
      Axios.post(Constants.postUrl.cart, payload).then((res) => {
        Axios.get(Constants.getUrl.cartDetails).then((res) => {
          this.setState(
            {
              cartDetails: res.data,
              added: true,
            },
            () => {
              this.props.onAddToCart(this.state.cartDetails);
            }
          );
          setTimeout(() => {
            this.setState({ added: false });
          }, 1000);
        });
      });
    } else {
      console.log(this.state.data);
      let payload = {
        product_variant_id: this.state.variantId,
        product: {
          product_name: this.state.data.product_name,
        },
        product_variant: {
          image: this.state.images,
        },
        quantity: parseInt(this.state.orderQuantity),
        product_id: parseInt(this.state.product_id),
        max_quantity: this.state.quantity,
        unit_price: this.state.unit_price,
      };
      if (addToCartWithoutLogin(payload) === "success") {
        const localData = localStorage.getItem("cart");
        const parsedData = JSON.parse(localData);
        this.props.onAddToCart(parsedData);
        this.setState({ added: true });
        setTimeout(() => {
          this.setState({ added: false });
        }, 1000);
      }
    }
    // }
  };
  updateCart = (id) => {
    console.log(this.state.variantId);
    Axios.get(Constants.getUrl.cartDetails).then((res) => {
      this.setState(
        {
          cartDetails: res.data,
        },
        () => {
          console.log(this.state.cartDetails);
          const cartObj = this.state.cartDetails.find(
            (crt) => crt.product_id == id
          );
          console.log(cartObj);
          if (cartObj) {
            console.log("product Exist");
            const Cartid = cartObj.id;
            const Quantity = cartObj.quantity;
            const payload = {
              product_id: cartObj.product_id,
              product_variant_id: cartObj.product_variant.id,
              quantity: Quantity + 1,
            };
            console.log(payload);
            Axios.post(
              Constants.postUrl.updateQuantityInCart + Cartid,
              payload
            ).then((res) => {
              console.log(res);
              if (res.data) {
                this.setState({
                  cartDetails: res.data,
                });
                // this.props.onAdd(res.data);
              }
            });
          } else {
            let payload = {
              product_id: parseInt(this.state.product_id),
              product_variant_id: this.state.variantId,
              quantity: this.state.orderQuantity,
              product_name: this.state.data.product_name,
              img_url: this.state.bigImage,
              price: this.state.base_price,
            };
            console.log(payload);
            if (login()) {
              Axios.post(Constants.postUrl.cart, payload).then((res) => {
                this.setState(
                  {
                    cartDetails: res.data,
                    showNotif: true,
                    notiMessage: "Item Added  Successfully...",
                  },
                  () => {
                    console.log(this.state.cartDetails);
                    // this.props.onAdd(res.data);
                    // this.props.onAddToCart(this.state.cartDetails);
                  }
                );
                setTimeout(() => {
                  this.setState({
                    showNotif: false,
                  });
                }, 2000);
              });
            } else {
              if (addToCartWithoutLogin(payload) === "success") {
                this.setState({
                  showNotif: true,
                  notiMessage: "Item Added  Successfully...",
                });
                setTimeout(() => {
                  this.setState({
                    showNotif: false,
                  });
                }, 2000);
                const localData = localStorage.getItem("cart");
                const parsedData = JSON.parse(localData);
                this.props.onAdd(parsedData);
              }
            }
          }
        }
      );
    });
    // const cartObj = this.state.cartDetails.find(
    //   crt => crt.product_variant.id == id
    // );
    // console.log(cartObj);
  };
  cartData = (e) => {
    let payload = {
      product_id: parseInt(this.state.product_id),
      product_variant_id: this.state.variantId,
      quantity: this.state.orderQuantity,
      product_name: this.state.data.product_name,
      img_url: this.state.bigImage,
      price: this.state.base_price,
    };
    console.log(payload);
    console.log(login());
    if (login()) {
      // this.props.onAddToCart("some");
      // console.log(payload);

      Axios.post(Constants.postUrl.cart + this.state.id, payload).then(
        (res) => {
          Axios.get(Constants.getUrl.cartDetails).then((res) => {
            this.setState(
              {
                cartDetails: res.data,
                showNotif: true,
                notiMessage: "Item Added  Successfully...",
              },
              () => {
                this.props.onAddToCart(this.state.cartDetails);
              }
            );
            setTimeout(() => {
              this.setState({
                showNotif: false,
              });
            }, 2000);
          });
        }
      );
    } else {
      if (addToCartWithoutLogin(payload) === "success") {
        const localData = localStorage.getItem("cart");
        const parsedData = JSON.parse(localData);
        // this.setState({ cartDetails: parsedData });
        this.props.onAddToCart(parsedData);
      }
    }
  };
  showBrandInfo() {
    this.setState({
      showBrand: !this.state.showBrand,
      showDesc: false,
    });
  }
  showDesc() {
    this.setState({
      showDesc: true,
      showBrand: false,
    });
  }
  buyNow = () => {
    this.cartData();
    window.location.href = "/checkout";
  };
  addItems = (payload) => {
    // console.log(payload);

    this.setState(
      {
        cartDetails: payload,
      },
      () => {
        this.props.onAddToCart(this.state.cartDetails);
      }
    );
  };
  render() {
    return (
      <div>
        {/* <Header cartItems={this.state.cartDetails} />
        <Menu /> */}

        <div className="productDetails">
          <div className="container">
            {this.state.showNotif ? (
              <NotificationonAddCart message={this.state.notiMessage} />
            ) : null}
            <div className="row rowpadd">
              <div className="col-md-6 col-sm-6">
                {/* <img src={this.state.bigImage} alt="some" width="100%" /> */}

                <ReactImageMagnify
                  {...{
                    style: {
                      zIndex: 99,
                    },
                    smallImage: {
                      alt: "some product",
                      isFluidWidth: true,
                      src: this.state.bigImage,
                    },
                    largeImage: {
                      src: this.state.bigImage,
                      width: 1200,
                      height: 1800,
                    },
                  }}
                />
                <div className="row rowpadd gridDetails">
                  {this.state.variantsImages.map((singleImg, index) => (
                    <div
                      style={{
                        border:
                          this.state.actImage === singleImg
                            ? "2px solid #ef0081"
                            : "",
                        borderRadius: "5px",
                      }}
                      className="col-md-3 col-xs-3 col-sm-3"
                      key={index}
                    >
                      <img
                        src={singleImg}
                        alt="some"
                        width="100%"
                        onClick={(e) => {
                          this.setImage(e);
                        }}
                      />
                    </div>
                  ))}
                </div>
                <div className="desc">
                  {" "}
                  <b>Note:</b> {this.state.data.note}
                </div>
              </div>

              <div className="col-md-6 col-sm-6">
                <div className="text">
                  <div>
                    <h1 className="heading">{this.state.data.product_name}</h1>
                    {/* <h1> {this.state.data.primary_category.description} </h1> */}
                    <p className="price">₹ {this.state.base_price}</p>
                    {/* <p className="ratings">5</p> */}
                    <p className="description">
                      {this.state.data.product_overview}
                    </p>
                    {this.state.sArray.length>0?(

                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft">Size : </p>
                      </div>
                      <div className="col-md-8 col-xs-6 col-sm-6">
                        {this.state.sArray.map(
                          (varient, index) => (
                            // varient.type === "size" ? (
                            <p
                              key={index}
                              style={{
                                border:
                                  this.state.actSize === varient
                                    ? "1px solid #ef0081"
                                    : "1px solid lightgray",
                              }}
                              className="font size blockElement"
                              id={varient}
                              onClick={() => {
                                this.selectSize(varient);
                              }}
                            >
                              {/* {varient.value} */}
                              {varient}
                            </p>
                          )
                          // ) : null
                        )}
                      </div>
                    </div>
                    ):null}
                 {this.state.cArray.length>0?(
                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft">Color : </p>
                      </div>
                      <div className="col-md-8 col-sm-6 col-xs-6">
                        {this.state.cArray.map((varient, index) => (
                          <p
                            key={index}
                            className="font color_cloths blockElement"
                            onClick={() => {
                              this.selectColor(varient);
                            }}
                            id={varient}
                            style={{
                              border:
                                this.state.actColor === varient
                                  ? "1px solid #ef0081"
                                  : "1px solid lightgray",
                              backgroundColor: varient,
                            }}
                          ></p>
                        ))}
                      </div>
                    </div>
                    ):null}
                    {this.state.packOf.length>0?(

                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft">Pack : </p>
                      </div>

                      <div className="col-md-8 col-xs-6 col-sm-6">
                        {this.state.packOf.map((v, i) => (
                          <p
                            onClick={() => {
                              this.selectPack(v.pack);
                            }}
                            style={{
                              border:
                                this.state.actPack === v.pack
                                  ? "1px solid #ef0081"
                                  : "1px solid lightgray",
                            }}
                            className="font size blockElement "
                          >
                            Pack Of {v.pack}
                          </p>
                        ))}
                      </div>
                    </div>
                    ):null}

                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft">Quantity : </p>
                      </div>
                      <div className="col-md-8 col-xs-6 col-sm-6">
                        <p className="font ">
                          <select
                            className="quantity"
                            name="quantity"
                            onChange={this.changeQuantity}
                            value={this.state.orderQuantity}
                          >
                            {this.state.quantities.map((v) => (
                              <option value={v}>{v}</option>
                            ))}
                          </select>
                        </p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft">Subtotal : </p>
                      </div>
                      <div className="col-md-8 col-xs-6 col-sm-6">
                        <p className="font color">{this.state.price}</p>
                      </div>
                    </div>
                    {this.state.data.type ? (
                      <div className="row">
                        <div className="col-md-4 col-xs-6 col-sm-6">
                          <p className="fontLeft">Product Type : </p>
                        </div>
                        <div className="col-md-8 col-xs-6 col-sm-6">
                          <p className="font">
                            {this.state.data.type
                              ? this.state.data.type.name
                              : null}
                          </p>
                        </div>
                      </div>
                    ) : null}
                    {this.state.brandData ? (
                      <div className="row">
                        <div className="col-md-4 col-xs-6 col-sm-6">
                          <p className="fontLeft">Brand Type : </p>
                        </div>
                        <div className="col-md-8 col-xs-6 col-sm-6">
                          <p className="font">{this.state.brandData}</p>
                        </div>
                      </div>
                    ) : null}

                    <div className="row">
                      <div className="col-md-4 col-xs-6 col-sm-6">
                        <p className="fontLeft ">Availability : </p>
                      </div>
                      <div className="col-md-8 col-xs-6 col-sm-6">
                        <p className="font font_color">
                          {this.state.quantity > 0 ? (
                            this.state.quantity + " In Stock"
                          ) : (
                            <span style={{ color: "red" }}>Out of Stock</span>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="breakadd">
                      <button
                        disabled={this.state.quantity === 0 ? true : false}
                        className="button btnmargin"
                        // onClick={this.cartData}
                        onClick={() => {
                          this.updateCart(this.props.location.state.proId);
                        }}
                      >
                        <i className="icofont-shopping-cart"></i> Add to cart
                      </button>
                      {/* <Link
                        to={{
                          pathname: "/checkout",
                          state: { productData: this.state.data },
                          quantity: this.state.quantity,
                          price: this.state.price
                        }}
                      > */}
                      <button
                        disabled={this.state.quantity === 0 ? true : false}
                        onClick={this.buyNow}
                        className="button margin"
                      >
                        Buy it now
                      </button>
                      {/* </Link> */}
                    </div>
                    {/* <div className="desc">
                      {" "}
                      <b>Note:</b> {this.state.data.note}
                    </div> */}
                    <label htmlFor="">Check Delivery</label>
                    <br />
                    <input
                      type="text"
                      onChange={(e) => {
                        this.setState({ pincode: e.target.value });
                      }}
                      style={{
                        width: "65%",
                        border: "1px solid #ddd",
                        padding: "10px",
                        borderRadius: "10px",
                        marginBottom: "20px",
                        marginTop: "10px",
                        marginLeft: "-12px",
                      }}
                    />
                    <input
                      type="button"
                      value="Check"
                      onClick={this.pinCheck}
                      style={{
                        backgroundColor: "#62575f",
                        color: "white",
                        border: "none",
                        borderRadius: "10px",
                        padding: " 10px 30px",
                        marginLeft: "10px",
                      }}
                    />

                    {this.state.warn ? (
                      <span style={{ color: "red", marginBottom: "10px" }}>
                        Out of Service!
                      </span>
                    ) : null}
                    {this.state.success ? (
                      <span style={{ color: "green", marginBottom: "10px" }}>
                        In Service
                      </span>
                    ) : null}
                    {/* <p className="icon_box">
                      <span className="box">
                        <i className="icofont-facebook"></i>
                      </span>
                      <span className="box">
                        <i className="icofont-twitter"></i>
                      </span>
                      <span className="box">
                        <i className="icofont-print"></i>
                      </span>
                      <span className="box">
                        <i className="icofont-ui-message"></i>
                      </span>
                      <span className="box">
                        <i className="icofont-plus"></i>
                      </span>
                      <span className="box">0</span>
                    </p> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="break">
              <button
                className="button"
                onClick={() => {
                  this.showDesc();
                }}
                style={{
                  backgroundColor: this.state.showDesc ? "#ef0081" : "#62575f",
                }}
              >
                {" "}
                Description{" "}
              </button>
              <button
                className="button  margin"
                onClick={() => {
                  this.showBrandInfo();
                }}
                style={{
                  backgroundColor: this.state.showBrand ? "#ef0081" : "#62575f",
                }}
              >
                Brand Information
              </button>

              {/* <button className="button">shipping Detail</button> */}
              {this.state.showDesc ? (
                <div>
                  <p className="paragarph">
                    {renderHTML(
                      this.state.data.specification
                        ? this.state.data.specification
                        : ""
                    )}
                  </p>
                  {this.state.spec ? (
                    <ReactPlayer
                      // width="600px"
                      // height="300px"
                      className="reactplayer"
                      controls={true}
                      playing={false}
                      url={this.state.spec.slice(
                        this.state.spec.search("<oembed") + 12,
                        this.state.spec.search("></oembed>")
                      )}
                    />
                  ) : null}
                </div>
              ) : null}
              {this.state.showBrand ? (
                // {/* <button className="button  margin">Brand Info</button> */}
                <div className="row" style={{ marginTop: "20px" }}>
                  <div className="col-sm-3">
                    {/* <p className="paragarph"> */}
                    <img
                      src={this.state.bImg}
                      width="100%"
                      className="brandImg"
                      alt="some"
                    />
                  </div>
                  <div className="col-sm-9">
                    <h6 style={{ fontSize: "15px", fontWeight: "bold" }}>
                      {this.state.bName}
                    </h6>
                    <p>
                      {renderHTML(this.state.bDesc ? this.state.bDesc : "")}
                    </p>
                  </div>
                  {/* </p> */}
                </div>
              ) : null}
            </div>

            <div className="conatiner">
              <div className="row rowpadding">
                <div className="textCenter">
                  <h4>related products</h4>
                  <h2>From this Collection</h2>
                </div>

                {this.state.related
                  .filter((v, i) => i <= 3)
                  .map((singledata, index) => (
                    <Products
                      onAdd={this.addItems}
                      id={singledata.id}
                      key={index}
                      name={singledata.product_name}
                      brand={singledata.brand.name}
                      price={
                        singledata.product_variant[0]
                          ? singledata.product_variant[0].price
                          : ""
                      }
                      url={
                        singledata.product_variant[0]
                          ? JSON.parse(singledata.product_variant[0].image)[0]
                          : ""
                      }
                      variantId={
                        singledata.product_variant[0]
                          ? singledata.product_variant[0].id
                          : ""
                      }
                      note={singledata.note}
                    />
                  ))}
              </div>
              {/* <div className="row rowpadding">
                <div className="textCenter">
                  <h2>Recently Viewed Products</h2>
                </div>

                {this.state.variantsImages.map((singleImg, index) => (
                  <Products
                    key={index}
                    url={singleImg}
                    name={this.state.data.product_name}
                    brand={this.state.data.brand.name}
                    price={this.state.data.productVariants[0].price}
                  />
                ))}
              </div> */}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
