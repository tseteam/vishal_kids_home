// import { isAuth, authToken } from "./AuthHelper";

const init = () => {
  const authToken = localStorage.getItem("accessToken");
  const isAuth = authToken ? true : false;
  let w = window;
  let status = false;
  w.axios = require("axios");
  // console.log(authToken);
  // alert();
  if (isAuth) {
    w.axios.defaults.headers.common["Authorization"] = "Bearer " + authToken;
    status = true;
  } else {
    status = false;
  }
  return status ? "success" : "failed";
};
export default init;
