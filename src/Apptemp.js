import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./assets/css/bootstrap.min.css";
import "./assets/css/icofont.min.css";
import "./assets/css/App.css";
import Home from "./Home";
import Products from "./Partials/Products";
import ProductDetail from "./ProductDetail";
import Checkout from "./Checkout";
import Register from "./Auth/Register";
import SignIn from "./Auth/SignIn";
import ProductList from "./ProductList";
import Contact from "./Contact";
import About from "./About";
import Header from "./Partials/Header";
import Menu from "./Components/Menu";
import Footer from "./Partials/Footer";

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Menu />
        <Route exact path="/" component={Home} />
        <Route path="/productlist" component={ProductList} />
        <Route path="/product-detail/:id" component={ProductDetail} />
        {/* <Route path="/products" component={Products} /> */}
        <Route path="/contact" component={Contact} />
        <Route path="/checkout" component={Checkout} />
        <Route path="/register" component={Register} />
        <Route path="/sign-in" component={SignIn} />
        <Route path="/about" component={About} />
        {/* <Route path="/user-verification" component={UserVerification} /> */}
        <Route path="/user" component={User} />
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
