import React from "react";
import { Link } from "react-router-dom";
import Constants from "./variable/Constants";
import Axios from "axios";
// import Lottie from "react-lottie";
// import animationData from "../src/assets/cart.json";
import login from "./Components/LoginCheck.jsx";
import addToCartWithoutLogin from "./Components/WithoutLoginCartHandler";
import NotificationonAddCart from "./Components/NotificationonAddCart";

export default class Products extends React.Component {
  state = {
    products: [],
    product_id: 0,
    product_variant_id: this.props.variantId,
    quantity: 1,
    showNotif: false,
    notiMessage: "",
    cartDetails: []
  };
  componentDidMount() {
    let id = this.props.id;
    this.setState({
      product_id: id
    });
    if (login()) {
      Axios.get(Constants.getUrl.cartDetails).then(res => {
        this.setState({ cartDetails: res.data });
      });
    }
  }
  addToCart = (e, id, product) => {
    let payload = {
      product_id: parseInt(this.state.product_id),
      product_variant_id: this.state.product_variant_id,
      quantity: this.state.quantity,
      product_name: this.props.name,
      img_url: this.props.url,
      price: this.props.price
    };
    if (login()) {
      Axios.post(Constants.postUrl.cart, payload).then(resp => {
        console.log(resp);
        this.setState({
          showNotif: true,
          notiMessage: "Item Added  Successfully..."
        });

        Axios.get(Constants.getUrl.cartDetails).then(res => {
          this.setState(
            {
              cartDetails: res.data,
              showNotif: false
            },
            () => {
              console.log(this.state.cartDetails);
              this.props.onAdd(res.data);
            }
          );
        });
      });
    } else {
      if (addToCartWithoutLogin(payload) === "success") {
        this.setState({
          showNotif: true,
          notiMessage: "Item Added  Successfully..."
        });
        const localData = localStorage.getItem("cart");
        const parsedData = JSON.parse(localData);
        this.setState({
          showNotif: false
        });
        this.props.onAdd(parsedData);
      }
    }
  };
  updateCart = id => {
    console.log(this.props.variantId);
    const cartObj = this.state.cartDetails.find(
      crt => crt.product_variant.id == id
    );
    console.log(cartObj);
    if (cartObj) {
      console.log("product Exist");
      const Cartid = cartObj.id;
      const Quantity = cartObj.quantity;
      const payload = {
        product_id: this.props.id,
        product_variant_id: this.props.variantId,
        quantity: Quantity + 1
      };
      console.log(payload);
      Axios.post(Constants.postUrl.updateQuantityInCart + Cartid, payload).then(
        res => {
          console.log(res);
          if (res.data) {
            this.setState({
              cartDetails: res.data
            });
            // this.props.onAdd(res.data);
          }
        }
      );
    } else {
      let payload = {
        product_id: parseInt(this.state.product_id),
        product_variant_id: this.state.product_variant_id,
        quantity: this.state.quantity,
        product_name: this.props.name,
        img_url: this.props.url,
        price: this.props.price
      };
      if (login()) {
        Axios.post(Constants.postUrl.cart, payload).then(res => {
          this.setState(
            {
              cartDetails: res.data,
              showNotif: true,
              notiMessage: "Item Added  Successfully..."
            },
            () => {
              console.log(this.state.cartDetails);
              // this.props.onAdd(res.data ? res.data : null);
            }
          );
          setTimeout(() => {
            this.setState({
              showNotif: false
            });
          }, 2000);
        });
      } else {
        if (addToCartWithoutLogin(payload) === "success") {
          this.setState({
            showNotif: true,
            notiMessage: "Item Added  Successfully..."
          });
          setTimeout(() => {
            this.setState({
              showNotif: false
            });
          }, 2000);
          const localData = localStorage.getItem("cart");
          const parsedData = JSON.parse(localData);
          this.props.onAdd(parsedData);
        }
      }
    }
  };
  render() {
    return (
      <div className="col-sm-3 product col-xs-6">
        {this.state.showNotif ? (
          <NotificationonAddCart message={this.state.notiMessage} />
        ) : null}
        <div className="card">
          <div className="productWrapper">
            {/* <Link
              to={`/product-detail/` + this.props.id}
              style={{ textDecoration: "none" }}
            > */}
            <div>
              <div className="layerOnimageProduct">
                <div className="btn">
                  <button
                    type="submit"
                    // onClick={() => {
                    //   this.addToCart();
                    // }}
                    // this.props.variantId, this.props.id
                    onClick={() => {
                      this.updateCart(this.props.variantId, this.props.id);
                    }}
                    className="pbtn"
                  >
                    {/* <i class="fa fa-shopping-cart"></i> */}
                    <i class="icofont-cart-alt"></i>
                  </button>
                  <button type="submit" style={{ top: "14%" }} className="pbtn">
                    {/* <i class="fa fa-shopping-cart"></i> */}
                    {/* <i class="icofont-cart-alt"></i> */}
                    <Link
                      // to={`/product-detail/` + this.props.id}
                      style={{ textDecoration: "none" }}
                      to={{
                        pathname: "/product-detail/" + this.props.id,
                        state: { proId: this.props.id }
                      }}
                    >
                      <i class="icofont-eye-alt"></i>
                    </Link>
                  </button>
                </div>
              </div>
              <div className="imgDiv">
                <img alt="some" src={this.props.url} width="100px" />
              </div>
              <div className="description">
                <div className="name">
                  <h4
                    className="productTitle"
                    style={{
                      // textAlign: "center",
                      color: " #62575f",
                      fontWeight: 100,
                      fontSize: "17px",
                      marginBottom: 0,
                      fontFamily: "Jua"
                    }}
                  >
                    {this.props.name.length > 15
                      ? this.props.name.substring(0, 25) + "..."
                      : this.props.name}
                  </h4>
                  <h4
                    className="productTitle_resp"
                    style={{
                      // textAlign: "center",
                      color: " #62575f",
                      fontWeight: 100,
                      fontSize: "17px",
                      marginBottom: 0,
                      fontFamily: "Jua"
                    }}
                  >
                    {this.props.name.length > 15
                      ? this.props.name.substring(0, 15) + "..."
                      : this.props.name}
                  </h4>
                  <h6
                    style={{
                      textAlign: "center",
                      fontFamily: "Nunito",
                      fontSize: "13px"
                    }}
                  >
                    {this.props.overView}{" "}
                  </h6>
                  {/* <h5 style={{ fontFamily: "Nunito" }}>
                    {" "}
                    Brand : {this.props.brand}{" "}
                  </h5> */}
                  <div style={{ display: "flex" }}>
                    <h5
                      className="productPrice"
                      style={{ fontFamily: "Nunito", color: " #62575f" }}
                    >
                      {" "}
                      {this.props.offer_price}{" "}
                    </h5>
                    {this.props.offer_price ? (
                      <s>
                        <h5
                          className="productPrice"
                          style={{
                            fontFamily: "Nunito",
                            color: " #62575f",
                            marginLeft: "60px",
                            textAlign: "right",
                            fontSize: "16px"
                          }}
                        >
                          {" "}
                          ₹ {this.props.price}{" "}
                        </h5>
                      </s>
                    ) : (
                      <h5
                        className="productPrice"
                        style={{
                          fontFamily: "Nunito",
                          color: " #62575f",
                          marginLeft: "60px",
                          textAlign: "right"
                        }}
                      >
                        {" "}
                        ₹ {this.props.price}{" "}
                      </h5>
                    )}
                  </div>
                </div>
              </div>
              {/* </Link> */}
            </div>
          </div>
          {/* <div className="btn">
            <button type="submit" onClick={this.addToCart}>
              <i class="fa fa-shopping-cart"></i>
            </button>
          </div> */}
        </div>
      </div>
    );
  }
}
