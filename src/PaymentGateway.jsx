import React, { Component } from "react";
import Axios from "axios";
import Constants from "./variable/Constants";
import Footer from "./Partials/Footer";
import { Redirect } from "react-router-dom";
import Loader from "../src/Components/Loader";

class PaymentGateway extends Component {
  state = {
    // productData: [
    //   {
    //     sub_total: 0,
    //     product_variant_id: 0,
    //     quantity: 0
    //   },
    //   {
    //     sub_total: 1000,
    //     product_variant_id: 1,
    //     quantity: 1
    //   }
    // ],
    payment_mode: "",
    total: 0,
    discount_amount: 1000,
    tax_amount: 500,
    coupon_id: 1,
    carts: [],
    // addressId: 0,
    // newAddressId: 0,
    address_id: 0,
    redirect: false,
    orderId: 0,
    loading: false
  };
  componentDidMount() {
    console.log(this.props);
    this.setState(
      {
        total: this.props.location.total
      },
      () => {
        // console.log(this.state.addressId);
      }
    );
    if (this.props.location.state.productData !== null) {
      console.log("test");

      this.state.carts.push({
        sub_total: this.props.location.state.productData.productVariants[0]
          .price,
        quantity: 1
        // product_variant_id: this.props.location.state.productData
        //   .productVariants[0].id
      });
    } else {
      // console.log("some");
      Axios.get(Constants.getUrl.cartDetails).then(res => {
        this.setState(
          {
            cartDetails: res.data
          },
          () => {
            this.state.cartDetails.forEach(element => {
              this.state.carts.push({
                sub_total: element.sub_total,
                quantity: element.quantity,
                product_variant_id: element.product_variant.id
              });
            });
          }
        );
      });
    }
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    this.setState({
      loading: true
    });
    e.preventDefault();
    // const addressId = this.props.location.addressId;
    const addressId = this.props.location.addressId;
    const newAddressId = this.props.location.newAddressId;
    const payload = {
      address_id: addressId === 0 ? newAddressId : addressId,
      payment_mode: this.state.payment_mode,
      total: this.state.total,
      discount_amount: 0,
      tax_amount: 0,
      coupon_id: 1,
      productData: this.state.carts
    };
    // console.log(payload);

    Axios.post(Constants.postUrl.paymentMode, payload).then(res => {
      // console.log(res);
      if (this.state.payment_mode === "COD") {
        // alert("COD");
        window.location.href = "/orderconfirmed";
      } else {
        const url = res.data.url + "?orderId=" + res.data.orderDetails.orderId;
        window.open(url, "_blank");

        this.setState({
          redirect: true,
          orderId: res.data.orderDetails.orderId
        });
      }
    });
  };
  render() {
    if (this.state.redirect) {
      // return <Redirect to="/paymentstatus" />;
      return (
        <Redirect
          to={{ pathname: "/paymentstatus", orderId: this.state.orderId }}
        />
      );
    } else {
      return (
        <div className="payment">
          {/* <Header cartItems={this.state.cartDetails} />
          <Menu /> */}
          <form onSubmit={this.handleSubmit}>
            <div className="mainDiv">
              <h3>PAYMENT OPTIONS</h3>
              <div className=" card">
                <div className="block">
                  {/* <div className="col-md-12"> */}
                  <input
                    type="radio"
                    name="payment_mode"
                    value="COD"
                    onChange={this.handleChange}
                  />
                  Cash On Delivery
                </div>
                {/* </div> */}
                {/* <div className="col-md-12"> */}
                <div className="block">
                  <input
                    type="radio"
                    name="payment_mode"
                    value="online"
                    onChange={this.handleChange}
                  />
                  Online
                </div>
              </div>
              <button type="submit">
                {this.state.loading ? <Loader /> : "Submit"}
              </button>
            </div>
          </form>
          <Footer />
        </div>
      );
    }
  }
}

export default PaymentGateway;
