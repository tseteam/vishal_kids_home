import React, { Component } from "react";
import Axios from "axios";
import init from "./helper/WindowToken";
import { Redirect } from "react-router-dom";
import Constants from "./variable/Constants";
import Footer from "./Partials/Footer";

export default class Checkout extends Component {
  state = {
    email: "",
    type: "",
    address: "",
    some: "",
    city: "",
    success: false,
    view: false,
    checked: false,
    pincode: "",
    state: "",
    addresses: [],
    warn: false,
    discount: 0,
    coupon: "",
    // checkout: [],
    cart: [],
    redirect: false,
    addressId: 0,
    total: 0,
    newAddressId: 0,
    productData: {},
    tax: 0,
    quantity: 0,
    price: 0,
    grandTotal: 0,
    tax_type: "",
    taxAmount: 0,
    sub_total: 0,
    subTotal: 0,
    id: 0,
    visible: false
  };
  componentDidMount() {
    console.log(this.props);
    let id = localStorage.getItem("id");
    let name = localStorage.getItem("name");
    let email = localStorage.getItem("email");
    if (this.props.location.state) {
      this.setState(
        {
          // productData: this.props.location.state.productData,
          // price: this.props.location.state.productData.productVariants[0].price,
          price: this.props.location.price,
          tax: this.props.location.state.productData.tax_type.value,
          total: this.props.location.state.productData.productVariants[0].price,
          tax_type: this.props.location.state.productData.tax_type.name,
          quantity: this.props.location.quantity
        },
        () => {
          // console.log(this.state.total);
          let value = this.state.tax / 100;
          let taxAmount = value * this.state.price;
          let sub_total = this.state.price - taxAmount;
          // console.log(sub_total);

          // console.log(taxAmount);
          this.setState({
            taxAmount,
            sub_total
          });
        }
      );
    
    } else {
      let url = Constants.getUrl.cartDetails;
      Axios.get(url).then(res => {
        console.log(res.data);

        // console.log(res.data[0].product_variant.price);

        this.setState(
          {
            // checkout: res.data.checkout,
            cart: res.data
            // total: res.data[0].product_variant.price
          },
          () => {
            // console.log(this.state.cart[0].product.tax_type.value);

            let value = this.state.cart[0].product.tax_type.value / 100;

            let taxAmount = value * this.state.cart[0].sub_total;
            // console.log(taxAmount);

            let subTotal = this.state.cart[0].sub_total - taxAmount;
            // console.log(subTotal);
            this.setState({
              subTotal
            });
            let sub_total = 0;

            this.state.cart.forEach(item => {
              sub_total += item.sub_total;
            });
            this.setState(
              {
                total: sub_total
              },
              () => {
                // console.log(this.state.total);
              }
            );
          }
        );
      });
    }
    Axios.get(Constants.getUrl.addresses).then(res => {
      // console.log(res.data.address);
      this.setState({
        addresses: res.data.address
      });
    });
    Axios.get(Constants.getUrl.userDetails).then(res => {
      console.log(res.data);
      this.setState(
        {
          userDetails: res.data,
          // userId: res.data.id
          id
        },
        () => {
          this.setState({
            // name: this.state.userDetails.name,
            name,
            email
            // email: this.state.userDetails.email
          });
        }
      );
    });
    Axios.get(Constants.getUrl.cartDetails).then(res => {
      // console.log(res.data);

      this.setState(
        {
          cartDetails: res.data
        },

        () => {
          // console.log(this.state.cartDetails);
        }
      );
    });
  }
  applyCoupon = () => {
    this.setState({ applying: true });
    Axios.post(Constants.postUrl.applyCoupon, {
      coupon: this.state.coupon
    }).then(res => {
      console.log(res.data);
      if (res.data.status === "failed") {
        this.setState({
          applying: false,
          message: res.data.message,
          warn: true,
          discount: 0
        });
        setTimeout(() => {
          this.setState({ warn: false });
        }, 2000);
      } else {
        this.setState({
          discount: parseFloat(res.data.value),
          success: true,
          applying: false
        });
        setTimeout(() => {
          this.setState({ success: false });
        }, 2000);
      }
    });
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    // alert();
    const payload = {
      address: this.state.address,
      city: this.state.city,
      pincode: parseInt(this.state.pincode),
      type: this.state.type,
      state: this.state.state
    };
    // console.log(payload);
    Axios.post(Constants.postUrl.address, payload).then(res => {
      console.log(res.data);
      this.setState({
        redirect: true,
        newAddressId: res.data.id
        // userId
      });
    });
  };
  selectAddress = (e, id) => {
    // console.log(id);

    this.setState(
      {
        redirect: true,
        addressId: id
      },
      () => {
        // console.log(this.state.addressId);
      }
    );
  };
  render() {
    if (init() === "success") {
      if (this.state.redirect) {
        return (
          <Redirect
            to={{
              pathname: "/paymentgateway",
              addressId: this.state.addressId,
              total: this.state.total - this.state.discount,
              newAddressId: this.state.newAddressId,
              state: {
                productData: this.props.location.state
                  ? this.props.location.state.productData
                  : null
              }
            }}
          />
        );
      } else {
        return (
          <div>
            <div>
              {/* <Header cartItems={this.state.cartDetails} />
              <Menu /> */}
              <div className="Checkout">
                <div className="" style={{ padding: "0 48px" }}>
                  <div className="row ">
                    {/* {this.state.checkout.map((element, index) => ( */}
                    <div className="col-md-7 firstsection col-sm-7 ">
                      {/* <img alt="some" src={element.brandlogo} /> */}

                      <p>
                        <span>Information</span>
                        <i className="icofont-rounded-right"></i>
                        <span>Shipping</span>
                        <i className="icofont-rounded-right"></i>
                        <span>Payment</span>
                        <i className="icofont-rounded-right"></i>
                      </p>

                      <div className="contactInfo">
                        <div className="row">
                          {this.state.addresses
                            ? this.state.addresses.map((address, index) => {
                                return (
                                  <div className="col-md-6">
                                    <div className="userAddress">
                                      <p>{address.address}</p>
                                      <p>{address.city}</p>
                                      <p>{address.state}</p>

                                      <div className="button">
                                        <button
                                          type="submit"
                                          onClick={e =>
                                            this.selectAddress(e, address.id)
                                          }
                                        >
                                          Deliver to this address
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                );
                              })
                            : null}
                        </div>
                      </div>
                      <form
                        onSubmit={this.handleSubmit}
                        className="addressForm"
                      >
                        <h4>Add New Address</h4>

                        <div className="shipping">
                          <input
                            type="text"
                            name="address"
                            placeholder="Address"
                            onChange={this.handleChange}
                            className="inputClass"
                          />

                          <input
                            type="text"
                            name="city"
                            placeholder="City"
                            onChange={this.handleChange}
                            className="inputClass"
                          />
                          <input
                            type="text"
                            name="state"
                            placeholder="State"
                            onChange={this.handleChange}
                            className="inputClass"
                          />

                          <div className="row rowmargin">
                            <div className="col-md-4">
                              <input
                                type="text"
                                name="pincode"
                                placeholder="Pin Code"
                                onChange={this.handleChange}
                                id="pinCode"
                              />
                            </div>
                            <div>
                              <input
                                type="radio"
                                value="work"
                                name="type"
                                className="inttype"
                                onChange={this.handleChange}
                              />
                              work
                              <input
                                type="radio"
                                value="home"
                                name="type"
                                className="inttype"
                                onChange={this.handleChange}
                              />
                              home
                            </div>
                          </div>

                          <div className="button">
                            <button type="submit">
                              Deliver to this address
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>

                    <h3 style={{ textAlign: "center" }}>Your Order</h3>
                    <div className="col-md-5 col-sm-5 cart ">
                      <div
                        className="padding row"
                        style={{
                          borderBottom: "1px solid #ccc",
                          width: "100%"
                        }}
                      >
                        <div className="col-md-4 col-xs-3">
                          <h5 style={{ fontWeight: "600" }}>Name</h5>
                        </div>
                        <div className="col-md-1 col-xs-2">
                          <h5 style={{ fontWeight: "600" }}>Qty</h5>
                        </div>
                        <div className="col-md-2 col-xs-2">
                          <h5 style={{ fontWeight: "600" }}>Price</h5>
                        </div>
                        <div className="col-md-3 col-xs-3">
                          <h5 style={{ fontWeight: "600" }}>Tax</h5>
                        </div>
                        <div className="col-md-2 col-xs-2">
                          <h5 style={{ fontWeight: "600" }}>Total</h5>
                        </div>
                      </div>
                      {Object.keys(this.state.productData).length === 0 ? (
                        this.state.cart.map((element, index) => {
                          return (
                            <div>
                              <div className="row Solidborder padding ">
                                <div className="col-md-4 col-xs-3">
                                  <h5>{element.product.product_name}</h5>
                                </div>
                                <div className="col-md-1 col-xs-2">
                                  <h5>{element.quantity}</h5>
                                </div>
                                <div className="col-md-2 col-xs-2">
                                  <h5>₹{element.product_variant.price}</h5>
                                </div>
                                <div className="col-md-3 col-xs-3">
                                  <h5>
                                    {element.product.tax_type.value}% (
                                    {element.product.tax_type.name})
                                  </h5>
                                </div>
                                <div className="col-md-2 col-xs-2">
                                  <h5>₹{element.sub_total}</h5>
                                </div>

                                {/* 
                                  <div className="col-md-6">
                                    <p className="fontColor">
                                      {element.product.product_name}
                                    </p>
                                  </div>
                                  <div className="col-md-6">
                                    <p
                                      className="rightalign"
                                      style={{ marginTop: "13px" }}
                                    >
                                      ₹{element.product_variant.price}
                                    </p>
                                  </div> */}
                              </div>
                              {/* <div className="row padding margin"> */}
                              {/* <div className="col-md-6">
                                    <p className="leftalign">Quantity</p>
                                    <p className="leftalign">Subtotal</p>
                                  </div>
                                  <div className="col-md-6">
                                    <p className="rightalign">
                                      {element.quantity}
                                    </p>
                                    <p className="rightalign">
                                      ₹{this.state.subTotal}
                                    </p>
                                  </div>
                                </div>
                                <div className="row Solidborder padding margin3 ">
                                  <div className="col-md-6">
                                    <p className="leftalign margin2">
                                      {element.product.tax_type.name}
                                    </p>
                                  </div>

                                  <div className="col-md-6">
                                    <p className="rightalign">
                                      {element.product.tax_type.value}%
                                    </p>
                                  </div>
                                </div>
                                <div className="row Solidborder padding">
                                  <div className="col-md-6">
                                    <p className="leftalign ">Total</p>
                                  </div>
                                  <div className="col-md-6">
                                    <p className="rightalign boldfont">
                                      ₹ {element.sub_total}
                                    </p>
                                  </div> */}
                              {/* </div> */}
                            </div>
                          );
                        })
                      ) : (
                        <div className="col-md-5 cart">
                          <div className="row Solidborder padding ">
                            <div className="col-md-6">
                              <img
                                // src={Constants.mainDomain + parseImageArray[0]}
                                alt=""
                              />
                              <p className="fontColor">
                                {this.state.productData.product_name}
                              </p>
                            </div>
                            <div className="col-md-6">
                              <p className="rightalign">₹{this.state.price}</p>
                            </div>
                          </div>
                          <div className="row padding margin">
                            <div className="col-md-6">
                              <p className="leftalign">Quantity</p>
                              <p className="leftalign">Subtotal</p>
                            </div>
                            <div className="col-md-6">
                              <p className="rightalign">
                                {this.state.quantity}
                              </p>
                              <p className="rightalign">
                                ₹ {this.state.taxAmount}
                              </p>
                            </div>
                          </div>

                          <div className="row Solidborder padding margin3 ">
                            <div className="col-md-6">
                              <p className="leftalign margin2">
                                {this.state.tax_type}
                              </p>
                            </div>

                            <div className="col-md-6">
                              <p className="rightalign">{this.state.tax}%</p>
                            </div>
                          </div>
                          <div className="row Solidborder padding">
                            <div className="col-md-6">
                              <p className="leftalign ">Total</p>
                            </div>
                            <div className="col-md-6">
                              <p className="rightalign boldfont">
                                ₹ {this.state.price}
                              </p>
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="row Solidborder fontColor">
                        <div className="col-md-6 col-xs-6">
                          <p style={{ marginTop: "16px", paddingLeft: "32px" }}>
                            {" "}
                            Total
                          </p>
                        </div>
                        <div className="col-md-6 col-xs-6">
                          <p className="rightalign boldfont">
                            ₹ {this.state.total}
                          </p>
                        </div>
                      </div>
                      <div className="row Solidborder fontColor">
                        <div className="col-md-6 col-xs-6">
                          <p style={{ marginTop: "16px", paddingLeft: "32px" }}>
                            {" "}
                            Discount
                          </p>
                        </div>
                        <div className="col-md-6 col-xs-6">
                          <p className="rightalign boldfont">
                            ₹ {this.state.discount}
                          </p>
                        </div>
                      </div>
                      <div className="row Solidborder fontColor">
                        <div className="col-md-6 col-xs-6">
                          <p style={{ marginTop: "16px", paddingLeft: "32px" }}>
                            {" "}
                            Grand Total
                          </p>
                        </div>
                        <div className="col-md-6 col-xs-6">
                          <p className="rightalign boldfont">
                            ₹ {this.state.total - this.state.discount}
                          </p>
                        </div>
                      </div>
                      <div>
                        <label className="checkbox-wrap checkbox-primary">
                          <input
                            type="checkbox"
                            onClick={() => {
                              if (this.state.checked === true) {
                                this.setState({
                                  checked: false,
                                  view: false,
                                  discount: 0
                                });
                              } else {
                                this.setState({ checked: true, view: true });
                              }
                            }}
                            checked={this.state.checked ? true : false}
                          />
                          <span className="checkmark"></span>
                        </label>
                        <span
                          style={{
                            position: "relative",
                            left: "10px",
                            bottom: "1px"
                          }}
                        >
                          I have a Coupon Code
                        </span>
                      </div>
                      {this.state.view ? (
                        <div>
                          <div className="input-group">
                            <input
                              type="text"
                              onChange={e => {
                                this.setState({ coupon: e.target.value });
                              }}
                              placeholder="Enter Coupon Code"
                              className="quantity form-control"
                            />
                          </div>
                          {this.state.warn ? (
                            <span style={{ color: "red" }}>
                              Coupon is Not Valid!
                            </span>
                          ) : null}
                          {this.state.success ? (
                            <span style={{ color: "green" }}>
                              Coupon Applied Successfully!
                            </span>
                          ) : null}
                          <span
                            onClick={this.applyCoupon}
                            style={{ width: "20%", marginTop: "10px" }}
                            className="btn btn-primary py-3 px-4"
                          >
                            Apply
                          </span>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
              <Footer />
            </div>
          </div>
        );
      }
    } else {
      return <Redirect to="/sign-in" />;
    }
  }
}
