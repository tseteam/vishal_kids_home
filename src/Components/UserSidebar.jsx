import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Axios from "axios";
import Constants from "../variable/Constants";

export default class UserSidebar extends Component {
  state = {
    userDetails: "",
    name: "",
    data: []
  };
  componentDidMount() {
    let name = localStorage.getItem("name");
    // let data = localStorage.getItem("data");
    Axios.get(Constants.getUrl.userDetails).then(res => {
      // console.log(res.data);
      this.setState(
        {
          userDetails: res.data,
          
          // data
        },
        () => {
          // console.log(this.state.data);
        }
      );
    });
  }
  logout = e => {
    localStorage.clear();
  };
  render() {
    return (
      <div className="sidebar">
        <div className="hello">
          <div className="row userRow">
            <div className="col-md-3 col-xs-3 col-sm-3">
              <img
                src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/profile-pic-male_2fd3e8.svg"
                alt=""
                width="60px"
                className="img"
              />
            </div>
            <div className="col-md-9 col-xs-9 col-sm-9">
              <h3>{this.state.userDetails.name}</h3>
              {/* <h3>{this.state.data.name}</h3> */}
            </div>
          </div>
        </div>
        <div className="orders">
          <NavLink
            to="/user/order"
            className="active"
            activeClassName="selectedLink"
          >
            <h3>My Orders</h3>
          </NavLink>
        </div>
        <div className="setting">
          <h3>Account Setting</h3>
          <NavLink
            to="/user/info"
            className="active"
            activeClassName="selectedLink"
          >
            <p>Profile Information</p>
          </NavLink>
          <NavLink
            to="/user/manage"
            className="active"
            activeClassName="selectedLink"
          >
            <p>Manage Address</p>
          </NavLink>
        </div>
        <div className="logout">
          <NavLink to="/" className="active" onClick={this.logout}>
            <h3>Logout</h3>
          </NavLink>
        </div>
      </div>
    );
  }
}
