import React, { Component } from "react";
import Axios from "axios";

export default class Slider extends Component {
  state = {
    slider: []
  };
  componentDidMount() {
    let url = "Slider.json";
    Axios.get(url).then(res => {
      console.log(res.data);

      this.setState({
        slider: res.data.slider
      });
    });
  }
  render() {
    return (
      <div className="Slider">
        {this.state.slider.map((singleElement, index) => (
          <div
            key={index}
            className="Bgimg"
            style={{
              backgroundImage: "url(" + singleElement.ImgUrl + ")"
            }}
          >
            <div className="layer"></div>
            <div className="textcenter">
              <h1>{singleElement.Heading}</h1>
              <p>
                <i>{singleElement.Offer}</i>
              </p>
              <button>{singleElement.Button}</button>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
