import React from "react";
import Axios from "axios";

export default class BigStore extends React.Component {
  state = {
    big_store: []
  };
  componentDidMount() {
    let url = "BigStore.json";
    Axios.get(url).then(res => {
      this.setState({
        big_store: res.data.big_store
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.big_store.map(element => (
          <div className="bigstore">
            <h1> {element.title} </h1>
            <div className="grid">
              {/* <i className="icofont-toy-horse"></i> */}
              <div style={{ textAlign: "center" }}>
                <i
                  class="fa fa-horse"
                  style={{ fontSize: "30px", color: "lightblue" }}
                ></i>
                <p>{element.product} </p>
              </div>
              {/* <i className="icofont-star"></i>   */}
              <div style={{ textAlign: "center" }}>
                <i
                  class="fa fa-star"
                  style={{ fontSize: "30px", color: "lightblue" }}
                ></i>
                <p> {element.brands}</p>
              </div>
              <div style={{ textAlign: "center" }}>
                {/* <i className="icofont-baby"></i> */}
                {/* <i class="icofont-baby"></i> */}
                <i
                  class="fa fa-baby"
                  style={{ fontSize: "30px", color: "lightblue" }}
                ></i>
                <p> {element.registered_users} </p>
              </div>
              <div style={{ textAlign: "center" }}>
                <i
                  class="fa fa-undo"
                  style={{ fontSize: "30px", color: "lightblue" }}
                ></i>
                <p> {element.policy} </p>
              </div>
              <div style={{ textAlign: "center" }}>
                {/* <i className="icofont-truck-loaded"></i> */}
                <i
                  class="fa fa-truck"
                  style={{ fontSize: "30px", color: "lightblue" }}
                ></i>
                <p>{element.shipping} </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
