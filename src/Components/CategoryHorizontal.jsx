import React from "react";
export default class CategoryHorizontal extends React.Component {
  componentDidMount() {
    // console.log(this.props);
  }
  render() {
    return (
      <div className="col-md-6 col-xs-6">
        <div className="card">
          <div className="row rowpadd">
            <div className="col-md-4 image">
              <img
                src={this.props.imgurl}
                width="100%"
                className="img"
                alt="Some"
              />
            </div>

            <div className="col-md-8">
              <div className="text">
                <p className="title">{this.props.title}</p>

                {/* <p className="para">Brand : {this.props.brand}</p> */}
                <div className="row rowpadd">
                  <div className="col-md-6 image">
                    <p className="price">₹{this.props.price}</p>
                  </div>
                  <div className="col-md-6">
                    {/* <p className="cart">
                      <i className="icofont-cart"></i>
                    </p> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
