import React, { Component } from "react";
import Axios from "axios";
export default class Offer extends Component {
  state = {
    offers: []
  };
  componentDidMount() {
    let url = "Offer.json";
    Axios.get(url).then(res => {
      this.setState({
        offers: res.data.offers
      });
    });
  }
  render() {
    return (
      <div className="offer">
        {this.state.offers.map((SingleElement, index) => (
          <div
            key={index}
            className="bgimg"
            style={{ backgroundImage: "url(" + SingleElement.Bgimg + ")" }}
          >
            <div className="layer"></div>
            <div className="textcenter">
              <p className="Shead">{SingleElement.smallhead}</p>
              <h3>{SingleElement.heading}</h3>
              <p className="date">{SingleElement.Date}</p>
              <h1>{SingleElement.Bighead}</h1>
              <button>{SingleElement.Button}</button>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
