import React, { Component } from "react";

export default class Button extends Component {
  render() {
    return (
      <div className="demo">
        <div className="main">
          <button className="button">Submit</button>
        </div>
        <div className="row">
          <div className="col-md-4">
            <div className="image">
              <div className="layer">
                <div className="text">
                  <h1>Good</h1>
                </div>
              </div>
              <img
                src="https://colorlib.com/preview/theme/meetme/img/gallery/project-3.jpg"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
