import React, { Component } from "react";
import Lottie from "react-lottie";
import animationData from "../../src/assets/cart.json";

export default class NotificationonAddCart extends Component {
  render() {
    return (
      <div className="notificationWrapper">
        <Lottie
          options={{
            animationData: animationData
          }}
          style={{
            width: "30px",
            height: "30px"
            // marginRight: "16px",
          }}
        />
        <h6
          style={{ color: "white", fontFamily: "Nunito", marginLeft: "10px" }}
        >
          {this.props.message}
        </h6>
      </div>
    );
  }
}
