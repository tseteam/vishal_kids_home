import React, { Component } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";
import login from "../Components/LoginCheck";
import init from "../helper/WindowToken";
import Constants from "../variable/Constants";
export default class Menu extends Component {
  state = {
    menu: [],
    Pdata: [],
    display: false,
    img: "",
    Secondary_Categories: [],
    Ternary_Categories: [],
    brands: [],
    ageGroup: [],
    ternary_Categories: [],
    primaryCatId: 0,
    userDetails: [],
    selectCat: false,
    visible: true,
    new: false,
    scrolled: false
  };
  componentDidMount() {
    // window.addEventListener("scroll", () => {
    //   console.log(window.scrollY);
    //   const isTop = window.scrollY < 100;
    //   if (isTop !== true) {
    //     this.setState({ scrolled: true });
    //   } else {
    //     this.setState({ scrolled: false });
    //   }
    // });
    if (init() === "success") {
      if (login()) {
        Axios.get(Constants.getUrl.userDetails).then(res => {
          this.setState({
            userDetails: res.data
          });
          console.log("user", res.data);
        });
      }
    }
    Axios.get(Constants.getUrl.Primary_Categories).then(res => {
      console.log(res);
      this.setState(
        {
          Pdata: res.data
        },
        () => {
          console.log(this.state.Pdata);
        }
      );
    });
    Axios.get(Constants.getUrl.Secondary_Categories).then(res => {
      // console.log(res.data);
      this.setState({
        Secondary_Categories: res.data,
        Second_Category: res.data
      });
    });
    Axios.get(Constants.getUrl.ternary_Categories).then(res => {
      // console.log(res.data);
      this.setState({
        // ternary_Categories: res.data,
        // ternory_Category: res.data
      });
    });

    Axios.get(Constants.getUrl.ternary_Categories).then(res => {
      // console.log(res.data);
      this.setState(
        {
          // Ternary_Categories: res.data
        },
        () => {
          // console.log(this.state.Ternary_Categories);
        }
      );
    });
    Axios.get(Constants.getUrl.brands).then(res => {
      // console.log(res.data.data);

      this.setState({
        // brands: res.data
      });
    });
    Axios.get(Constants.getUrl.ageGroup).then(res => {
      // console.log(res.data.data);
      this.setState({
        // ageGroup: res.data
      });
    });
  }

  hideMenu() {
    this.setState({
      visible: false
    });
  }
  openLeftMenu() {
    document.getElementById("leftMenu").style.display = "block";
  }
  closeLeftMenu() {
    document.getElementById("leftMenu").style.display = "none";
  }
  show = id => {
    Axios.get("https://admin.vishalkids.in/api/primary-category/" + id).then(
      res => {
        // console.log(res.data);
        this.setState({
          ternary_Categories: res.data.ternary_category,
          ageGroup: res.data.age_group,
          brands: res.data.brand_name,
          img: res.data.image,
          new: true
        });
      }
    );
  };
  logout = e => {
    localStorage.clear();
    window.location.href = "/";
  };
  selectCategory = e => {
    this.setState({
      selectCat: true
    });
  };
  render() {
    return (
      <div>
        <div className="menu">
          <div className="hidefrommainu">
            {/* <img
            alt="some"
            src="https://vishalkids.in/vishal-logo.png"
            width="40%"
            className="logoimage"
            style={{
              background: "white",
              width: "133%",
              paddingRight: "60px",
              paddingLeft: "60px",
              marginLeft: "-17px"
            }}
          /> */}
            {/* <p
            className="hoverClass"
            style={{ color: "white", marginLeft: "90%" }}
            onClick={() => {
              this.hideMenu();
            }}
          >
            <i class="fas fa-times"></i>
          </p>
          {this.state.visible ? (
            <div className="somecontents" style={{ left: "-300px" }}>
              <Menu />
            </div>
          ) : null} */}
            <div className="dropdown" style={{}}>
              <h5 className="loginResp">
                <i
                  style={{
                    // marginLeft: "4px",
                    color: "white",
                    fontSize: "12px",
                    background: "orange",
                    borderRadius: "15px",
                    padding: "7px",
                    border: "1px solid white",
                    marginRight: "10px",
                    marginTop: "32px",
                    marginLeft: "-10px"
                  }}
                  class="far fa-user"
                ></i>
                {login()
                  ? "Hi, " + this.state.userDetails.name
                  : "Login | Sign up"}
                {/* <i className="fas fa-user-alt"></i> */}
              </h5>
              <div className="dropdown-contents" style={{ left: "3%" }}>
                {login() ? (
                  <div>
                    <Link to="/user/info">My Profile</Link>
                    <Link to="/user/order">Orders</Link>
                    <Link to="/sign-in" onClick={this.logout}>
                      Logout
                    </Link>
                  </div>
                ) : (
                  <div>
                    <Link to="/sign-in"> Login</Link>
                    <Link to="/register"> Create account</Link>
                  </div>
                )}
              </div>
            </div>
            <img
              alt="some"
              src="https://vishalkids.in/vishal-logo.png"
              width="40%"
              className="logoimage"
              style={{
                // background: "white",
                width: "96%",
                paddingRight: "60px",
                paddingLeft: "60px",
                marginLeft: "-17px",
                marginTop: "20px"
              }}
            />
          </div>
          <div className="showNavMenu">
            <ul>
              <div className="main">
                <li className="menuTitle menuTitleWrapper">
                  <Link to="/" style={{ textDecoration: "none" }}>
                    {/* <i class="fa fa-home homeIcon"></i> */}
                    HOME
                    <i
                      class="icofont-thin-right"
                      // style={{
                      //   position: "absolute",
                      //   top: "26%",
                      //   left: "84%"
                      // }}
                    ></i>
                  </Link>
                </li>
                <li className="menuTitle ">
                  <Link
                    to="/"
                    style={{
                      textDecoration: "none",
                      textTransform: "uppercase"
                    }}
                  >
                    {/* <i class="fa fa-home homeIcon"></i> */}
                    All Categories
                    <i
                      class="icofont-thin-right"
                      // style={{
                      //   position: "absolute",
                      //   top: "26%",
                      //   left: "84%"
                      // }}
                      // 
                    ></i>
                    <span style={{ marginLeft: "0px" }}>
                      <i
                        className="fas fa-chevron-down chevron chevronIcon "
                        // style={{ color: "white" }}
                      ></i>
                    </span>
                    <div
                      className="dropdown hidefrommain "
                      style={{
                        minHeight: "100%"
                      }}
                    >
                      <h4 style={{ fontWeight: "bold", fontSize: "24px" }}>
                        All Categories
                      </h4>
                      <div className="row">
                        {this.state.Pdata.map((element, index) => (
                          <li
                            key={index}
                            onMouseEnter={() => {
                              this.show(element.id);
                              this.setState({
                                newState: index
                              });
                            }}
                            onMouseLeave={this.hide}
                            id={element.id}
                            style={{
                              display: "flex",
                              textAlign: "left",
                              backgroundColor: "white"
                            }}

                            // className="menuTitle"
                            // onClick={this.selectCategory}
                          >
                            {/* {this.state.selectCat ? ( */}
                            <div className="col-sm-3">
                              <Link
                                className="linkHoverinmenu"
                                style={{
                                  textDecoration: "none",
                                  color: "black",
                                  padding: "5px"
                                }}
                                to={{
                                  pathname: "/productlist/" + element.id
                                }}
                              >
                                <span
                                  onClick={() => {
                                    window.location.href =
                                      "/productlist/" + element.id;
                                  }}
                                >
                                  {element.category_name}
                                </span>
                              </Link>
                            </div>
                            {this.state.new && this.state.newState === index ? (
                              <div
                                className="row"
                                style={{
                                  margin: "0px auto 0px",
                                  position: "absolute",
                                  top: "10px",
                                  left: "30%"
                                }}
                              >
                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Category</h5>

                                  {this.state.ternary_Categories.map(
                                    (tCategory, index) => (
                                      <Link
                                        style={{
                                          textDecoration: "none",
                                          color: "white",
                                          padding: "0px",
                                          display: "block"
                                        }}
                                        to={{
                                          pathname:
                                            "/productlist/" + element.id,
                                          state: {
                                            ternaryId: tCategory.id
                                          }
                                        }}
                                      >
                                        <h4
                                          onClick={() => {
                                            window.location.href =
                                              "/productlist/" + element.id;
                                          }}
                                          key={index}
                                        >
                                          {tCategory.category_name}
                                        </h4>
                                      </Link>
                                    )
                                  )}
                                </div>
                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Age</h5>
                                  {this.state.ageGroup.map((age, index) => (
                                    <Link
                                      style={{
                                        textDecoration: "none",
                                        color: "white",
                                        padding: "0px",
                                        display: "block"
                                      }}
                                      to={{
                                        pathname: "/productlist/" + element.id,
                                        state: {
                                          ageId: age.id
                                        }
                                      }}
                                    >
                                      <h4
                                        onClick={() => {
                                          window.location.href =
                                            "/productlist/" + element.id;
                                        }}
                                        key={index}
                                      >
                                        {age.name}
                                      </h4>
                                    </Link>
                                  ))}
                                </div>

                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Brands</h5>
                                  {this.state.brands.map((brand, index) => (
                                    <Link
                                      style={{
                                        textDecoration: "none",
                                        color: "white",
                                        padding: "0px",
                                        display: "block"
                                      }}
                                      to={{
                                        pathname: "/productlist/" + element.id,
                                        state: {
                                          brandId: brand.id
                                        }
                                      }}
                                    >
                                      <h4
                                        onClick={() => {
                                          window.location.href =
                                            "/productlist/" + element.id;
                                        }}
                                        key={index}
                                      >
                                        {brand.name}
                                      </h4>
                                    </Link>
                                  ))}
                                </div>
                                <div className="col-md-3 col-sm-3">
                                  <img
                                    src={this.state.img}
                                    alt=""
                                    style={{ width: "200px" }}
                                  />
                                </div>
                              </div>
                            ) : null}
                          </li>
                        ))}
                      </div>
                    </div>
                  </Link>
                </li>
                {this.state.Pdata.slice(0, 8).map((element, index) => (
                  <li
                    key={index}
                    onMouseEnter={() => {
                      this.show(element.id);
                    }}
                    onMouseLeave={this.hide}
                    id={element.id}
                    className="menuTitle hidefromresp"
                    // onClick={this.selectCategory}
                  >
                    {/* {this.state.selectCat ? ( */}
                    <Link className="CateColor"
                      style={{ textDecoration: "none", color: "white" }}
                      to={{
                        pathname: "/productlist/" + element.id
                      }}
                    >
                      <span
                        onClick={() => {
                          window.location.href = "/productlist/" + element.id;
                        }}
                      >
                        {element.category_name}
                      </span>
                    </Link>
                    <span style={{ marginLeft: "0px" }}>
                      <i
                        className="fas fa-chevron-down chevron chevronIcon"
                        style={{ color: "white" }}
                      ></i>
                    </span>
                    <div className="dropdown hidefrommain ">
                      <div className="row">
                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Category</h5>

                          {this.state.ternary_Categories.map(
                            (tCategory, index) => (
                              <Link
                                style={{
                                  textDecoration: "none",
                                  color: "white",
                                  padding: "0px",
                                  display: "block"
                                }}
                                to={{
                                  pathname: "/productlist/" + element.id,
                                  state: {
                                    ternaryId: tCategory.id
                                  }
                                }}
                              >
                                <h4
                                  onClick={() => {
                                    window.location.href =
                                      "/productlist/" + element.id;
                                  }}
                                  key={index}
                                >
                                  {tCategory.category_name}
                                </h4>
                              </Link>
                            )
                          )}
                        </div>
                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Age</h5>
                          {this.state.ageGroup.map((age, index) => (
                            <Link
                              style={{
                                textDecoration: "none",
                                color: "white",
                                padding: "0px",
                                display: "block"
                              }}
                              to={{
                                pathname: "/productlist/" + element.id,
                                state: {
                                  ageId: age.id
                                }
                              }}
                            >
                              <h4
                                onClick={() => {
                                  window.location.href =
                                    "/productlist/" + element.id;
                                }}
                                key={index}
                              >
                                {age.name}
                              </h4>
                            </Link>
                          ))}
                        </div>

                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Brands</h5>
                          {this.state.brands.map((brand, index) => (
                            <Link
                              style={{
                                textDecoration: "none",
                                color: "white",
                                padding: "0px",
                                display: "block"
                              }}
                              to={{
                                pathname: "/productlist/" + element.id,
                                state: {
                                  brandId: brand.id
                                }
                              }}
                            >
                              <h4
                                onClick={() => {
                                  window.location.href =
                                    "/productlist/" + element.id;
                                }}
                                key={index}
                              >
                                {brand.name}
                              </h4>
                            </Link>
                          ))}
                        </div>
                        <div className="col-md-3 col-sm-3">
                          {/* <h5>Shop By Price</h5> */}
                          <img
                            src={this.state.img}
                            alt=""
                            style={{ width: "200px" }}
                          />
                        </div>
                      </div>
                    </div>

                    {/* // ) : null} */}
                    {/* )} */}
                  </li>
                ))}
              </div>
            </ul>
          </div>
          <div className="shownav">
            <ul>
              <div className="main">
                <li className="menuTitle menuTitleWrapper">
                  <Link to="/" style={{ textDecoration: "none" }}>
                    {/* <i class="fa fa-home homeIcon"></i> */}
                    HOME
                    <i
                      class="icofont-thin-right"
                      // style={{
                      //   position: "absolute",
                      //   top: "26%",
                      //   left: "84%"
                      // }}
                    ></i>
                  </Link>
                </li>
                <li className="menuTitle ">
                  <Link
                    to="/"
                    style={{
                      textDecoration: "none",
                      textTransform: "uppercase"
                    }}
                  >
                    All Categories
                    <i
                      class="icofont-thin-right"
                      // style={{
                      //   position: "absolute",
                      //   top: "26%",
                      //   left: "84%"
                      // }}
                    ></i>
                    <span style={{ marginLeft: "0px" }}>
                      <i
                        className="fas fa-chevron-down chevron chevronIcon"
                        style={{ color: "white" }}
                      ></i>
                    </span>
                    <div
                      className="dropdown hidefrommain "
                      style={{
                        minHeight: "100%"
                      }}
                    >
                      <h4 style={{ fontWeight: "bold", fontSize: "24px" }}>
                        All Categories
                      </h4>
                      <div className="row">
                        {this.state.Pdata.map((element, index) => (
                          <li
                            key={index}
                            onMouseEnter={() => {
                              this.show(element.id);
                              this.setState({
                                newState: index
                              });
                            }}
                            onMouseLeave={this.hide}
                            id={element.id}
                            style={{
                              display: "flex",
                              textAlign: "left",
                              backgroundColor: "white"
                            }}

                            // className="menuTitle"
                            // onClick={this.selectCategory}
                          >
                            {/* {this.state.selectCat ? ( */}
                            <div className="col-sm-3">
                              <Link
                                className="linkHoverinmenu"
                                style={{
                                  textDecoration: "none",
                                  color: "black",
                                  padding: "5px"
                                }}
                                to={{
                                  pathname: "/productlist/" + element.id
                                }}
                              >
                                <span
                                  onClick={() => {
                                    window.location.href =
                                      "/productlist/" + element.id;
                                  }}
                                >
                                  {element.category_name}
                                </span>
                              </Link>
                            </div>
                            {this.state.new && this.state.newState === index ? (
                              <div
                                className="row"
                                style={{
                                  margin: "0px auto 0px",
                                  position: "absolute",
                                  top: "10px",
                                  left: "30%"
                                }}
                              >
                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Category</h5>

                                  {this.state.ternary_Categories.map(
                                    (tCategory, index) => (
                                      <Link
                                        style={{
                                          textDecoration: "none",
                                          color: "white",
                                          padding: "0px",
                                          display: "block"
                                        }}
                                        to={{
                                          pathname:
                                            "/productlist/" + element.id,
                                          state: {
                                            ternaryId: tCategory.id
                                          }
                                        }}
                                      >
                                        <h4
                                          onClick={() => {
                                            window.location.href =
                                              "/productlist/" + element.id;
                                          }}
                                          key={index}
                                        >
                                          {tCategory.category_name}
                                        </h4>
                                      </Link>
                                    )
                                  )}
                                </div>
                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Age</h5>
                                  {this.state.ageGroup.map((age, index) => (
                                    <Link
                                      style={{
                                        textDecoration: "none",
                                        color: "white",
                                        padding: "0px",
                                        display: "block"
                                      }}
                                      to={{
                                        pathname: "/productlist/" + element.id,
                                        state: {
                                          ageId: age.id
                                        }
                                      }}
                                    >
                                      <h4
                                        onClick={() => {
                                          window.location.href =
                                            "/productlist/" + element.id;
                                        }}
                                        key={index}
                                      >
                                        {age.name}
                                      </h4>
                                    </Link>
                                  ))}
                                </div>

                                <div className="col-md-3 col-sm-3">
                                  <h5>Shop By Brands</h5>
                                  {this.state.brands.map((brand, index) => (
                                    <Link
                                      style={{
                                        textDecoration: "none",
                                        color: "white",
                                        padding: "0px",
                                        display: "block"
                                      }}
                                      to={{
                                        pathname: "/productlist/" + element.id,
                                        state: {
                                          brandId: brand.id
                                        }
                                      }}
                                    >
                                      <h4
                                        onClick={() => {
                                          window.location.href =
                                            "/productlist/" + element.id;
                                        }}
                                        key={index}
                                      >
                                        {brand.name}
                                      </h4>
                                    </Link>
                                  ))}
                                </div>
                                <div className="col-md-3 col-sm-3">
                                  <img
                                    src={this.state.img}
                                    alt=""
                                    style={{ width: "200px" }}
                                  />
                                </div>
                              </div>
                            ) : null}
                          </li>
                        ))}
                      </div>
                    </div>
                  </Link>
                </li>
             
                {this.state.Pdata.slice(0, 6).map((element, index) => (
                  <li
                    key={index}
                    onMouseEnter={() => {
                      this.show(element.id);
                    }}
                    onMouseLeave={this.hide}
                    id={element.id}
                    className="menuTitle hidefromresp"
                   
                  >
                   
                    <Link
                      style={{ textDecoration: "none", color: "white" }}
                      to={{
                        pathname: "/productlist/" + element.id
                      }}
                    >
                      <span
                        onClick={() => {
                          window.location.href = "/productlist/" + element.id;
                        }}
                      >
                        {element.category_name}
                      </span>
                    </Link>
                    <span style={{ marginLeft: "0px" }}>
                      <i
                        className="fas fa-chevron-down chevron chevronIcon"
                        style={{ color: "white" }}
                      ></i>
                    </span>
                    <div className="dropdown hidefrommain ">
                      <div className="row">
                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Category</h5>

                          {this.state.ternary_Categories.map(
                            (tCategory, index) => (
                              <Link
                                style={{
                                  textDecoration: "none",
                                  color: "white",
                                  padding: "0px",
                                  display: "block"
                                }}
                                to={{
                                  pathname: "/productlist/" + element.id,
                                  state: {
                                    ternaryId: tCategory.id
                                  }
                                }}
                              >
                                <h4
                                  onClick={() => {
                                    window.location.href =
                                      "/productlist/" + element.id;
                                  }}
                                  key={index}
                                >
                                  {tCategory.category_name}
                                </h4>
                              </Link>
                            )
                          )}
                        </div>
                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Age</h5>
                          {this.state.ageGroup.map((age, index) => (
                            <Link
                              style={{
                                textDecoration: "none",
                                color: "white",
                                padding: "0px",
                                display: "block"
                              }}
                              to={{
                                pathname: "/productlist/" + element.id,
                                state: {
                                  ageId: age.id
                                }
                              }}
                            >
                              <h4
                                onClick={() => {
                                  window.location.href =
                                    "/productlist/" + element.id;
                                }}
                                key={index}
                              >
                                {age.name}
                              </h4>
                            </Link>
                          ))}
                        </div>

                        <div className="col-md-3 col-sm-3">
                          <h5>Shop By Brands</h5>
                          {this.state.brands.map((brand, index) => (
                            <Link
                              style={{
                                textDecoration: "none",
                                color: "white",
                                padding: "0px",
                                display: "block"
                              }}
                              to={{
                                pathname: "/productlist/" + element.id,
                                state: {
                                  brandId: brand.id
                                }
                              }}
                            >
                              <h4
                                onClick={() => {
                                  window.location.href =
                                    "/productlist/" + element.id;
                                }}
                                key={index}
                              >
                                {brand.name}
                              </h4>
                            </Link>
                          ))}
                        </div>
                        <div className="col-md-3 col-sm-3">
                          {/* <h5>Shop By Price</h5> */}
                          <img
                            src={this.state.img}
                            alt=""
                            style={{ width: "200px" }}
                          />
                        </div>
                      </div>
                    </div>

                    {/* // ) : null} */}
                    {/* )} */}
                  </li>
                ))}
              </div>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
