import React, { Component } from "react";
import Axios from "axios";
import Constants from "../variable/Constants";
import { Redirect } from "react-router-dom";
import Modal from "react-modal";
import init from "../helper/WindowToken";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    position: "absolute",
    boxShadow:
      " 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
    background: "rgb(255, 255, 255)",
    overflow: "auto",
    outline: "none",
    padding: "20px"
  }
};
Modal.setAppElement("body");
export default class ManageInfo extends Component {
  state = {
    addresses: [],
    address: "",
    city: "",
    state: "",
    pincode: 0,
    type: "",
    openModal: false,
    addAddress: "",
    redirect: false,
    id: 0,
    usertoken: ""
  };
  componentDidMount() {
    let id = localStorage.getItem("id");
    // console.log(usertoken);
    Axios.get(Constants.getUrl.addresses).then(res => {
      console.log(res);
      this.setState(
        {
          addresses: res.data.address,
          id
          // usertoken
        },
        () => {
          // console.log(this.state.addresses[0].address);
        }
      );
    });
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    // alert();
    const payload = {
      address: this.state.address,
      city: this.state.city,
      pincode: parseInt(this.state.pincode),
      type: this.state.type,
      state: this.state.state
    };

    // const usertoken = localStorage.getItem("token");
    // console.log(usertoken);
    Axios.post(
      Constants.postUrl.address,

      payload
    ).then(res => {
      console.log(res);
      this.setState({
        redirect: true
      });
    });
  };
  // api/allsubject?id=
  maxLengthCheck = e => {
    if (e.target.value.length > e.target.maxLength) {
      e.target.value = e.target.value.slice(0, e.target.maxLength);
    }
  };
  openModal = e => {
    // this.setState({
    //   openModal: true
    // });
    let data = this.state.addresses.find(address => address.id === e.target.id);
    // console.log(data);
    this.setState(
      {
        address: data.address,
        city: data.city,
        state: data.state,
        pincode: data.pincode,
        type: data.type,
        openModal: true
      },
      () => {
        // console.log(this.state.adsdress);
      }
    );
  };
  updateInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  updateAddress = (e, id) => {
    e.preventDefault();
    const payload = {
      address: this.state.address,
      city: this.state.city,
      pincode: this.state.pincode,
      type: this.state.type,
      state: this.state.state
    };
    Axios.post(
      Constants.postUrl.updateAddress + this.state.addresses[0].id,
      payload
    ).then(res => {
      // console.log(res);
      this.setState({
        redirect: true
      });
    });
  };
  closeModal = e => {
    this.setState({ openModal: false });
  };

  render() {
    if (init() === "success") {
      if (this.state.redirect) {
        return <Redirect to="/user/manage" />;
      } else {
        return (
          <div className="manage">
            <div className="row">
              <div className="userAddContainer">
                {this.state.addresses
                  ? this.state.addresses.map((address, index) => (
                      <div className="col-md-6" key={index}>
                        <div className="userAddress">
                          <p> {address.address}</p>
                          <p>{address.city}</p>
                          <p>{address.state}</p>
                          <p>{address.pincode}</p>
                          <button
                            type="submit"
                            onClick={this.openModal}
                            id={address.id}
                            className="addressBtn"
                          >
                            Update Address
                          </button>
                        </div>
                      </div>
                    ))
                  : null}
              </div>
              <div style={{ width: "50%" }}>
                <Modal
                  isOpen={this.state.openModal}
                  onAfterOpen={this.afterOpenModal}
                  onRequestClose={this.closeModal}
                  style={customStyles}
                  contentLabel="Example Modal"
                >
                  <form onSubmit={this.updateAddress}>
                    <div style={{ margin: "10px" }}>
                      <input
                        style={{
                          padding: "10px",
                          border: "1px solid #d3d4d2"
                        }}
                        name="address"
                        type="text"
                        defaultValue={this.state.address}
                        onChange={this.updateInput}
                      />
                    </div>
                    <div style={{ margin: "10px" }}>
                      <input
                        style={{ padding: "10px", border: "1px solid #d3d4d2" }}
                        type="text"
                        name="city"
                        defaultValue={this.state.city}
                        onChange={this.updateInput}
                      />
                    </div>
                    <div style={{ margin: "10px" }}>
                      <input
                        style={{ padding: "10px", border: "1px solid #d3d4d2" }}
                        type="text"
                        name="state"
                        defaultValue={this.state.state}
                        onChange={this.updateInput}
                      />
                    </div>
                    <div style={{ margin: "10px" }}>
                      <input
                        style={{
                          padding: "10px",
                          border: "1px solid #d3d4d2"
                        }}
                        name="pincode"
                        type="text"
                        defaultValue={this.state.pincode}
                        onChange={this.updateInput}
                      />
                    </div>
                    <div style={{ margin: "10px" }}>
                      <input
                        style={{ padding: "10px", border: "1px solid #d3d4d2" }}
                        name="type"
                        type="text"
                        defaultValue={this.state.type}
                        onChange={this.updateInput}
                      />
                    </div>
                    <button
                      onClick={this.closeModal}
                      style={{
                        width: "80px",
                        padding: "10px",
                        border: "none",
                        backgroundColor: "lightgray",
                        margin: "2px"
                      }}
                    >
                      Close
                    </button>
                    <button
                      type="submit"
                      style={{
                        margin: "2px",

                        width: "150px",
                        padding: "10px",
                        border: "none",
                        backgroundColor: "lightgray"
                      }}
                    >
                      Save changes
                    </button>
                  </form>
                </Modal>
              </div>
            </div>
            <form onSubmit={this.handleSubmit}>
              {/* <h3 className="main">Manage Addresses</h3> */}
              <div className="address">
                <h2 className="add">Add New Address</h2>
                {/* <h3 className="location">Use My Current Location</h3> */}
                {/* <input type="text" className="int" placeholder="Name" /> */}
                <input
                  name="address"
                  onChange={this.handleChange}
                  type="text"
                  className="area"
                  placeholder="Address (Area and Street)"
                />
                <div>
                  <input
                    name="city"
                    onChange={this.handleChange}
                    type="textarea"
                    className="int"
                    placeholder="City/District/Town"
                  />
                </div>
                <div>
                  <input
                    name="state"
                    type="textarea"
                    className="int"
                    placeholder="State"
                    onChange={this.handleChange}
                  />
                </div>
                <div>
                  <input
                    name="pincode"
                    type="text"
                    className="int"
                    placeholder="Pincode"
                    onChange={this.handleChange}
                  />
                </div>
                <div>
                  {/* <input
                type="text"
                className="int"
                placeholder="Landmark (Optional)"
              /> */}
                  {/* <input
                type="text"
                className="int"
                placeholder="Alternate Phone (optional)"
                maxLength="10"
                onInput={this.maxLengthCheck}
              /> */}
                </div>
                {/* <p className="type">Address Type</p> */}
                <input
                  name="type"
                  type="radio"
                  value="home"
                  className="inttype"
                  onChange={this.handleChange}
                />
                Home
                <input
                  name="type"
                  type="radio"
                  value="work"
                  className="inttype"
                  onChange={this.handleChange}
                />
                Work
                <div className="savebtn">
                  <button type="submit" className="save">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        );
      }
    } else {
      return <Redirect to="/sign-in" />;
    }
  }
}
