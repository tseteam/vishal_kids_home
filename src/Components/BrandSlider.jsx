import React, { Component } from "react";
import Axios from "axios";
import Constants from "../variable/Constants";
import { Slide } from "react-slideshow-image";
// import {
//   CarouselProvider,
//   Slider,
//   Slide,
//   ButtonBack,
//   ButtonNext
// } from "pure-react-carousel";
export default class BrandSlider extends Component {
  state = {
    top_brands: [],
    brands: []
  };
  componentDidMount() {
    // let url = "Brand_slider.json";
    Axios.get(Constants.getUrl.brands).then(res => {
      console.log(res.data);
      res.data.forEach(item => {
        this.add(item);
      });
      console.log(this.state.brands);

      this.setState(
        {
          top_brands: this.state.brands
        },
        () => {
          console.log(this.state.top_brands);
        }
      );
    });
  }
  add = object => {
    if (object) {
      if (this.state.brands.filter(x => x.name === object.name).length === 0) {
        this.state.brands.push(object);
      }
    }
  };
  render() {
    const properties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicators: true,
      arrows: false,
      onChange: (oldIndex, newIndex) => {
        // console.log(`slide transition from ${oldIndex} to ${newIndex}`);
      }
    };
    return (
      <div className="brand">
        <div className="content">
          <div className="brand_slider">
            <div className="text">
              <p className="top">Top</p>
              <p style={{ fontWeight: "bold" }}>Brands</p>
            </div>
            {/* <Slide {...properties}>
              {this.state.top_brands.map((singleElement, index) => (
                <img
                  src={singleElement.image}
                  key={index}
                  alt="some"
                  className="brandImage"
                />
              ))}
            </Slide> */}
            {/* <CarouselProvider
              naturalSlideWidth={5}
              naturalSlideHeight={5}
              totalSlides={this.state.top_brands.length}
              isPlaying={true}
              interval={700}
              // animation= {3s ease-in 1s}
            >
              <Slider>
                {this.state.top_brands.map((singleElement, index) => (
                  // <Slide index={index}>
                  <img
                    className="brandImage"
                    // style={{ width: "30px", display: "inline-block" }}
                    src={singleElement.image}
                    key={index}
                    alt="some"
                  />
                  // </Slide>
                ))}
              </Slider>
            </CarouselProvider> */}

            {/* {this.state.top_brands.map((singleElement, index) => (
              <marquee>
                <img
                  className="brandImage"
                  // style={{ width: "30px", display: "inline-block" }}
                  src={singleElement.image}
                  key={index}
                  alt="some"
                  width="100%"
                />
              </marquee>
            ))} */}
            <marquee
              width="300%"
              direction="left"
              height="37px"
              // style={{ textAlign: "center" }}
            >
              {this.state.top_brands.map((singleElement, index) => (
                <img
                  className="brandImage"
                  src={singleElement.image}
                  key={index}
                  alt="some"
                />
              ))}
            </marquee>
          </div>
        </div>
      </div>
    );
  }
}
