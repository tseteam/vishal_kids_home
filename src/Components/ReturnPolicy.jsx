import React, { Component } from 'react'
import Footer from '../Partials/Footer'

export default class ReturnPolicy extends Component {
    render() {
        return (
                <div >
            <div className="p-3 terms_conditions container">
                <h1 className=" text-center">Return & Refund Policy</h1>
                <p>
                Thanks for shopping at www.vishalkids.com. </p>
                <h5>Returns</h5>
               <p>Since, we sell various customized and personalized products, /return/exchange of the product is not possible. </p>
               <p>
               However, in case of special circumstances and after fulfilling certain conditions, our team may allow return/exchange of the product.</p>
               <p> Following are the conditions to be fulfilled for return/exchange: </p>
               <ol>
                   <li>To be eligible for a return, your item must be unused and in the same condition that you received it</li>
                   <li>
                   Your item must be in the original packaging.
                   </li>
                   <li>
                   Your item needs to have the receipt or proof of purchase.
                   </li>
               </ol>
               <p>
               If above conditions are fulfilled, customer can reach us through mail vishalkids.com and place a request for return/exchange. Once we receive your mail, we will inspect the case and notify you our approval/denial as the case may be. </p>
               <h5> Refunds </h5>
               <p>If your return request is approved, we will further guide you to deliver the product in a particular address. We will immediately notify you on the status of your refund after inspecting the item.</p>
               <p> If your refund is approved, we will initiate a refund to your original method of payment.</p>
               <p>You will receive the credit within a certain amount of days, depending on your card issuer's policies.</p>
               <h5> Shipping</h5>
              <p> You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are nonrefundable.</p>
              <p> If you receive a refund, the cost of return shipping will be deducted from your refund.</p>
                  
                 <h5> Contact Us </h5>
                <p> If you have any questions on how to return your item to us, contact us on 75594 13990.
               </p>
               </div>
               <Footer/>
            </div>
        )
    }
}
