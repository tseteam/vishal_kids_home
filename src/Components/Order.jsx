import React, { Component } from "react";
import Axios from "axios";
import Constants from "../variable/Constants";

export default class Order extends Component {
  state = {
    orders: []
  };
  componentDidMount() {
    Axios.get(Constants.getUrl.orders).then(res => {
      console.log(res);
      this.setState({
        orders: res.data.myorder
      });
    });
  }
  render() {
    return (
      <div className="orderSection">
        {/* <div className="row"> */}
        {this.state.orders.map((order, index) => {
          let parseImageArray = [];
          if (order.product_variant) {
            parseImageArray = JSON.parse(order.product_variant.image);
          }

          return (
            <div key={index}>
              <div className="orderWraper">
                <div className="row">
                  <div className="col-sm-3 col-xs-6">
                    <div className="orderImg">
                      {order.product_variant ? (
                        <img src={parseImageArray[0]} width="100%" alt="" />
                      ) : (
                        <p>Image Not Available</p>
                      )}
                    </div>
                  </div>
                  <div className="col-sm-3 orderDetails col-xs-6">
                    <h3>
                      {order.product_variant
                        ? order.product_variant.product.product_name
                        : "NA"}
                    </h3>

                    <label htmlFor=""> Order Id:</label>
                    <span> {order.order_id} </span>

                    <label htmlFor=""> Payment Mode : </label>
                    <span> {order.payment_mode} </span>
                  </div>
                  <div className="col-sm-3">
                    <h4> ₹ {order.total} </h4>
                  </div>

                  <div className="col-sm-3 status">
                    <label htmlFor=""> Payment Status </label>
                    <span> {order.payment_status} </span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
        {/* </div> */}
      </div>
    );
  }
}
