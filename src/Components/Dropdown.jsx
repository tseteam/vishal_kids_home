import React, { Component } from "react";
import Constants from "../variable/Constants";
import Axios from "axios";

export default class Dropdown extends Component {
  state = {
    Ternary_Categories: [],
    brands: [],
    ageGroup: []
  };
  componentDidMount() {
    // console.log(this.props.dropdowncontent);

    let terneryCategories = this.props.dropdowncontent;
    console.log(terneryCategories);

    Axios.get(Constants.getUrl.ternary_Categories).then(res => {
      // console.log(res.data);
      this.setState(
        {
          Ternary_Categories: res.data
        },
        () => {
          // console.log(this.state.Ternary_Categories);
        }
      );
    });
    Axios.get(Constants.getUrl.brands).then(res => {
      // console.log(res.data.data);

      this.setState({
        brands: res.data.data
      });
    });
    Axios.get(Constants.getUrl.ageGroup).then(res => {
      // console.log(res.data.data);
      this.setState({
        ageGroup: res.data.data
      });
    });
  }
  render() {
    let Secondary_Categories = this.props.dropdowncontent;
    console.log(Secondary_Categories);

    return (
      <div className="dropdown">
        <div className="row">
          {/* {Secondary_Categories.map((singleElement, index) => (
            <div className="col-md-3" key={index}>
              <div className="text">
                <h4 className="heading">{singleElement.category_name}</h4>

                {this.state.Ternary_Categories.map((tCategory, index) => (
                  // <a href={tCategory.url} key={index}>
                  <p key={index}>{tCategory.category_name}</p>
                  // </a>
                ))}
              </div>
            </div>
          ))} */}
          <div className="col-md-3">
            <h4>Shop By Category</h4>

            {this.state.Ternary_Categories.map((tCategory, index) => (
              // <a href={tCategory.url} key={index}>
              <p key={index}>{tCategory.category_name}</p>
              // </a>
            ))}
          </div>
          <div className="col-md-3">
            <h4>Shop By Age</h4>
            {this.state.ageGroup.map((age, index) => (
              <p key={index}>{age.name}</p>
            ))}
          </div>
          <div className="col-md-3">
            <h4>Shop By Brands</h4>
            {this.state.brands.map((brand, index) => (
              // <a href={tCategory.url} key={index}>
              <p key={index}>{brand.name}</p>
              // </a>
            ))}
          </div>
          <div className="col-md-3">
            <h4>Shop By Price</h4>
            {/* <img src={} alt="" /> */}
          </div>
        </div>
      </div>
    );
  }
}
