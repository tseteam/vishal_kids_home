import React, { Component } from "react";
import Axios from "axios";
import Constants from "../variable/Constants";
export default class PersonalInfo extends Component {
  state = {
    userDetails: {},
    name: "",
    oldName: "",
    email: "",
    oldEmail: "",
    phone_number: 0,
    userId: 0,
    mobile:0
  };
  componentDidMount() {
    // console.log(this.props);
    const userId = localStorage.getItem("id");
    const name = localStorage.getItem("name");
    const email = localStorage.getItem("email");
    Axios.get(Constants.getUrl.userDetails).then(res => {
      console.log(res.data);
      this.setState(
        {
          userDetails: res.data,
          // userId: res.data.id
          userId
        },
        () => {
          console.log(this.state.userDetails);
          this.setState({
            // name: this.state.userDetails.name,
            name:this.state.userDetails.name,
            email:this.state.userDetails.email,
            mobile:this.state.userDetails.phone_number
            // email: this.state.userDetails.email
          });
        }
      );
    });
  }
  handleInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();

    const payload = {
      name: this.state.name,
      email: this.state.email
    };
    Axios.post(
      Constants.postUrl.updateUserInfo + this.state.userId,
      payload
    ).then(res => {
      console.log(res);
    });
  };
  render() {
    return (
      <div>
        <div className="personalInfo ">
          <h3 className="head">Personal Information</h3>
          <form onSubmit={this.handleSubmit}>
            <div>
              <input
                type="text"
                name="name"
                className="inputs"
                onChange={this.handleInput}
                // defaultValue={this.state.userDetails.name}
                defaultValue={this.state.name}
              />

              <p className="email">Email Address</p>
              <input
                type="text"
                className="inputs"
                name="email"
                onChange={this.handleInput}
                defaultValue={this.state.email}
                // defaultValue={this.state.userDetails.email}
              />
              <p className="email">Mobile Number</p>
              <input
                type="text"
                className="inputs"
                name="phone_number"
                disabled
                value= {this.state.mobile}
                // value={this.state.userDetails.phone_number}
              />
            </div>
            <div>
              <button type="submit">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
