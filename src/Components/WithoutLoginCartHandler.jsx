const addToCartWithoutLogin = product => {
  const cartData = localStorage.getItem("cart");

  let isAddedToCart = false;
  if (cartData) {
    console.log("data present");
    let parsedData = JSON.parse(cartData);
    const productObj = parsedData.find(
      obj => obj.product_variant_id === product.product_variant_id
    );
    if (productObj) {
      parsedData.forEach(item => {
        if (product.product_variant_id === item.product_variant_id) {
          item.quantity += 1;
          // item.quantity = product.quantity;
        }
      });
    } else {
      parsedData.push(product);
    }
    let stringifiedCartData = JSON.stringify(parsedData);
    localStorage.setItem("cart", stringifiedCartData);
    isAddedToCart = true;
    // alert("item added");
    // <NotificationonAddCart />;
  } else {
    console.log("empty");
    let cartArray = [];
    cartArray.push(product);
    let stringifiedCartData = JSON.stringify(cartArray);
    localStorage.setItem("cart", stringifiedCartData);
    isAddedToCart = true;
  }
  return isAddedToCart ? "success" : "failed";
};

export default addToCartWithoutLogin;
