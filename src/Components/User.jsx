import React, { Component } from "react";
import UserSidebar from "./UserSidebar";
import { Switch, Route } from "react-router-dom";
import Order from "./Order";
import PersonalInfo from "./PersonalInfo";
import ManageInfo from "./ManageInfo";
import Footer from "../Partials/Footer";
import Axios from "axios";
import Constants from "../variable/Constants";

export default class User extends Component {
  state = {
    cartDetails: []
  };
  componentDidMount() {
    Axios.get(Constants.getUrl.cartDetails).then(res => {
      // console.log(res.data);

      this.setState(
        {
          cartDetails: res.data
        },

        () => {
          // console.log(this.state.cartDetails);
        }
      );
    });
  }
  render() {
    return (
      <div className="mainuser">
        {/* <Header cartItems={this.state.cartDetails} />
        <Menu /> */}
        <div className="user">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-sm-3">
                <UserSidebar />
              </div>
              <div className="col-md-9 col-sm-9">
                <div className="userInformation">
                  <Switch>
                    <Route path="/user/order" component={Order} />
                    <Route path="/user/info" component={PersonalInfo} />
                    <Route path="/user/manage" component={ManageInfo} />
                  </Switch>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
